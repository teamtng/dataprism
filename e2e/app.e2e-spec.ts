import { DataPrismPage } from './app.po';

describe('data-prism App', () => {
  let page: DataPrismPage;

  beforeEach(() => {
    page = new DataPrismPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
