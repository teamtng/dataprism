import { Schema } from 'app/schema/schema';
import { Table } from 'app/tables/table';
import { TargetColumn } from 'app/tables/targetcolumn';
import { ColumnLineage } from 'app/tables/columnlineage';

export interface SchemaFamily {
    parent: Schema,
    children: TableFamily[]
}

export interface TableFamily {
    parent: Table,
    children: TargetColumnFamily[]
}

export interface TargetColumnFamily {
    parent: TargetColumn,
    children: ColumnLineageInterface[];
}

export interface ColumnLineageInterface {
    parent: ColumnLineage,
    children: ColumnLineageInterface[];
}
