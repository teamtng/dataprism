import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetColumnTreeComponentComponent } from './target-column-tree-component.component';

describe('TargetColumnTreeComponentComponent', () => {
  let component: TargetColumnTreeComponentComponent;
  let fixture: ComponentFixture<TargetColumnTreeComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetColumnTreeComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetColumnTreeComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
