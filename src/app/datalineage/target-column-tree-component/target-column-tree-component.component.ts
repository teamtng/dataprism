import { Component, OnInit, Input, ViewChild, SimpleChange, OnChanges } from '@angular/core';
import { TreeData } from '../tree-data';
import { Table } from '../../tables/table';
import { TableService } from '../../tables/table.service';
import { TargetColumn } from '../../tables/targetcolumn';
import { TargetColumnService } from '../../tables/targetcolumn.service';
import { ColumnLineage } from '../../tables/columnlineage'
import { ColumnLineageService } from '../../tables/columnlineage.service';
import { CollapsibleTreeComponent } from '../../datalineage/collapsibleTree.component';
import { SchemaFamily, TableFamily, TargetColumnFamily, ColumnLineageInterface } from '../datalineage';

@Component({
    selector: 'dp-target-column-tree-component',
    templateUrl: './target-column-tree-component.component.html',
    providers: [TargetColumnService, TableService, ColumnLineageService],
    viewProviders: [CollapsibleTreeComponent],
    styleUrls: ['./target-column-tree-component.component.css']
})

export class TargetColumnTreeComponentComponent implements OnInit, OnChanges {

    @Input() columnSelected: TargetColumnFamily;

    treeData: TreeData;
    @ViewChild(CollapsibleTreeComponent) private collapsibleTree: CollapsibleTreeComponent;

    recursiveCount = 0;

    constructor(private targetColumnService: TargetColumnService,
        private tableService: TableService,
        private columnLineageService: ColumnLineageService) { }

    ngOnInit() { }

    /**
     * Reloads the columns when the SourceSystemId(systemId) changes
     * @param changes
     */
    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        this.InitializeTargetColumnTree()
    }

    InitializeTargetColumnTree() {
        this.treeData = new TreeData();
        this.getTargetColumns();
        this.initTreeRoot();
    }

    getTargetColumns() {
        this.targetColumnService.getTargetColumnsByTargetColumnFilterLineages(this.columnSelected.parent.DatabaseName)
            .subscribe(columns => {
                for (let targetColumn in columns) {
                    this.tableService.getTableById(columns[targetColumn].EDWTableId)
                        .subscribe(tables => {
                            let table = tables[0];
                            let columnLineage: TargetColumnFamily = { parent: columns[targetColumn], children: [] };
                            this.getColumnLineages(columnLineage, table.DatabaseName, table.SchemaName);
                        });
                }
            });
    }

    /**
     * Calls columnLineageService to get all column lineages by column id,
     * then initializes the tree with schema name, table name, and column
     * @param column target column to get lineages with
     * @param tableName name of table to use in tree
     * @param schemaName name of schema to use in tree
     */
    getColumnLineages(column: TargetColumnFamily, tableName: string, schemaName: string) {
        this.columnLineageService.getColumnLineageByColumn(column.parent.Id)
            .subscribe(columnLineages => {
                if (columnLineages && column.children.length == 0) {
                    for (let columnLineage in columnLineages) {
                        let lineageFamily = { parent: columnLineages[columnLineage], children: [] };
                        // Skips adding duplicates
                        if (!column.children.find(c => c.parent.SourceColumn.DatabaseName == lineageFamily.parent.SourceColumn.DatabaseName)) {
                            column.children.push(lineageFamily);
                        }
                    }
                }

                let treeColumnRoot = this.initTreeBranch(column, tableName, schemaName);

                if (column.children.length > 0) {
                    for (let child in column.children) {
                        // Keeps track so it only draws tree once at end
                        this.recursiveCount++;
                        this.getColumnLineageChildren(column, column.children[child], treeColumnRoot.children[child]);
                    }
                } else {
                    // Draws tree if no children because tree is complete
                    this.collapsibleTree.drawTree();
                }
            });
    }

    /**
     * Gets column lineage children of given columnChild then expands the tree at treeColumnRoot
     * @param column column base for lineage
     * @param columnChild current column child to get children of
     * @param treeColumnRoot column root on tree to expand from
     */
    getColumnLineageChildren(column: TargetColumnFamily, columnChild: ColumnLineageInterface, treeColumnRoot) {
        this.columnLineageService.getColumnLineageByTableAndColumn(columnChild.parent.SourceTable.DatabaseName, columnChild.parent.SourceColumn.DatabaseName)
            .subscribe(lineageChildren => {
                if (lineageChildren && columnChild.children.length == 0) {
                    for (let lineageChild in lineageChildren) {
                        // Skips pushing lineage names of column chosen
                        //if (lineageChildren[lineageChild].TargetColumn.DatabaseName != column.parent.DatabaseName) {
                        columnChild.children.push({ parent: lineageChildren[lineageChild], children: [] });
                        //}
                    }
                }

                // Expands the tree at treeColumnRoot with columnChild.children data
                this.expandTree(columnChild.children, column.parent.DatabaseName, treeColumnRoot);
            });
    }

    /**
     * Initializes the tree data
     */
    initTreeRoot() {
        this.treeData.children = [];
        // Sets root name
        this.treeData.text = 'Data Lineage';
    }

    /**
       * Initializes the tree with given schema name, table name children, and column children of those,
       * returns columnTreeRoot column leaf to use as root for lineage
       * @param column chosen column to be initialized with
       * @param tableName name of table
       * @param schemaName name of schema
       */
    initTreeBranch(column: TargetColumnFamily, tableName: string, schemaName: string) {
        if (this.treeData.children) {
            // Check if schema name is there, if it it do no push it
            let schemaIndex: number = this.treeData.children.findIndex(td => td.text == schemaName);
            if (schemaIndex < 0) {
                this.treeData.children.push({
                    'text': schemaName,
                    'tooltipText': '',
                    'children': []
                });
                schemaIndex = this.treeData.children.findIndex(td => td.text == schemaName);
            }

            // No need to check for table being there because they are retrieved as unique
            this.treeData.children[schemaIndex].children.push({
                'text': tableName,
                'tooltipText': '',
                'children': []
            });
            let tableIndex: number = this.treeData.children[schemaIndex].children.findIndex(td => td.text == tableName);

            // Root table for columns to be children of
            let columnTreeRoot = this.treeData.children.find(td => td.text == schemaName).children.find(td => td.text == tableName);

            for (let child in column.children) {
                let lineageData = column.children[child].parent;
                columnTreeRoot.children.push({
                    'text': lineageData.SourceColumn.FriendlyName1,
                    'tooltipText': lineageData.SourceTable.FriendlyName + ', ' + lineageData.SourceSystem.Name,
                    'children': []
                });
            }

            return columnTreeRoot;
        }
    }

    /**
     * Expands the tree at given column
     * @param columnChild child of column to be expanded
     * @param leafLayer current leaf layer of expansion
     */
    expandTree(columnChildren: ColumnLineageInterface[], baseColumnName: string, leafLayer) {
        if (columnChildren.length > 0 && leafLayer.children) {
            for (let child in columnChildren) {
                let lineageData = columnChildren[child].parent;
                let dataText = lineageData.TargetColumn.DatabaseName;
                let dataTooltip = lineageData.TargetTable.DatabaseName + ', ' + lineageData.TargetTable.SchemaName;
                // Gives dataText a blank value to reduce repetetive naming of base column
                if (dataText == baseColumnName) {
                    dataTooltip = dataText + ', ' + lineageData.TargetTable.DatabaseName + ', ' + lineageData.TargetTable.SchemaName;
                    dataText = '...';
                } else if (lineageData.TargetColumn && lineageData.TargetTable) {
                    leafLayer.children.push({
                        'text': dataText,
                        'tooltipText': dataTooltip,
                        'children': []
                    });
                } else {
                    leafLayer.children.push({
                        'text': lineageData.SourceColumn.FriendlyName1,
                        'tooltipText': lineageData.SourceTable.FriendlyName + ', ' + lineageData.SourceSystem.Name,
                        'children': []
                    });
                }
            }

            // Recurs for every column child and progresses through the family
            for (let child in columnChildren) {
                if (columnChildren[child].children.length > 0) {
                    this.recursiveCount++;
                    this.expandTree(columnChildren[child].children, baseColumnName, leafLayer.children[child]);
                }
            }
        }

        // If all expandTree functions have finished then drawTree will fire
        if (--this.recursiveCount <= 0) {
            this.collapsibleTree.drawTree();
        }
    }

}
