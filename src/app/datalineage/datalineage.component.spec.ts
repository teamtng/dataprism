import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DatalineageComponent } from './datalineage.component';

describe('DatalineageComponent', () => {
    let component: DatalineageComponent;
    let fixture: ComponentFixture<DatalineageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DatalineageComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DatalineageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
