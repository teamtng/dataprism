export class TreeData {
    text: string;
    tooltipText: string;
    children: TreeData[];

    constructor() {
        this.text = '';
        this.tooltipText = '';
    }
}
