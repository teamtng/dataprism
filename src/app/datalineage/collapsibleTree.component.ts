import { Component, OnInit, Input } from '@angular/core';
import * as d3 from 'd3';
import { TreeData } from './tree-data';

@Component({
    selector: 'dp-collapsible-tree',
    templateUrl: './collapsibleTree.component.html'
})

export class CollapsibleTreeComponent implements OnInit {

    // Initial tree data
    @Input() treeData: TreeData
    width: number; height: number; duration: number; i: number;
    margin; root; tree; svg;

    constructor() { }

    ngOnInit() {
        // Layout values for the tree diagram
        this.width = 900; this.height = 600;
        this.margin = { top: 15, right: 200, bottom: 30, left: 50 };

        // Time length of transition
        this.duration = 750;
        // Index of nodes
        this.i = 0;


        // Root of hierarchy of treeData
        this.root = d3.hierarchy(this.treeData, function (d) { return d.children; });

        // d3 tree of size
        this.tree = d3.tree().size([this.height - this.margin.top - this.margin.bottom, this.width - this.margin.left - this.margin.right]);

        // svg object appended onto html
        this.svg = d3.select('#collapsibleTreeArea').append('svg')
            .attr('width', this.width)
            .attr('height', this.height)
            .append('g')
            .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
    }

    /**
     * Adds header with column name chosen
     * @param columnName name of column chosen to display
     */
    addHeader(columnName) {
        if (columnName) {
            $('#collapsibleTreeHeader').text(columnName);
        }
    }

    /**
     * Draws d3 js tree structure
     * @param source - data source to update table
     */
    drawTree(source?) {
        if (!source) {
            // Sets source as root if no source is defined
            this.root = d3.hierarchy(this.treeData, function (d) { return d.children; });
            this.root.x0 = this.height / 2;
            this.root.y0 = 0;

            source = this.root;
        }

        // Assigns the x and y position for the nodes
        const rootData = this.tree(this.root);

        // Compute the new tree layout.
        const nodes = rootData.descendants(),
            links = rootData.descendants().slice(1);

        // Normalize for fixed-depth.
        nodes.forEach(function (d) { d.y = d.depth * 180 });

        // ****************** Nodes section ***************************

        // Update the nodes...
        const node = this.svg.selectAll('g.node')
            .data(nodes, d => d.id = d.id ? d.id : ++this.i);

        // Enter any new modes at the parent's previous position.
        const nodeEnter = node.enter().append('g')
            .attr('class', 'node')
            .attr('transform', function (d) { return 'translate(' + source.y0 + ',' + source.x0 + ')'; })
            .on('click', d => this.click(d));

        // Add Circle for the nodes
        nodeEnter.append('circle')
            .attr('class', 'node')
            .attr('r', 1e-6)
            .style('fill', function (d) { return d._children ? 'lightsteelblue' : '#fff'; });

        // Add labels for the nodes
        nodeEnter.append('text')
            .attr('dy', -10)
            .attr('text-anchor', 'middle')
            .text(function (d) { return d.data.text; });

        // Add tooltips to nodes
        nodeEnter.append('svg:title')
            .text(function (d) { return d.data.tooltipText; });

        // UPDATE
        const nodeUpdate = nodeEnter.merge(node);

        // Transition to the proper position for the node
        nodeUpdate.transition()
            .duration(this.duration)
            .attr('transform', function (d) {
                return 'translate(' + d.y + ',' + d.x + ')';
            });

        // Update the node attributes and style
        nodeUpdate.select('circle.node')
            .attr('r', 5)
            .style('fill', function (d) { return d._children ? 'lightsteelblue' : '#fff'; })
            .attr('cursor', 'pointer');


        // Remove any exiting nodes
        const nodeExit = node.exit().transition()
            .duration(this.duration)
            .attr('transform', function (d) {
                return 'translate(' + source.y + ',' + source.x + ')';
            })
            .remove();

        // On exit reduce the node circles size to 0
        nodeExit.select('circle')
            .attr('r', 1e-6);

        // On exit reduce the opacity of text labels
        nodeExit.select('text')
            .style('fill-opacity', 1e-6);

        // ****************** links section ***************************

        // Update the links...
        const link = this.svg.selectAll('path.link')
            .data(links, function (d) { return d.id; });

        // Enter any new links at the parent's previous position.
        const linkEnter = link.enter().insert('path', 'g')
            .attr('class', 'link')
            .attr('d', function (d) {
                const o = { x: source.x0, y: source.y0 }
                return new CollapsibleTreeComponent().diagonal(o, o)
            });

        // UPDATE
        const linkUpdate = linkEnter.merge(link);

        // Transition back to the parent element position
        linkUpdate.transition()
            .duration(this.duration)
            .attr('d', function (d) { return new CollapsibleTreeComponent().diagonal(d, d.parent) });

        // Remove any exiting links
        const linkExit = link.exit().transition()
            .duration(this.duration)
            .attr('d', function (d) {
                const o = { x: source.x, y: source.y }
                return new CollapsibleTreeComponent().diagonal(o, o)
            })
            .remove();

        // Store the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    /**
     * Toggles data children on click
     * @param d - data to toggle
     */
    private click(d) {
        if (!d.children) {
            d.children = d._children;
            d._children = null;
        } else {
            d._children = d.children;
            d.children = null;
        }
        this.drawTree(d);
    }

    /**
     * Creates a curved path from parent to the child nodes
     * @param s - source data
     * @param d - data
     */
    private diagonal(s, d) {
        const path = `M ${s.y} ${s.x}
                    C ${(s.y + d.y) / 2} ${s.x},
                    ${(s.y + d.y) / 2} ${d.x},
                    ${d.y} ${d.x}`;
        return path;
    }

    /**
     * Initializes the tree data
     */
    initTreeRoot() {
        // this.addHeader(column);
        this.treeData.children = [];
        // Sets root name
        this.treeData.text = 'Data Lineage';
    }

}