import { Component, OnInit, EventEmitter } from '@angular/core';
import { Schema } from 'app/schema/schema';
import { Table } from 'app/tables/table';
import { ColumnLineage } from 'app/tables/columnlineage'
import { SchemaService } from '../schema/schema.service';
import { TableService } from '../tables/table.service';
import { TargetColumn } from 'app/tables/targetcolumn';
import { TargetColumnService } from 'app/tables/targetcolumn.service';
import { ColumnLineageService } from 'app/tables/columnlineage.service';
import { CollapsibleTreeComponent } from 'app/datalineage/collapsibleTree.component';
import { SchemaFamily, TableFamily, TargetColumnFamily, ColumnLineageInterface } from './datalineage';

@Component({
    selector: 'dp-datalineage',
    templateUrl: './datalineage.component.html',
    providers: [SchemaService, TableService, TargetColumnService, ColumnLineageService, CollapsibleTreeComponent]
})

export class DatalineageComponent implements OnInit {
    schemaFamilies: SchemaFamily[];
    columnSelected: TargetColumnFamily;

    constructor(private schemaService: SchemaService, private tableService: TableService, 
        private targetColumnService: TargetColumnService, private columnLineageService: ColumnLineageService) { }

    ngOnInit() {
        this.schemaFamilies = [];
        this.getSchemas();
    }

    /**
     * Gets all schemas using schema service,
     * then adds them to the schemaFamiilies
     */
    getSchemas() {
        this.schemaService.getSchemas()
            .subscribe(schemas => {
                if (schemas && this.schemaFamilies.length == 0) {
                    for (let schema in schemas) {
                        this.schemaFamilies.push({ parent: schemas[schema], children: [] });
                    }
                }
            });
    }

    /**
     * Calls tableService and to get all tables associated with the given schema and filters the lineages,
     * then adds all tables to table families, and the resulting families to schemaFamily children
     * @param schema schema to get target tables by
     */
    getTables(schema: Schema) {
        this.tableService.getTablesbySchemaFilterLineages(schema)
            .subscribe(tables => {
                if (tables && this.schemaFamilies.find(sf => sf.parent == schema).children.length == 0) {
                    for (let table in tables) {
                        this.schemaFamilies.find(sf => sf.parent == schema)
                            .children.push({ parent: tables[table], children: [] });
                    }
                }
            });
    }

    /**
     * Calls targetColumnService to get all target columns of the target table by id,
     * then adds all columns go to tableFamilies as children
     * @param table target table to get columns by using the table id
     */
    getTargetColumns(table: Table) {
        this.targetColumnService.getTargetColumnsByTargetTableFilterLineages(table.Id)
            .subscribe(columns => {
                if (columns && this.schemaFamilies.find(sf => sf.parent.SchemaName == table.SchemaName).children.find(tf => tf.parent == table).children.length == 0) {
                    for (let column in columns) {
                        this.schemaFamilies.find(sf => sf.parent.SchemaName == table.SchemaName)
                            .children.find(tf => tf.parent == table)
                            .children.push({ parent: columns[column], children: [] });
                    }
                }
            });
    }

    /**
     * Expand/unexpands tree based on what object in the tree is clicked
     * @param $event mouse click event
     */
    handleListItemClicked($event: MouseEvent) {
        let target = $event.target,
            parent = $(target).parent(),
            tree = parent.children('ul.tree');

        if (parent.hasClass('expanded')) {
            parent.removeClass('expanded');
            tree.slideUp(200);
        } else {
            parent.addClass('expanded');
            tree.slideDown(200);
        }
    };

    /**
     * Expands/unexpands column based on choice
     * @param $event mouse click event
     */
    handleColumnClicked($event: MouseEvent) {
        let target = $event.target,
            parent = $(target).parent();

        if ($('.lineageSchemaColumn').hasClass('expanded')) {
            $('.lineageSchemaColumn').removeClass('expanded');
            parent.addClass('expanded');
        } else {
            parent.addClass('expanded');
        }
    };

    /**
     * Handles schema being clicked in selector
     * @param schema name of schema selected
     * @param $event mouse click event
     */
    schemaClicked(schema: Schema, $event: MouseEvent) {
        $event.stopPropagation();
        //this.handleListItemClicked($event, method);
        let target = $event.target,
        parent = $(target).parent(),
        tree = parent.children('ul.tree');

        if (parent.hasClass('expanded')) {
            parent.removeClass('expanded');
            tree.slideUp(200);
        } else {
            parent.addClass('expanded');
            tree.slideDown(200);
            this.getTables(schema);
        }
        
    }

    /**
     * Handles table being clicked in selector
     * @param table table object selected
     * @param $event mouse click event
     */
    tableClicked(table: Table, $event: MouseEvent) {
        $event.stopPropagation();
        //this.handleListItemClicked($event);
        //this.getTargetColumns(table);
        let target = $event.target,
        parent = $(target).parent(),
        tree = parent.children('ul.tree');
        if (parent.hasClass('expanded')) {
            parent.removeClass('expanded');
            tree.slideUp(200);
        } else {
            parent.addClass('expanded');
            tree.slideDown(200);
            this.getTargetColumns(table);
        }
    };

    /**
     * Handles column being clicked in selector.
     * Inits the tree root, gets all tables and schemas with column name then gets lineages
     * @param columnSelected column object selected
     * @param $event mouse click event
     */
    columnClicked(columnSelected: TargetColumnFamily, $event: MouseEvent) {
        $event.stopPropagation();
        this.handleColumnClicked($event);
        this.columnSelected = columnSelected;
    };

}
