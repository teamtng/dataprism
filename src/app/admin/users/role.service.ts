import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Role } from './role';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class RoleService {
    private apiPath = 'api/Roles';

    roles: Role[];

    constructor(private http: Http) { }

    /**
     * Gets all available user roles
     */
    getRoles(): Observable<any> {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);

    }

}
