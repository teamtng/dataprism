import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { Subject } from 'rxjs/Rx';
import { UserService } from 'app/admin/users/user.service';
import { User } from 'app/admin/users/user';
import { Role } from 'app/admin/users/role';

@Component({
    selector: 'dp-add-user-modal',
    templateUrl: './add-user-modal.component.html',
    providers: [UserService, SnackBarHandler]
})

export class AddUserModalComponent implements OnInit {

    @Output() userChange = new EventEmitter();

    User: User;
    roles: Role[];
    roleSelected: Role[] = [];

    constructor(private userService: UserService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() { 
        this.clearUser();
    }

    clearUser() {
        this.User = new User();
        this.roleSelected = [];
    }

    loadRoles(roles: Role[]) {
        this.roles = roles;
    }

    /**
     * Populates User with the selected user to edit.
     * Then puts the Users Roles into roleSelected.
     * @param User
     */
    loadUser(user: User) {
        this.User.Id = user.Id;
        this.User.FirstName = user.FirstName;
        this.User.LastName = user.LastName;
        this.User.Username = user.Username;
        this.User.Roles = user.Roles;
        this.User.Roles == null ? this.User.Roles = [] : this.roleSelected = this.User.Roles;
    }

    /**
     * Adds or removes a role from roleSelected
     * @param role
     */
    roleChose(role: Role) {
        let index = -1;
        index = this.roleSelected.findIndex(selected => role.Id == selected.Id);
        index > -1 ? this.roleSelected.splice(index, 1) : this.roleSelected.push(role);
    }

    /**
     * Returns bool of if role is in roleSelected
     * @param role
     */
    roleExists(role: Role): boolean {
        return this.roleSelected.find(selected => role.Id == selected.Id) ? true : false;
    }

    /**
     * Calls UserService to save new user with roles if input fields are valid
     */
    saveUser(user: User) {
        if (!user.Id) {
            user.Id = -1;
            user.Password = window.btoa(user.Password);
        }
        user.Roles = this.roleSelected;
        this.userService.saveUser(user)
            .subscribe(() => {
                //this.getUsers();
                this.userChange.emit();
                this.snackBarHandler.open(this.User.FirstName + ' ' + this.User.LastName + ' has been saved.', 'success');
            }, err => console.log(err));
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    public show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }
}