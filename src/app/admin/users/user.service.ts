import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { User } from './user';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class UserService {
    private apiPath = 'api/EDWUser';

    users: User[];

    constructor(private http: Http) { }

    /**
     * Gets all Users
     */
    getUsers(): Observable<any> {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Saves a new User
     * @param User
     */
    saveUser(User): Observable<any> {
        return this.http.put(this.apiPath, JSON.stringify(User))
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Deletes a User by Id
     * @param Id id of a user
     */
    deleteUser(Id): Observable<any> {
        return this.http.delete(this.apiPath + '/?Id=' + Id)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
