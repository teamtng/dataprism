import { Role } from './role'

export class User {

    Id: number;
    UserId: number;
    Username: string;
    FirstName: string;
    LastName: string;
    Password: string;
    Roles: Role[];
    rolesString: string;

    constructor() {
        this.Username = null;
        this.Password = null;
    }

}
