import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { RoleService } from './role.service';
import { User } from './user';
import { Role } from './role';
import { Subject } from 'rxjs/Rx';
import { UtilityHandler } from 'utilities/utility';
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-users',
    templateUrl: './users.component.html',
    providers: [UserService, RoleService, SnackBarHandler]
})

export class UsersComponent implements OnInit {
    users: User[];
    roles: Role[];
    roleSelected: Role[] = [];
    User: User;

    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<User[]> = new Subject();

    constructor(private userService: UserService, private roleService: RoleService,
        private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.dtOptions = {};
        this.initTables();
    }

    /**
     * Empties out the User Object
     */
    clearUser() {
        this.User = new User();
        this.roleSelected = [];
    }

    /**
     * Calls UserService to Delete user
     * @param user
     */
    deleteUser(user: User) {
        const deletedUser = user;
        this.userService.deleteUser(user.Id).subscribe(() => {
            this.getUsers();
            this.snackBarHandler.open(deletedUser.FirstName + ' ' + deletedUser.LastName + ' was deleted.', 'success');
        });

    }

    /**
     * Calls UserService to get all users
     */
    getUsers() {
        this.userService.getUsers()
            .subscribe(users => {
                this.users = users;
                this.dtTrigger.next();
                this.loadRoleStrings();
            });
    }

    /**
     * Calls RoleService to get all roles
     */
    getRoles() {
        this.roleService.getRoles()
            .subscribe(roles => {
                this.roles = roles;
                this.loadRoleStrings();
            });
    }

    /**
     * Initializes the user and roles tables along with clearing out the current User
     */
    protected initTables() {
        // Does not call this.getUsers because it needs to listen with subscribe
        this.userService.getUsers()
            .subscribe(users => {
                this.users = users;
                this.dtTrigger.next();
                this.getRoles();
            });
        this.clearUser();
    }

    /**
     * Creates a string of all the user's roles for display
     */
    protected loadRoleStrings() {
        if (this.users[0] != null) {
            for (let i = 0; i < this.users.length; i++) {
                if (this.users[i].Roles[0] != null) {
                    this.users[i].rolesString = this.users[i].Roles[0].RoleName;
                    for (let j = 1; j < this.users[i].Roles.length; j++) {
                        this.users[i].rolesString += ', ' + this.users[i].Roles[j].RoleName;
                    }
                }
            }
        }
    }

    /**
     * Listener for user being added/edited via modal
     */
    usersChanged() {
        this.getUsers();
    }

}