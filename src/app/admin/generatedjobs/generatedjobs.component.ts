import { Component, OnInit } from '@angular/core';
import { GeneratedJobsService } from './generatedjobs.service';
import { GeneratedJob } from './generatedjobs';
import { Subject } from 'rxjs/Rx';
import { UtilityHandler } from 'utilities/utility';
import { SnackBarHandler } from 'utilities/snackbar';

import { environment } from '../../../environments/environment';

@Component({
    selector: 'dp-generated-jobs',
    templateUrl: './generatedjobs.component.html',
    providers: [GeneratedJobsService, SnackBarHandler]
})

export class GeneratedJobsComponent implements OnInit {
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<GeneratedJob[]> = new Subject();

    generatedJobs: GeneratedJob[];

    constructor(private generatedJobsService: GeneratedJobsService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.getGeneratedJobs()
    }


    /**
     * Calls GeneratedJobs to get all generated jobs
     * @author John Crabill
     */
    getGeneratedJobs() {
        this.generatedJobsService.getJobs()
            .subscribe(generatedJobs => {
                this.generatedJobs = generatedJobs;
            });
    }

    /**
     * Sends to new window for download of Generated Jobs
     * @author John Crabill
     */
    downloadLink(job){
        this.generatedJobsService.downloadJob(job.Path)
            .subscribe(data =>{
                this.downloadUri("data:application/zip;base64,"+data, job.PackageName);
            },
            err=>{
                this.snackBarHandler.open(`Failed to download ${job.PackageName}`, 'failure');
            })
        //window.open(environment.serviceUrl+'etl/'+job.PackageName);
    }

    
    downloadUri(uri, name){
        var link = document.createElement("a");
        link.download = name;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        //delete link;
    }

    
}