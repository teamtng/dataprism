import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class GeneratedJobsService {
    private apiPath = 'api/SSISDownload';


    constructor(private http: Http) { }

    /**
     * Gets all Users
     */
    getJobs(): Observable<any> {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    downloadJob(filepath): Observable<any>{
        return this.http.get(`${this.apiPath}?filepath=${filepath}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
