import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../auth/authentication.service';

@Component({
    'selector': 'dp-header',
    'templateUrl': './header.component.html',
    providers: [AuthenticationService],
    'styleUrls': ['./header.component.css']
})

export class HeaderComponent {

    constructor(private router: Router,
        private authService: AuthenticationService) { }

    isLoginPage() {
        return this.router.url === '/login';
    }

    logOut() {
        this.authService.signOut();
    }
}
