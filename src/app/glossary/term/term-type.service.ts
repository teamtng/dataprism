import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TermType } from './term-type';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class TermTypeService {
    private apiPath = 'api/BGTermType';

    termTypes: TermType[] = [{ Id: 1, TermId: 1, TermType: 'NOT SAME AS'},
                             { Id: 2, TermId: 2, TermType: 'SEE ALSO' },
                             { Id: 3, TermId: 3, TermType: 'CALCULATED FROM' }];

    constructor(private http: Http) { }

    getTermTypes() {
        return this.termTypes;
    }

}
