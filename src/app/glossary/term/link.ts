import { Schema } from '../../schema/schema';
import { TargetTable } from '../../tables/targettable';
import { TargetColumn } from '../../tables/targetcolumn';
import { Term } from './term';

export class Link {

    term: Term;
    schema: Schema;
    targetTable: TargetTable;
    targetColumn: TargetColumn;

    constructor(term) {
        this.term = term;
        this.schema = new Schema;
        this.targetTable = new TargetTable(-1);
        this.targetTable.Id = -1;
        this.targetColumn = new TargetColumn(-1);
        this.targetColumn.Id = -1;
    }

}