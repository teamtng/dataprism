/**
 * Modal for adding/editing a term
 * @author Colling Atkins / 10.4.17 / Overhauled ui and refactored code
 */

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Term } from '../term';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { TermService } from '../term.service';
import { TermStatus } from '../term-status';
import { TermStatusService } from '../term-status.service';
import { Category } from 'app/glossary/category/category';

@Component({
    selector: 'dp-term-modal',
    templateUrl: './term-modal.component.html',
    styleUrls: ['./term-modal.component.css'],
    providers: [TermService, TermStatusService, SnackBarHandler]
})

export class TermModalComponent implements OnInit {

    @Output() termChange = new EventEmitter();

    private Term: Term = new Term(-1);
    private ParentCategory: Category = new Category(-1);
    private TermStatuses: TermStatus[] = new Array();

    constructor(private termService: TermService, private termStatusService: TermStatusService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() { }

    /**
     * Calls the termStatus Service to get the term statuses
     */
    private getTermStatus() {
        this.TermStatuses = this.termStatusService.getTermStatus()
    }

    /**
     * Saves term
     */
    private saveTerm() {
        this.termService.saveTerm(this.Term)
            .subscribe(() => {
                this.snackBarHandler.open(`${this.Term.TermName} was saved.`, 'success');
                this.termChange.emit();
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${this.Term.TermName} failed to save.`, 'failure');
            });
    }


    /**
     * Modal Variables
     */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    show(parentCategory: Category, term?: Term) {
        this.ParentCategory = parentCategory;
        this.Term = term ? term : new Term(parentCategory.Id);

        this.getTermStatus();

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    hide() {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    onContainerClicked(event: MouseEvent) {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }
}
