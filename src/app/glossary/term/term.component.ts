/**
 * Term
 * @author Collin Atkins / 10.4.17 / Overhauled ui and refactored code
 */

import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Http, Response } from '@angular/http'
import { TermService } from './term.service';
import { Term } from './term';
import { TermType } from './term-type';
import { TermStatus } from './term-status';
import { TermTypeService } from './term-type.service';
import { TermStatusService } from './term-status.service';
import { Subject, Observable } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { SnackBarHandler } from '../../../utilities/snackbar';
import { TargetColumnService } from '../../tables/targetcolumn.service';
import { TargetColumn } from '../../tables/targetcolumn';
import { TargetTable } from '../../tables/targettable';
import { TableService } from '../../tables/table.service';

@Component({
    selector: 'dp-terms',
    templateUrl: './term.component.html',
    providers: [TermService, TermTypeService, TargetColumnService, TableService, TermStatusService],
    styleUrls: ['./term.component.css']
})

export class TermComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtTrigger: Subject<Term[]> = new Subject();

    private Term: Term = new Term(-1);
    private TermStatuses: TermStatus[] = new Array();
    private TermTypes: TermType[] = new Array();

    private TargetTable: TargetTable = new TargetTable(-1);
    private TargetColumn: TargetColumn = new TargetColumn(-1);

    constructor(private route: ActivatedRoute, private router: Router, private termService: TermService,
        private targetTableService: TableService, private termTypeService: TermTypeService, private targetColumnService: TargetColumnService,
        private termStatusService: TermStatusService, private snackBarHandler: SnackBarHandler) { };

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.Term.Id = +params['id'];
        });

        this.getTermTypes();
        this.getTermStatus();
        this.getTerm();
    }

    /**
     * Deletes term and routes back to parent category
     */
    private deleteTerm() {
        this.termService.deleteTerm(this.Term.Id)
            .subscribe(glossary => {
                this.snackBarHandler.open(`${this.Term.TermName} was deleted.`, 'success');
                this.router.navigate(['/glossary/category/', this.Term.Category.Id]);
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${this.Term.TermName} failed to be deleted.`, 'failure');
            });
    }

    /**
     * @param relationTerm 
     */
    private deleteRelationTerm(relationTerm) {
        this.termService.deleteRelationTerm(relationTerm.RelationId)
            .subscribe(term => {
                this.snackBarHandler.open('Ralation was deleted.', 'success');
                this.getTerm();
            }, err => {
                console.log(err);
                this.snackBarHandler.open('Relation failed to be deleted.', 'failure');
            });
    }

    /**
     * Calls the termType Service to get the term types
     */
    private getTermTypes() {
        this.TermTypes = this.termTypeService.getTermTypes();
    }

    /**
     * Calls the termStatus Service to get the term statuses
     */
    private getTermStatus() {
        this.TermStatuses = this.termStatusService.getTermStatus()
    }

    /**
     * Gets term by id
     */
    private getTerm() {
        this.rerender();

        this.termService.getTermById(this.Term.Id)
            .subscribe(term => {
                this.Term = term;
                this.getNameByColumn(this.Term.targetColumn);
                this.dtTrigger.next();
            });
    }

    /**
     * Getting table columns, currently initialized to 0, so does not run on that
     * @author John Crabill
     */
    private getNameByColumn(column) {
        if (column != 0) {
            this.targetColumnService.getColumnById(column)
                .subscribe(column => {
                    this.TargetColumn = column[0];
                    this.getNameByTable(this.TargetColumn.EDWTableId);
                }, err => console.log(err));
        }
    }

    private getNameByTable(tableId) {
        this.targetTableService.getTableById(tableId)
            .subscribe(table => {
                this.TargetTable = table[0];
            }, err => console.log(err));
    }

    /**
     * @param relatedTerm
     * @author Collin Atkins / 10.4.17 / 
     */
    private gotoRelatedTerm(relatedTerm: Term) {
        this.router.navigate(['/glossary/term/' + relatedTerm.Id]);
        this.Term = relatedTerm;
        this.getTerm();
    }

    /**
     * @author Collin Atkins / 10.4.17 / 
     */
    private gotoTargetTable(relatedTerm: Term) {
        this.router.navigate(['/schema/table/' + this.TargetColumn.EDWTableId], { queryParams: { name: this.TargetTable.DatabaseName } });
    }

    /**
     * Destroys table instance when populated
     */
    private rerender() {
        if (this.dtElement && this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
            });
        }
    }

}
