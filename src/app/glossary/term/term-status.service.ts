import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TermStatus } from './term-status';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class TermStatusService {
    private apiPath = 'api/BGTermStatus';

    termStatus: TermStatus[] = [{ Id: '1', Status: 'DRAFT' },
                                { Id: '2', Status: 'PROPOSED' },
                                { Id: '3', Status: 'IN REVIEW' },
                                { Id: '4', Status: 'PUBLISHED' }];

    constructor(private http: Http) { }

    getTermStatus() {
        return this.termStatus;
    }

}
