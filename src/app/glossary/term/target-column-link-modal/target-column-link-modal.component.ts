/**
 * Modal for adding a target column link to a term
 * @author Collin Atkins / 10.5.17 / Overhauled ui, significantly reduced complexity, improved speed, and fixed validation
 */

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Link } from '../link';
import { Term } from '../term';
import { Schema } from '../../../schema/schema';
import { TargetTable } from '../../../tables/targettable';
import { TargetColumn } from '../../../tables/targetcolumn';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { UtilityHandler } from '../../../../utilities/utility';
import { SchemaService } from '../../../schema/schema.service';
import { TableService } from '../../../tables/table.service';
import { TargetColumnService } from '../../../tables/targetcolumn.service';
import { TermService } from '../term.service';
import { Subject } from 'rxjs/Rx';

@Component({
    selector: 'dp-target-link-modal',
    templateUrl: './target-column-link-modal.component.html',
    styleUrls: ['./target-column-link-modal.component.css'],
    providers: [SchemaService, TableService, TargetColumnService, TermService, SnackBarHandler]
})

export class TargetLinkModalComponent implements OnInit {

    @Output() linkAdded = new EventEmitter();

    private Term: Term = new Term(-1);
    private Target: Link = new Link(new Term(-1));
    private Schemas: Schema[] = new Array();
    private Tables: TargetTable[] = new Array();
    private Columns: TargetColumn[] = new Array();

    constructor(private schemaService: SchemaService, private tableService: TableService, private termService: TermService,
        private targetColumnService: TargetColumnService, private snackbar: SnackBarHandler) { }

    ngOnInit() { }

    /**
     * For empty modal
     * @author John Crabill
     * @author Collin Atkins / 10.5.17 / Removed all extra initialization
     */
    private clearLink() {
        this.Target = new Link(this.Term);
        this.Schemas = new Array();
        this.Tables = new Array();
        this.Columns = new Array();
    }

    /**
     * Retrieving Schemas
     * @author John Crabill
     * @author Collin Atkins / 10.5.17 / Removed initialization
     */
    private getSchemas() {
        this.schemaService.getSchemas()
            .subscribe(schemas => {
                this.Schemas = schemas;
            });
    }

    /**
     * Gets Target Tables after narrowing tables that have Target Columns
     * @author John Crabill
     * @author Collin Atkins / 10.5.17 / Overhauled to remove glut and created new service call
     */
    private getTargetTable(schemaName: string) {console.log(schemaName);
        this.targetColumnService.getDistincTablesBySchemaName(schemaName)
            .subscribe(tables => {
                this.Tables = tables;
            }, err => console.log(err));
    }

    /**
     * General Get Target Column, does not display columns that are already linked
     * @param targetTableId
     * @author John Crabill
     * @author Collin Atkins / 10.5.17 / Overhauled to remove glut and created new service call
     */
    private getTargetColumn(targetTableId: number) {
        this.targetColumnService.getTargetColumnsByTargetTableFilterLinked(targetTableId)
            .subscribe(columns => {console.log(columns);
                this.Columns = columns;
            });
    }

    /**
     * Saves link
     * @author John Crabill
     *  @author Collin Atkins / 10.5.17 / Added snackbar feedback
     */
    private saveLink() {
        this.Target.term.targetColumn = this.Target.targetColumn.Id;
        this.termService.saveTerm(this.Target.term)
            .subscribe(tables => {
                this.snackbar.open(`Target column was linked.`, 'success');
                this.linkAdded.emit();
            }, err => {
                console.log(err);
                this.snackbar.open(`Target column failed to link.`, 'failure');
            });
    }

    /**
     * Modal Variables
     */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     * @author Collin Atkins / 10.5.17 / Moved initialization here
     */
    show(term: Term) {
        this.Term = term;

        this.clearLink();
        this.getSchemas();

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    hide() {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    onContainerClicked(event: MouseEvent) {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }
}
