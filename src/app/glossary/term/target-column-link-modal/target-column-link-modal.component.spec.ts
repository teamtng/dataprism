import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetLinkModalComponent } from './target-column-link-modal.component';

describe('TargetLinkModalComponent', () => {
  let component: TargetLinkModalComponent;
  let fixture: ComponentFixture<TargetLinkModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetLinkModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetLinkModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});