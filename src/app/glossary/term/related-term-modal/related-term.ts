import { User } from '../../../admin/users/user';

export class RelatedTerm {
    Id: number;
    RelatedTermId: number;
    RelatedTermParentId: number;
    RelatedTermChildId: number;
    RelatedTermTypeId: number;
    CreatedDate: Date;
    UpdatedDate: Date;
    CreatedUser: User;
    UpdatedUser: User;

    constructor() {
        this.Id = -1;
    }

}
