import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedTermModalComponent } from './related-term-modal.component';

describe('RelatedTermModalComponent', () => {
  let component: RelatedTermModalComponent;
  let fixture: ComponentFixture<RelatedTermModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedTermModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedTermModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
