/**
 * Modal to add a related term
 * @author Collin Atkins / 10.4.17 / Overhauled the ui and cleaned up the code
 */

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Term } from '../term';
import { Category } from '../../category/category';
import { Glossary } from '../../glossary/glossary';
import { GlossaryService } from '../../glossary/glossary.service';
import { CategoryService } from '../../category/category.service';
import { RelatedTerm } from './related-term';
import { RelatedTermService } from './related-term.service';
import { TermType } from '../term-type';
import { TermTypeService } from '../term-type.service';
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-related-term-modal',
    templateUrl: './related-term-modal.component.html',
    providers: [GlossaryService, CategoryService, RelatedTermService, SnackBarHandler, TermTypeService],
    styleUrls: ['./related-term-modal.component.css']
})

export class RelatedTermModalComponent implements OnInit {

    @Output() termRelationChange = new EventEmitter();

    ParentTerm: Term = new Term(-1);
    RelatedTerm: RelatedTerm = new RelatedTerm();

    Glossary: Glossary = new Glossary();
    Category: Category = new Category(-1);
    
    TermTypes: TermType[] = new Array();

    constructor(private glossaryService: GlossaryService, private categoryService: CategoryService, private relatedTermService: RelatedTermService,
        private termTypeService: TermTypeService, private snackbar: SnackBarHandler) { }

    ngOnInit() { }

    private clearFields() {
        this.ParentTerm = new Term(-1);
        this.RelatedTerm = new RelatedTerm();
        this.Glossary = new Glossary();
        this.Category = new Category(-1);
    }

    /**
     * Gets category by id, then filters the terms
     * @param Id
     * @author Collin Atkins / 10.4.17 / Added related term filter
     */
    private getCategoryById(Id: number) {
        this.categoryService.getCategoryById(Id)
            .subscribe(category => {
                // Removes terms already related
                if (this.ParentTerm.RelatedTerms && category.Terms.length > 0) {
                    this.ParentTerm.RelatedTerms.forEach(parentRelatedTerm => {
                        let relatedTermIndex: number = category.Terms.findIndex(term => term.Id != parentRelatedTerm.Id);
                        if (relatedTermIndex >= 0 && category.Terms.length > 0) {
                            category.Terms.splice(relatedTermIndex + 1, 1)
                        }
                    });
                }

                this.Category.Terms = category.Terms;
            }, err => console.log(err))
    }

    /**
     * Gets glossary by id
     */
    private getGlossaryById() {
        this.glossaryService.getGlossaryById(this.ParentTerm.Category.BusinessGlossaryId)
            .subscribe(glossary => {
                this.Glossary = glossary;
            }, err =>  console.log(err));
    }

    /**
     * Loads the term types into the termTypes array
     */
    private loadTermTypes() {
        this.TermTypes = this.termTypeService.getTermTypes();
    }

    /**
     * Saved relation of current term to term selected
     * @author Collin Atkins / 10.4.17 / Removed glut and moved variable saving to ngModel
     */
    private saveRelation() {
        this.relatedTermService.saveRelatedTerm(this.RelatedTerm)
            .subscribe(term => {
                this.snackbar.open(`${term.ChildTerm.TermName} was saved.`, 'success');
                this.termRelationChange.emit();
            }, err => {
                console.log(err);
                this.snackbar.open('Relation failed to save.', 'failure');
            })
    }

    /**
    * Modal Variables
    */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    show(term: Term) {
        this.clearFields();

        this.ParentTerm = term;
        this.RelatedTerm.RelatedTermParentId = this.ParentTerm.Id;

        this.getGlossaryById();
        this.loadTermTypes();

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    hide() {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    onContainerClicked(event: MouseEvent) {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
