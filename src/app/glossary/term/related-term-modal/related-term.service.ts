import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { RelatedTerm } from './related-term';
import { UserUtility } from 'utilities/user-utility';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class RelatedTermService {
    private apiPath = 'api/BGRelatedTerms';

    constructor(private http: Http, private userUtil: UserUtility) { } // private authService: AuthenticationService

    /**
     * Save the Term from modal
     * @param relatedTerm: RelatedTerm
     */
    saveRelatedTerm(relatedTerm: RelatedTerm): Observable<any> {
        relatedTerm = this.userUtil.loadUser(relatedTerm);
        return this.http.put(this.apiPath, relatedTerm)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}