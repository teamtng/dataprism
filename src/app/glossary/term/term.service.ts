import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Term } from './term';
import { UserUtility } from 'utilities/user-utility';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class TermService {
    private apiPath = 'api/BGTerm';
    private apiRelationPath = 'api/BGRelatedTerms';
    terms: Term[];

    constructor(private http: Http, private userUtil: UserUtility) { }


    deleteTerm(termId): Observable<any>{
        return this.http.delete(`${this.apiPath}?id=${termId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    deleteRelationTerm(relationTermId): Observable<any>{
        return this.http.delete(`${this.apiRelationPath}?id=${relationTermId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets Glossary Terms based on the active parameter
     * @param active: boolean
     */
    getActiveTerms(active) {
        return this.http.get(`${this.apiPath}?active=${active}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets Glossary Terms based on the active parameter
     * @param active: boolean
     */
    getTermById(termId: number) {
        return this.http.get(`${this.apiPath}?id=${termId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    getTermByTargetColumn(targetColumnId: number){
        return this.http.get(`${this.apiPath}?target=${targetColumnId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    getAllTargetColumnTerms(){
        return this.http.get(`${this.apiPath}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Save the Term from modal
     * @param term
     */
    saveTerm(term): Observable<any> {
        term = this.userUtil.loadUser(term);
        return this.http.put(this.apiPath, term)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }


}
