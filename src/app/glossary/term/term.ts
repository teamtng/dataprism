import { Category } from '../category/category';
import { User } from '../../admin/users/user';

export class Term {

    Id: number;
    TermId: number;
    TermName: string;
    TermDescription: string;
    TermStatus: string;
    TermType: number;
    CreatedDate: Date;
    UpdatedDate: Date;
    Category: Category;
    CategoryId: number;
    CreatedUser: User;
    UpdatedUser: User;
    RelatedTerms: Term[];
    targetColumn: number;

    constructor(categoryId: number) {
        this.Id = -1;
        this.TermId = this.Id;
        //this.TermStatus = '1';
        this.Category = new Category(-1);
        this.Category.Id = categoryId;
        this.TermName = '';
        this.TermDescription = '';
        this.RelatedTerms = new Array();
    }

}
