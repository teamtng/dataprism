/**
 * Business Glossary
 * @author Collin Atkins / 10.5.17 / Completed overhaul, added table, moved add button
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { Glossary } from '../glossary/glossary';
import { GlossaryService } from '../glossary/glossary.service'
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { MdSidenav } from '@angular/material';

@Component({
    selector: 'dp-business-glossary',
    templateUrl: './business-glossary.component.html',
    providers: [GlossaryService],
    styleUrls: ['./business-glossary.component.css']
})

export class BusinessGlossaryComponent implements OnInit {

    @ViewChild('glossarySideNav') glossarySideNav: MdSidenav;

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtTrigger: Subject<Glossary[]> = new Subject();

    glossaryList: Glossary[] = new Array();
    private showBusinessGlossary: boolean = false;

    constructor(private glossaryService: GlossaryService, private router: Router) { }

    ngOnInit() {
        // if (!window.location.href.split('glossary/')[1]) {
        //     this.glossarySideNav.open();
        // }

        this.showBusinessGlossary = !window.location.href.split('glossary/')[1] ? true : false;

        this.getGlossaries();
    }

    /**
     * Calls GlossaryService to get all active Glossaries
     */
    private getGlossaries() {
        this.rerender();

        this.glossaryService.getGlossaries()
            .subscribe(glossaries => {
                this.glossaryList = glossaries;
                this.dtTrigger.next();
            })
    }

    /**
     * @author Collin Atkins / 10.5.17 / 
     */
    private gotoGlossary(glossaryId?: number) {
        this.glossarySideNav.close();

        if (glossaryId) {
            this.showBusinessGlossary = false;
            this.router.navigate(['/glossary/' + glossaryId]);
        } else {
            this.showBusinessGlossary = true;
            this.router.navigate(['/glossary/']);
        }
    }

    /**
     * Destroys table instance when populated
     */
    private rerender() {
        if (this.dtElement && this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
            });
        }
    }

}
