import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Category } from './category';
import { UserUtility } from 'utilities/user-utility';
import { DataHandler } from 'utilities/data';
import { ErrorHandler } from 'utilities/error';

@Injectable()
export class CategoryService {
    private apiPath = 'api/BGCategory';

    categories: Category[];

    constructor(private http: Http, private userUtil: UserUtility) { }

    deleteCategory(categoryId): Observable<any>{
        return this.http.delete(`${this.apiPath}?id=${categoryId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all Categories
     * Unused: delete if no use is found
     */
    getCategories(): Observable<any> {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets a Category by Id with all associated terms
     * @param Id
     */
    getCategoryById(Id: number): Observable<any> {
        return this.http.get(`${this.apiPath}?id=${Id}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    getCategoriesByGlossaryId(Id): Observable<any> {
        return this.http.get(`${this.apiPath}?id=${Id}&optional=${true}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Save the Source System from modal
     */
    saveCategory(category): Observable<any> {
        category = this.userUtil.loadUser(category);
        return this.http.put(this.apiPath, category)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
