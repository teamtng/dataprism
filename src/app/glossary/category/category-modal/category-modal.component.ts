/**
 * Modal for adding/editing a category
 * @author Collin Atkins / 10.4.17 / Overhauled ui and cleaned up code
 */

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Category } from '../category';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { UtilityHandler } from '../../../../utilities/utility';
import { CategoryService } from '../category.service';
import { Glossary } from '../../glossary/glossary';
import { GlossaryService } from '../../glossary/glossary.service';

@Component({
    selector: 'dp-category-modal',
    templateUrl: './category-modal.component.html',
    styleUrls: ['./category-modal.component.css'],
    providers: [GlossaryService, CategoryService, SnackBarHandler]
})

export class CategoryModalComponent implements OnInit {

    @Output() categoryChange = new EventEmitter();

    private ParentGlossary: Glossary = new Glossary();
    private Category: Category = new Category(-1);

    constructor(private glossaryService: GlossaryService, private categoryService: CategoryService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() { }

    /**
     * Save category
     */
    private saveCategory(category: Category) {
        this.categoryService.saveCategory(this.Category)
            .subscribe(() => {
                this.snackBarHandler.open(`${this.Category.CategoryName} was saved.`, 'success');
                this.categoryChange.emit();
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${this.Category.CategoryName} failed to save.`, 'failure');
            });
    }


    /**
     * Modal Variables
     */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    show(parentGlossary: Glossary, category?: Category) {
        this.ParentGlossary = parentGlossary;
        this.Category = category ? category : new Category(parentGlossary.Id);

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }
}
