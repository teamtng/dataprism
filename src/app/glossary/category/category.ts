
import { User } from '../../admin/users/user';
import { Term } from '../term/term';

export class Category {

    Id: number;
    CategoryId: number;
    BusinessGlossaryId: number;
    CategoryName: string;
    CategoryDescription: string;
    CreatedDate: Date;
    UpdatedDate: Date;
    CreatedUser: User;
    UpdatedUser: User;
    ParentCategory: Category;
    UsageContext: string;
    ParentCategoryId: number;
    Terms: Term[];

    constructor(glossaryId: number) {
        this.Id = -1;
        this.CategoryId = -1;
        this.BusinessGlossaryId = glossaryId;
    }
}
