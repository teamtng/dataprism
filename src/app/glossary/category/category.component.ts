/**
 * Category
 * @author Collin Atkins / 10.4.17 / Overhauled ui and refactored code
 */

import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Http, Response } from '@angular/http';
import { CategoryService } from './category.service';
import { TermService } from '../term/term.service';
import { Category } from './category';
import { Term } from '../term/term';
import { Subject, Observable } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { GlossaryService } from '../glossary/glossary.service';
import { Glossary } from 'app/glossary/glossary/glossary';
import { SnackBarHandler } from 'utilities/snackbar';
import { MdExpansionPanel } from '@angular/material';

@Component({
    selector: 'dp-category',
    templateUrl: './category.component.html',
    providers: [CategoryService, TermService],
    styleUrls: ['./category.component.css']
})

export class CategoryComponent implements OnInit {

    @ViewChild('categoryDetailPanel') categoryDetailPanel: MdExpansionPanel;

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    private dtTrigger: Subject<Term[]> = new Subject();

    private Category: Category = new Category(-1);
    private ParentGlossary: Glossary = new Glossary();

    constructor(private route: ActivatedRoute, private router: Router, private termService: TermService,
        private categoryService: CategoryService, private glossaryService: GlossaryService, private snackBarHandler: SnackBarHandler) { };

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.Category.Id = +params['id'];
        });

        this.getCategory();
    }

    /**
     * 
     */
    private deleteCategory() {
        this.categoryService.deleteCategory(this.Category.Id)
            .subscribe(glossary => {
                this.snackBarHandler.open(`${this.Category.CategoryName} was deleted.`, 'success');
                this.router.navigate(['/glossary/', this.ParentGlossary.BusinessGlossaryId]);
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${this.Category.CategoryName} failed to be deleted.`, 'failure');
            });
    }

    /**
     * Gets category by id then gets parent glossary of the category and renders the table
     */
    private getCategory() {
        this.rerender();

        this.categoryService.getCategoryById(this.Category.Id)
            .subscribe(category => {
                this.Category = category;
                this.getGlossary(this.Category.BusinessGlossaryId);
                this.dtTrigger.next();
            });
    }

    /**
     * @param glossaryId 
     */
    private getGlossary(glossaryId: number){
        this.glossaryService.getGlossaryById(glossaryId)
            .subscribe(glossary => {
                this.ParentGlossary = glossary;
            }, err => console.log(err));
    }

    /**
     * @author Collin Atkins / 10.5.17 / 
     */
    private gotoParentCategory(parentCategoryId: number) {
        this.ParentGlossary = new Glossary();
        this.Category = new Category(-1);
        this.categoryDetailPanel.close();

        this.router.navigate(['/glossary/category/' + parentCategoryId]);

        this.Category.Id = parentCategoryId;
        this.getCategory();
    }

    /**
     * @author Collin Atkins / 10.4.17 /
     */
    private saveCategory(category: Category) {
        this.categoryService.saveCategory(this.Category)
            .subscribe(() => {
                this.snackBarHandler.open(`${this.Category.CategoryName} was saved.`, 'success');
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${this.Category.CategoryName} failed to save.`, 'failure');
            });
    }

    /**
     * Destroys table instance when populated
     */
    private rerender() {
        if (this.dtElement && this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
            });
        }
    }

}