/**
 * Modal for adding/editing a glossary
 * @author Collin Atkins / 10.4.17 / Updated to angular md and html5, removed glut
 */

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Glossary } from '../glossary';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { UtilityHandler } from '../../../../utilities/utility';
import { GlossaryService } from '../glossary.service';

@Component({
    selector: 'dp-glossary-modal',
    templateUrl: './glossary-modal.component.html',
    styleUrls: ['./glossary-modal.component.css'],
    providers: [GlossaryService, SnackBarHandler]
})
export class GlossaryModalComponent implements OnInit {

    @Output() glossaryChange = new EventEmitter();

    private Glossary: Glossary = new Glossary();

    constructor(private glossaryService: GlossaryService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() { }

    /**
     * Save/Update of items
     * @author Collin Atkins / 10.4.17 / Removed checks for validation instead with html5 forms
     */
    private saveGlossary(glossary: Glossary) {
        this.glossaryService.saveGlossary(this.Glossary)
            .subscribe(() => {
                this.snackBarHandler.open(`${this.Glossary.BusinessGlossaryName} was saved.`, 'success');
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${this.Glossary.BusinessGlossaryName} failed to save.`, 'failure')
        });
    }

    /**
     * Modal Variables
     */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    show(glossary?: Glossary) {
        this.Glossary = glossary ? glossary : new Glossary();

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    hide() {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    onContainerClicked(event: MouseEvent) {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
