import { Injectable, Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Glossary } from './glossary';
import { UserUtility } from 'utilities/user-utility';
import { DataHandler } from 'utilities/data';
import { ErrorHandler } from 'utilities/error';

@Injectable()
export class GlossaryService {
    private apiPath = 'api/BGBusinessGlossary';

    glossaries: Glossary[];

    constructor(private http: Http, private userUtil: UserUtility) {
     }

    deleteGlossary(glossaryId): Observable<any>{
        return this.http.delete(`${this.apiPath}?id=${glossaryId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all Source Systems
     */
    getGlossaries(): Observable<any> {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets a Glossary by Id with all associated Categories
     * @param id
     */
    getGlossaryById(id): Observable<any> {
        return this.http.get(`${this.apiPath}?id=${id}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Save the Source System from modal
     */
    saveGlossary(glossary): Observable<any> {
        glossary = this.userUtil.loadUser(glossary);
        return this.http.put(this.apiPath, glossary)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
