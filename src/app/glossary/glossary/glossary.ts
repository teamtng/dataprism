import { Category } from '../category/category';
import { User } from '../../admin/users/user';

export class Glossary {
    Id: number;
    BusinessGlossaryId: number;
    BusinessGlossaryDescription;
    BusinessGlossaryName: string;
    Categories: Category[];
    CreatedDate: Date;
    UpdatedDate: Date;
    CreatedUser: User;
    UpdatedUser: User;

    constructor() {
        this.Id = -1;
        this.Categories = new Array();
    }
}
