/**
 * Glossary
 * @author Collin Atkins / 10.3.17 / Overhauled ui, refactored code
 */

import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { GlossaryService } from './glossary.service';
import { Glossary } from './glossary';
import { Category } from '../category/category';
import { CategoryService } from '../category/category.service';
import { Subject } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-glossary',
    templateUrl: './glossary.component.html',
    providers: [GlossaryService, CategoryService],
    styleUrls: ['./glossary.component.css']
})

export class GlossaryComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtTrigger: Subject<Category[]> = new Subject();

    private glossary: Glossary = new Glossary();

    constructor(private route: ActivatedRoute, private router: Router, private glossaryService: GlossaryService,
        private categoryService: CategoryService, private snackBarHandler: SnackBarHandler) {
        this.router.routeReuseStrategy.shouldReuseRoute = function () {
            return false;
        }
    };

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.glossary.Id = +params['id'];
        });
        
        this.getGlossaryById();
    }

    /**
     * 
     */
    private deleteGlossary() {
        this.glossaryService.deleteGlossary(this.glossary.Id)
            .subscribe(glossary => {
                this.router.navigate(['/glossary/']);
            });
    }

    /**
     * Calls the categoryService to get all Category
     */
    private getGlossaryById() {
        this.rerender();

        this.glossaryService.getGlossaryById(this.glossary.Id)
            .subscribe(glossary => {
                this.glossary = glossary;
                this.dtTrigger.next();
            }, err => console.log(err));
    }

    /**
     * Destroys table instance when populated
     */
    private rerender() {
        if (this.dtElement && this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
            });
        }
    }

    /**
     * Saved the business glossary
     * @author Collin Atkins / 10.3.17 / 
     */
    private saveGlossary() {
        this.glossaryService.saveGlossary(this.glossary)
            .subscribe(() => {
                this.snackBarHandler.open(`${this.glossary.BusinessGlossaryName} was saved.`, 'success');
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${this.glossary.BusinessGlossaryName} failed to save.`, 'failure')
        });
    }

}
