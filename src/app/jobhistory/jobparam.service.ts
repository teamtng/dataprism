/**
 * Job Param Service
 * @author Collin Atkins / 9.28.17 / Added get job params
 */

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { JobHistory } from './jobhistory';
import { JobParam } from './jobparam';
import { DataHandler } from 'utilities/data';
import { ErrorHandler } from 'utilities/error';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class JobParamService {
    //TODO need to create back end apiPath to nothing that is fine
    private apiPath = 'api/EDWJobParam';

    constructor(private http: Http) { }

    /**
     * @author Collin Atkins / 9.28.17 / 
     */
    getJobParams(tableName): Observable<any> {
        return this.http.get(`${this.apiPath}?paramId=${tableName}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Saves the given Job
     * @param jobParam: job
     */
    createJobParams(jobParam, paramName, num): Observable<any> {
        var table = new JobParam();
        table.JobName = jobParam;
        table.paramName = paramName;
        table.ParamOrder = num;
        return this.http.put(this.apiPath, table)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    deleteJobParam(param): Observable<any> {
        var data = "jobName=" + param.JobName + "&paramName=" + param.paramName;

        return this.http.delete(this.apiPath + "?" + data)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
