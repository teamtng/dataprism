import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { DataHandler } from 'utilities/data';
import { ErrorHandler } from 'utilities/error';

@Injectable()
export class UpsertService {
    private apiPath = 'api/EDWUpsert';

    constructor(private http: Http) { }

    /**
     * Gets the Upsert Types
     */
    getUpserts(){
        return this.http.get(`${this.apiPath}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
