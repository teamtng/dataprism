import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { JobHistory } from './jobhistory';
import { DataHandler } from 'utilities/data';
import { ErrorHandler } from 'utilities/error';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class JobHistoryService {
    private apiPath = 'api/EDWCtlCDC';

    jobhistory: JobHistory[];

    constructor(private http: Http) { }

    /**
     * Gets all Jobs
     */
    getJobHistory(): Observable<any> {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    // getJobHistoryByDate(startDate, endDate){
    //     return this.http.get(`${this.apiPath}?startDate=${startDate}&endDate=${endDate}`)
    //         .map(new DataHandler().extractData)
    //         .catch(new ErrorHandler().handleError);
    // }

    /**
     * Gets Job by Table Name
     * @param tableName Name of the target table
     */
    getJobHistoryByTableName(tableName): Observable<any> {
        return this.http.get(`${this.apiPath}?tableName=${tableName}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets Job by Table Name
     * @param tableName Name of the target table
     */
    // getJobHistoryByTableNameAndDate(tableName, startDate, endDate) {
    //     return this.http.get(`${this.apiPath}?tableName=${tableName}&startDate=${startDate}&endDate=${endDate}`)
    //         .map(new DataHandler().extractData)
    //         .catch(new ErrorHandler().handleError);
    // }

    /**
     * Saves the given Job
     * @param job: JobHistory
     */
    saveJob(job): Observable<any> {
        return this.http.put(this.apiPath, job)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
