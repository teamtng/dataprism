/**
 * Job History Editor Modal
 * @author Collin Atkins / 9.27.17 / Added date filtering, datatables, cleaned up code, and improved speed
 * @author Collin Atkins / 9.28.17 / Removed date filtering, datatables. Fixed many param bugs, and drastically improved speed.
 */

import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { JobHistoryService } from '../jobhistory.service';
import { JobHistory } from '../jobhistory';
import { Upsert } from '../upsert/upsert';
import { UpsertService } from '../upsert/upsert.service';
import { JobParam } from '../jobparam';
import { JobParamService } from '../jobparam.service';
import { SnackBarHandler } from 'utilities/snackbar';
import { Subject, Observable } from 'rxjs/Rx';
import { JobHistoryRow } from 'app/jobhistory/jobhistoryrow';
import { DataSource } from '@angular/cdk';
import { Subscription } from 'rxjs/Subscription';

const PARAM_NULL: string = 'GLOBAL_NULL_REPLACEMENT';
const PARAM_CONCAT: string ='GLOBAL_CONCAT_CHAR';

@Component({
    selector: 'dp-job-editor-modal',
    templateUrl: './job-editor-modal.component.html',
    providers: [JobHistoryService, JobParamService, SnackBarHandler, UpsertService],
    styleUrls: ['./job-editor-modal.component.css']
})

export class JobEditorModalComponent implements OnInit {

    job: JobHistory;

    addJobParam: JobParam[];
    upsertments: Upsert[];

    constructor(private jobService: JobHistoryService, private jobParamService: JobParamService,
        private snackbar: SnackBarHandler, private upsertService: UpsertService) { }

    ngOnInit() { }

    /**
     * Calls the Job Param Service to add the selected paramname
     * @param param 
     * @author Collin Atkins / 9.28.17 / Added get call after creation
     */
    addJob(param: JobParam) {
        var num = param.paramName == PARAM_NULL ? 1 : 2;
        this.jobParamService.createJobParams(param.JobName, param.paramName, num)
            .subscribe(() => {
                this.getJobParams();
                this.snackbar.open(param.paramName + ' was successfully added.', 'success');
            }, err => {
                this.snackbar.open('Failed to add job param.', 'failure');
            });
    }

    /**
     * Creates default job params on creation of job
     * @author Collin Atkins / 9.28.17 / Fixed to get params after adding
     */
    // private createJobParams() {
    //     this.jobParamService.createJobParams(this.job.TargetTable, PARAM_NULL, 1)
    //         .subscribe(() => {
    //             this.jobParamService.createJobParams(this.job.TargetTable, PARAM_CONCAT, 2)
    //                 .subscribe(() => {
    //                     this.saveJob();
    //                     this.getJobParams();
    //                 }, err => {
    //                     this.snackbar.open('Failed to create job param.', 'failure');
    //                 });
    //         }, err => {
    //             this.snackbar.open('Failed to create job param.', 'failure');
    //         });
    // }

    // createIfNot() {
    //     if (!this.job.CurrentProcessDateTime) {
    //         this.createJobParams();
    //     }
    // }

    /**
     * Deletes the selected jobparam
     * @param param 
     * @author Collin Atkins / 9.28.17 / Added get call after deletion
     */
    deleteJob(param: JobParam) {
        this.jobParamService.deleteJobParam(param)
            .subscribe(() => {
                this.getJobParams()
                this.snackbar.open(param.paramName + ' was successfully deleted.', 'success');
            }, err => {
                this.snackbar.open('Failed to delete job param.', 'failure');
            });
    }

    /**
     * Fills job parameters with default params
     * @param params 
     * @author Collin Atkins / 9.28.17 / Refactored for brevity
     */
    private fillJobParams(params: JobParam[]) {
        this.addJobParam = new Array();
        // var used = [];
        // if (params) {
        //     for (let param of params) {
        //         used.push(param.paramName);
        //     }
        // } 

        if (params.findIndex(p => p.paramName == PARAM_NULL) == -1) {
            let globalReplace: JobParam = new JobParam();
            globalReplace.JobName = this.job.TargetTable;
            globalReplace.paramName = PARAM_NULL;
            this.addJobParam.push(globalReplace);
        }
        if (params.findIndex(p => p.paramName == PARAM_CONCAT) == -1) {
            let globalConcat: JobParam = new JobParam();
            globalConcat.JobName = this.job.TargetTable;
            globalConcat.paramName = PARAM_CONCAT;
            this.addJobParam.push(globalConcat);
        }
    }

    /**
     * Gets the Target Tables job using jobService
     * @author Collin Atkins / 9.28.17 /
     */
    getJobByTableName(tableName: string) {
        this.jobService.getJobHistoryByTableName(tableName)
            .subscribe(job => {
                this.job = job;

                this.fillJobParams(this.job.JobParams);
            }, err => {
                console.log(err);
            });
    }

    /**
     * @author Collin Atkins / 9.28.17 /
     */
    getJobParams() {
        this.jobParamService.getJobParams(this.job.TargetTable)
            .subscribe(params => {
                this.job.JobParams = params;
                this.fillJobParams(this.job.JobParams);
            }, err => {
                this.job.JobParams = [];
                this.fillJobParams(this.job.JobParams);
            });
    }

    /**
     * Calls the upsert Service to get all the upsert Types
     * @author Collin Atkins / 9.28.17 / Added private
     */
    private getUpserts() {
        if (!this.upsertments) {
            this.upsertService.getUpserts()
                .subscribe(upserts => {
                    this.upsertments = upserts;
                }, err => console.log(err));
        }
    }

    /**
     * Saves the current job using jobService
     */
    saveJob() {
        this.jobService.saveJob(this.job)
            .subscribe(job => {
                this.job = job;
                this.snackbar.open(this.job.TargetTable + ' was successfully saved.', 'success');
            }, err => {
                this.snackbar.open('Failed to save job.', 'failure');
            })
    }

    /**
     * Modal Variables
     */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     * @author Collin Atkins / 9.28.17 / Moved initialization here
     */
    show(jobHistory?: JobHistory, tableName?: string) {
        if (!jobHistory && !tableName) {
            this.visible = false;
            return;
        }

        if (tableName) {
            this.getJobByTableName(tableName);
        } else {
            this.job = jobHistory;
            
            if (!this.job.JobParams) {
                this.getJobParams();
            }
        }

        this.getUpserts();

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
