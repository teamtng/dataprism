import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobEditorModalComponent } from './job-editor-modal.component';

describe('JobEditorModalComponent', () => {
  let component: JobEditorModalComponent;
  let fixture: ComponentFixture<JobEditorModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobEditorModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobEditorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
