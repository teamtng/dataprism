export class JobHistoryRow {

    Id: number;
    JobHistoryId: number;
    RunDateDateTime: Date;
    Status: string;
    RecordCount: number;
    TargetTable: string;
    RequestGuid: string;
    ActiveFlag: string;
    ErrorCount: number;
    ExtractCount: number;
    InsertRowCount: number;
    UpdateRowCount: number;
    ExecStartTime: Date;
    ExecStopTime: Date;
    RunTime: number;


}
