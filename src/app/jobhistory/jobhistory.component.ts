/**
 * Job History Table
 * @author Collin Atkins / 9.27.17 / Changed object of filtering to RunDate, changed httprequest, cleaned code
 */

import { Component, OnInit, Inject, Input, ViewChild } from '@angular/core';
import { Http, Response } from '@angular/http'
import { JobHistoryService } from './jobhistory.service';
import { JobHistory } from './jobhistory';
import { Subject, Observable } from 'rxjs/Rx';
import { DataTableDirective } from "angular-datatables";
import { DatePipe } from '@angular/common';

@Component({
    selector: 'dp-jobhistory',
    templateUrl: './jobhistory.component.html',
    providers: [JobHistoryService]
})

export class JobHistoryComponent implements OnInit {

    @ViewChild(DataTableDirective)
    private dtElement: DataTableDirective;

    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<JobHistory[]> = new Subject();

    jobs: JobHistory[];
    backupJobs: JobHistory[];

    startDate: Date;
    endDate: Date;

    constructor(private jobHistoryService: JobHistoryService) { }

    ngOnInit() {
        this.dtOptions = {};

        this.startDate = null;
        this.setEndDate(new Date());

        this.getJobHistory();
    }

    /**
     * @param date 
     * @author Collin Atkins / 9.27.17 /
     */
    private formatDate(date: Date): string {
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }

    /**
     * @author Collin Atkins / 9.27.17 /
     */
    filterByDate() {
        this.jobs = this.backupJobs;

        this.rerender();

        this.jobs = this.jobs.filter(j => j.LastRunDate >= this.startDate && j.LastRunDate <= this.endDate);
        this.dtTrigger.next();
    }

    /**
     * Calls JobHistoryService to get job history
     * @author Collin Atkins / 9.27.17 / Added LastRunDate recorded to object for filtering
     */
    getJobHistory() {
        this.jobHistoryService.getJobHistory()
            .subscribe(jobs => {
                this.jobs = jobs;
                this.backupJobs = jobs;
                this.jobs.forEach(job => {
                    if (job.JobHistorys) {
                        job.LastRunDate = new Date(job.JobHistorys[job.JobHistorys.length - 1].RunDateDateTime);
                    }
                });
                this.dtTrigger.next();
            });
    }

    /**
     * @author Collin Atkins / 9.25.17 /
     */
    // getJobHistoryByDate() {
    //     if (this.jobs) {
    //         this.rerender();
    //     }
    //     this.jobHistoryService.getJobHistoryByDate(this.formatDate(this.startDate), this.formatDate(this.endDate))
    //         .subscribe(jobs => {
    //             this.jobs = jobs;
    //             this.jobs.forEach(job => {
    //                 if (job.JobHistorys) {
    //                     job.LastRunDate = job.JobHistorys[job.JobHistorys.length - 1].RunDateDateTime;
    //                 }
    //             })
    //             this.dtTrigger.next();
    //         });
    // }

    /**
     * Destroys table instance when populated
     */
    private rerender() {
        if (this.dtElement && this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
            });
        }
    }

    /**
     * Resets the date objects and sets jobs to backupJobs (unfiltered)
     * @author Collin Atkins / 9.27.17 /
     */
    resetDateFilter() {
        this.startDate = null;
        this.setEndDate(new Date());

        this.rerender();

        this.jobs = this.backupJobs;

        this.dtTrigger.next();
    }

    /**
     * @param date 
     * @author Collin Atkins / 9.25.17 /
     */
    setEndDate(date: Date) {
        this.endDate = date;
        // Max hour, minute, second of day
        this.endDate.setHours(23, 59, 59, 59);
    }

    /**
     * @param date 
     * @author Collin Atkins / 9.25.17 /
     */
    setStartDate(date: Date) {
        this.startDate = date;
    }

}