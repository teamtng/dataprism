import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';
import { Router } from '@angular/router';
import { TokenService } from '../auth/token.service';
import { User } from '../admin/users/user';
import { SnackBarHandler } from 'utilities/snackbar'

@Component({
    selector: 'dp-login',
    templateUrl: './login.component.html',
    providers: [AuthenticationService, TokenService, SnackBarHandler],
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    loginForm: User;

    constructor(private authService: AuthenticationService,
        private router: Router,
        private tokenService: TokenService,
        private snackbar: SnackBarHandler) { }

    ngOnInit() {
        this.loginForm = new User();
        this.checkAuth();
    }

    /**
     * Checks to see if a valid Authentication token exists
     */
    checkAuth() {
        this.authService.checkToken();
    }

    /**
     * Controls whether or not the login button is disabled
     * Checks if the username and password have been filled
     */
    loginValid() {
        return this.loginForm.Username && this.loginForm.Password;
    }

    /**
     * Sets the Token to the jwt received from the service
     * Redirects to the dashboard if the login was successfull
     * @param data Response from AuthController
     */
    processLogin(data) {
        this.authService.setTokenStatus(true);
        this.authService.user = JSON.parse(data._body);
        setTimeout(() => this.authService.processJWTData(), 2000);
        if (this.authService.login_redirect) {
            this.snackbar.open('Wrong Username or Password', 'failure');
            setTimeout(() => this.router.navigateByUrl('/login'), 2000);
        } else {
            this.snackbar.open('Success... Redirecting...', 'success');
            setTimeout(() => this.router.navigateByUrl('/'), 2000);
        }
    }

    /**
     * Takes credentials from loginForm then attempts login via authService
     */
    tryLogin() {
        const newLogin = {
            username: this.loginForm.Username,
            password: btoa(this.loginForm.Password)
        };
        this.authService.login(newLogin).toPromise()
            .then(data => {
                this.processLogin(data)
            },
            err => {
                console.log(err);
                this.snackbar.open('No connection.', 'failure');
                this.loginForm.Password = null;
            });
    };

}
