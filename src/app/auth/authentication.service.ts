import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService, CookieOptions } from 'ngx-cookie';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TokenService } from './token.service';
import { ErrorHandler } from 'utilities/error';
import { User } from '../admin/users/user'
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {
    private apiPath = 'api/Auth';

    token: string;
    currentPath: string;
    login_redirect: string;

    user: User;

    private token_is_validated = new BehaviorSubject<boolean>(false); // true is your initial value
    token_is_validated$ = this.token_is_validated.asObservable();

    token_is_validating = false;
    token_user: any = undefined;
    // token_levels: string[];
    // token_features: string[];

    constructor(private http: Http,
        private cookieService: CookieService,
        private tokenService: TokenService,
        private router: Router) {
        this.checkToken();
    }

    /**
     * Checks for a token and redirects to the homepage if it's found
     */
    private checkAuthPath() {
        if (this.router.url === '/login' && this.getTokenStatus()) {
            this.router.navigateByUrl('/');
        }
    }

    /**
     * Checks to see if the token is valid
     * Redirects to the login screen if it isn't
     */
    public checkToken() {
        this.token = this.tokenService.getSessionToken().rawToken;
        if (!this.token) { // token not found
            // check if callback is needed
            if (this.router.url !== '/login') {
                this.navigateToLoginScreen();
            }
        } else {
            this.token_is_validating = true;
            this.validateToken();
        }
    }

    /**
     * Returns the token cookie
     */
    getTokenStatus() {
        return this.token_is_validated.getValue();
    }

    /**
     * Sends a login request to the api with the login credentials
     * @param loginForm - login credentials including username and password
     */
    login(loginForm) {
        return this.http.post(this.apiPath, loginForm);
    }

    /**
     * Redirects to the login screen
     */
    private navigateToLoginScreen() {
        this.router.navigateByUrl('/login');
    }

    /**
     * Processes the token information
     */
    public processJWTData() {
        if (!this.token_user) {
            const tokenObj = this.tokenService.getSessionToken().payload;
            this.token_user = {
                Username: tokenObj.Username,
                Name: tokenObj.Name,
                Id: tokenObj.UserId
            };
            // this.authService.token_levels = tokenObj.Levels.split("||");
            // this.authService.token_features = tokenObj.Features.split("||");
        }
    }

    /**
     * Checks if the token is valid
     * @param status
     */
    setTokenStatus(status: boolean) {
        if (this.token_is_validated.getValue() !== status) { // prevent duplicate event trigger
            this.token_is_validated.next(status);
        }
    }

    /**
     * Removes the token cookie and redirects to the login screen
     */
    public signOut() {
        this.tokenService.clearSessionToken();
        this.setTokenStatus(false);
        this.token_user = undefined;
        this.checkToken();
    }

    /**
     * Calls the api to check if a there is a valid token cookie
     */
    private validateToken() {
        return this.http.get(this.apiPath).toPromise()
            .then(res => {
                this.processJWTData();
                this.token_is_validating = false;
                this.setTokenStatus(true);
                this.checkAuthPath();
            }).catch(err => {
                this.token_is_validating = false;
                this.setTokenStatus(false);
                this.navigateToLoginScreen();
                new ErrorHandler().handleError(err);
            });
    }

}
