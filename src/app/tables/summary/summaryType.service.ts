import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SummaryType } from './SummaryType';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';


@Injectable()
export class SummaryTypeService {
    private apiPath = 'api/EDWSummaryType';

    constructor(private http: Http) { }

    /**
     * Gets all Summary Types
     * @author Nash Lindley
     */
    getSummaryTypes() {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
