export class SummaryType {
    Id: number;
    SummaryTypeId: number;
    SummaryType: string;
    SummaryTypeDescription: string;
}
