import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';


@Injectable()
export class SummaryService {
    private apiPath = 'api/EDWSummary';

    constructor(private http: Http) { }

    /**
     * Gets all Summaries of a column Lineage
     * @author Nash Lindley
     */
    getLineageSummaries(Id) {
        return this.http.get(this.apiPath + '?ColumnLineageId=' +Id)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
