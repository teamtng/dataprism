import { Component, Input, OnInit } from '@angular/core';
import { ColumnLineage } from '../../columnlineage';
import { SummaryService } from './summary.service';
import { SummaryTypeService } from '../../summary/summarytype.service';
import { SummaryType } from '../../summary/summarytype';

@Component({
    selector: 'dp-summary-selector',
    templateUrl: './summary-selector.component.html',
    providers: [SummaryTypeService, SummaryService],
    styleUrls: ['./summary-selector.component.css']
})
export class SummarySelectorComponent implements OnInit {

    @Input() columnLineage: ColumnLineage;

    summaryTypes: SummaryType[];

    constructor(private summaryTypeService: SummaryTypeService, private summaryService: SummaryService) { }

    ngOnInit() {
        this.getSummaries();//this.columnLineage.Summaries = this.columnLineage.Summaries || [];
    }

    /**
     * Calls the SummaryTypeService to get all available SummaryTypes
     */
    getSummaryTypes() {
        this.summaryTypeService.getSummaryTypes()
            .subscribe(summaryTypes => { this.summaryTypes = summaryTypes; });
    }

    /**
     * Calls Get Summary Service to get the summary for this column lineage
     * @author Nash Lindley /9/28/2017
     */
    getSummaries(){
        this.summaryService.getLineageSummaries(this.columnLineage.Id).subscribe(summaries => {
            this.columnLineage.Summaries = summaries || [];
            this.getSummaryTypes();
        },  err=>{
            this.columnLineage.Summaries = [];
            this.getSummaryTypes();
        })
    }

    /**
     * Adds or removes a role from roleSelected
     * @param role
     */
    summaryChose(summary: SummaryType) {
        let index = -1;
        index = this.columnLineage.Summaries.findIndex(selected => summary.Id === selected.Id);
        index > -1 ? this.columnLineage.Summaries.splice(index, 1) : this.columnLineage.Summaries.push(summary);
    }

    /**
     * Returns bool of if summary is in summarySelected
     * @param role
     */
    summaryExists(summary: SummaryType): boolean {
        return this.columnLineage.Summaries.find(selected => summary.Id === selected.Id) ? true : false;
    }

}
