import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinAliasSelectorComponent } from './join-alias-selector.component';

describe('JoinAliasSelectorComponent', () => {
  let component: JoinAliasSelectorComponent;
  let fixture: ComponentFixture<JoinAliasSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinAliasSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinAliasSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
