import { Component, Input, OnInit } from '@angular/core';
import { ColumnLineage } from '../../columnlineage';
import { SourceTableJoinService } from '../../sourcetablejoin/sourcetablejoin.service'
import { SourceTableJoin } from '../../sourcetablejoin/sourcetablejoin';

@Component({
    selector: 'dp-join-alias-selector',
    templateUrl: './join-alias-selector.component.html',
    providers: [SourceTableJoinService],
    styleUrls: ['./join-alias-selector.component.css']
})
export class JoinAliasSelectorComponent implements OnInit {

    @Input() columnLineage: ColumnLineage;
    @Input() tableId: number;
    @Input() systemId: number;

    sourceTableJoins: SourceTableJoin[] = [];

    constructor(private sourceTableJoinService: SourceTableJoinService) { }

    ngOnInit() {
        if (this.columnLineage.SourceTableId > 0) {
            this.getSourceTableJoins();
        }
    }

    /**
     * Calls SourceTableJoinService in order to get all joins
     * related to the column lineage's source table
     */
    getSourceTableJoins() {
        this.sourceTableJoinService.getSourceTableJoinBySourceTable(this.tableId, this.systemId, this.columnLineage.SourceTableId)
            .subscribe(joins => {
                this.sourceTableJoins = joins.MappedJoinedTables;
                this.sourceTableJoins.forEach(join => join.show = false);
            })
    }

    /**
     * Sets the columnLineage's sourceTableAlias to the selected join
     * @param join
     */
    selectColumnLineageJoin(join) {
        this.columnLineage.SourceTableAlias = join.Id;
        this.toggleJoinDetails(join);
    }

    /**
     * Toggles the display of the join condition details for a specific join
     * @param join
     */
    toggleJoinDetails(join) {
        join.show = !join.show;
    }

}
