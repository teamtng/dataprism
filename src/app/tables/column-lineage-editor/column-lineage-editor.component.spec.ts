import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnLineageEditorComponent } from './column-lineage-editor.component';

describe('ColumnLineageEditorComponent', () => {
  let component: ColumnLineageEditorComponent;
  let fixture: ComponentFixture<ColumnLineageEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColumnLineageEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumnLineageEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
