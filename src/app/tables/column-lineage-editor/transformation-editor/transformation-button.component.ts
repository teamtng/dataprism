import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Transformation } from './transformation'

@Component({
    selector: 'transformation-button',
    templateUrl: './transformation-button.component.html'
})

export class TransformationButtonComponent {
    @Input() transformations: Transformation;
    @Input() transformIndex: Transformation;

    @Output() indexChange = new EventEmitter();

    setTransformIndex(index) {
        this.indexChange.emit({ index: index });
    }
}