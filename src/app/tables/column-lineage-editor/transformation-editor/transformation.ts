import { SnackBarHandler } from 'utilities/snackbar'

export class Transformation {
    Id: number;
    Name: string;
    Type: string;
    Params: any;
    canChange: boolean
    Index: number;
    Parent: Transformation;

    snackbar: SnackBarHandler;

    constructor(Id, Name, Type, canChange, Parent) {
        this.Id = Id;
        this.Name = Name;
        this.Type = Type;
        this.Params = [];
        this.canChange = canChange != null ? canChange : true;
        this.Index = 0;
        this.Parent = Parent;
    }

    appendArray(p, array, back) {
        return p.splice.apply(p, [p.length - back, 0].concat(array));
    }

    appendCase(Id, Name, Type) {
        if (this.Parent == null && this.Name !== 'CASE') {
            // this.snackbar.openSnackBar('Must place within a Case', 'failure');
            // Utility.displayAlert(this, "Must place within a CASE", "danger");
        } else if (this.Name === 'CASE') {
            let p = this.Params;
            if (p[p.length - 2].Name === 'ELSE') {
                if (Name === 'ELSE') {
                    // this.snackbar.openSnackBar('Can only have one ELSE per CASE', 'failure');
                    // Utility.displayAlert(this, "Can only have one ELSE per CASE", "danger");
                } else {
                    p = this.appendArray(p, [new Transformation(null, null, null, true, this)], 2);
                    this.Params[this.Params.length - 3].setTransformation(Id, Name, Type);
                }
            } else {
                p = this.appendArray(p, [new Transformation(null, null, null, true, this)], 1);
                this.Params[this.Params.length - 2].setTransformation(Id, Name, Type);
            }
        } else {
            this.Parent.appendCase(Id, Name, Type);
        }
    }

    appendExpression(Id, Name, Type) {
        if (this.Parent == null) {
            // Utility.displayAlert(this, "Must place within the expression", "danger");
        } else if (this.Name === 'WHEN' || this.Name === 'ELSE') {
            let p = this.Params;
            p = this.appendArray(p, [new Transformation(Id, Name, Type, false, this)], 1);
            p = this.Params;
            p = this.appendArray(p[p.length - 2].Params, this.expression(p[p.length - 2]), 0);
        } else {
            this.Parent.appendExpression(Id, Name, Type);
        }
    }

    appendList(Id, Name, Type) {
        if (this.Parent == null) {
            // Utility.displayAlert(this, "Must place within the expression", "danger");
        } else if (this.Name === 'WHEN' || this.Name === 'ELSE') {
            let p = this.Params;
            p = this.appendArray(p, [new Transformation(Id, Name, Type, false, this)], 1);
            p = this.Params;
            p = this.appendArray(p[p.length - 2].Params, this.list(p[p.length - 2]), 0);
        } else {
            this.Parent.appendList(Id, Name, Type);
        }
    }

    appendToList(index) {
        //if (this.Parent == null) {
            // Utility.displayAlert(this, "Must place within the expression", "danger");
        //} else if (this.Name === 'in') {
            let p = this.Parent.Params;
            p.splice(p.length - 1, 1)
            p = p.concat([this.addField(this.Parent), this.Comma(this.Parent), this.anyField(this.Parent)]); //this.appendArray(p, [this.addField(this.Parent), this.Comma(this.Parent), this.anyField(this.Parent)], 1);
       // } else {
         //   this.Parent.appendList(index.Id, index.Name, index.Type);
        //}
    }

    checkValid(canChange, curType, newType) {
        if (curType === newType) {
            return true;
        /*} else if((curType == null && newType == "Expression") || (curType == null && newType == "CurrentDate")){
            return true;*/
        } else if((curType === 'in' || curType === 'operator') && (newType === 'operator' || newType === 'in')){
            return true;
        } else if ((curType !== 'in' || curType !== 'operator') && (newType === 'operator' || newType === 'in')) {
            return false;
        } else if (!canChange && curType === 'numeric') {
            // Utility.displayAlert(this, "Must replace with a number", "danger");
            return false;
        } else if (!canChange && (curType === 'operator' || curType === 'in')) {
            // Utility.displayAlert(this, "Must replace with >, <, =, or !=", "danger");
            return false;
        } else if (!canChange) {
            // Utility.displayAlert(this, 'Can\'t change value' , 'danger');
        } else {
            return true;
        }
    }

    setIndex(i) {
        this.Index = i;
    }

    setTransformation(Id, Name, Type) {
        if (this.checkValid(this.canChange, this.Type, Type)) {
            this.Id = Id,
            this.Name = Name;
            this.Type = Type;

            switch (Type) {
                case 'Trim': 
                case 'Year': 
                case 'Month': 
                case 'Day': 
                case 'Max':
                case 'Min':
                case 'Count':
                case 'Sum':
                case 'Avg':
                case 'JulianToDate': this.Params = this.singleParam(this); break;
                case 'Substring': this.Params = this.substringParams(this); break;
                case 'Concat': this.Params = this.concat(this); break;
                case 'Nvl': this.Params = this.nvl(this); break;
                case 'CASE': this.Params = this.CASE(this);
                    this.Params[0].setTransformation(6, 'WHEN', 'WHEN');
                    break;
                case 'WHEN': this.Params = this.WHEN(this);
                    this.Params[3].setTransformation(7, 'THEN', 'THEN');
                    break;
                case 'ELSE': this.Params = this.THEN(this); break;
                case 'THEN': this.Params = this.THEN(this); break;
                case 'in':  this.spliceNext(this); this.Params = this.inn(this); break;
                //case 'Expression': this.Params = this.expression(this); break;
                default: this.Params = [];
            }
        } /*else if (this.canChange) {
            this.Id = Id;
            this.Name = Name;
            this.Type = Type;
        }*/
    }

    spliceNext = that =>{
        that.Parent.Params.splice(this.Parent.Params.indexOf(that)+1, 1);
    }

    // Function Operators
    substringParams = Parent => (
        [
            this.columnOrFunc(Parent),
            this.Comma(Parent),
            this.Numeric(Parent),
            this.Comma(Parent),
            this.Numeric(Parent),
            this.Parenthesis(Parent)
        ]
    )

    concat = Parent => (
        [
            this.columnOrFunc(Parent),
            this.Comma(Parent),
            this.columnOrFunc(Parent),
            this.Parenthesis(Parent)
        ]
    )

    singleParam = Parent => (
        [
            this.columnOrFunc(Parent),
            this.Parenthesis(Parent)
        ]
    )
    

    nvl = Parent => (
        [
            this.columnOrFunc(Parent),
            this.Comma(Parent),
            this.addField(Parent),
            this.Parenthesis(Parent)
        ]
    )


    CASE = Parent => (
        [
            new Transformation(null, null, null, true, Parent),
            this.END(Parent)
        ]
    )

    WHEN = Parent => (
        this.expression(Parent).concat(new Transformation(null, null, null, true, Parent))
    )

    THEN = Parent => (
        [
            this.addField(Parent)
        ]
    )

    expression = Parent => (
        [
            this.addField(Parent),
            this.operator(Parent),
            this.addField(Parent)//,
            //this.Parenthesis(Parent)
        ]
    )

    list = Parent => (
        [
            this.anyField(Parent), 
            this.Comma(Parent), 
            this.addField(Parent)
        ]
    )

    inn = Parent => (
        [
            this.addField(Parent),
            this.Comma(Parent),
            this.anyField(Parent)
        ]
    )

    END = Parent => (
        new Transformation('END', 'END', 'END', false, Parent)
    )

    addField = Parent => (
        new Transformation(null, 'ENTER ANY VALUE', null, true, Parent)
    )

    anyField = Parent => (
        new Transformation(null, 'ADD NEW VALUE', null, false, Parent)
    )

    columnOrFunc = Parent => (
        new Transformation(null, 'ENTER COLUMN OR FUNC', 'column', true, Parent)
    )

    operator = Parent => (
        new Transformation(null, 'ENTER OPERATOR', 'operator', false, Parent)
    )

    Comma = Parent => (
        new Transformation(',', ',', ',', false, Parent)
    )

    Numeric = Parent => (
        new Transformation(null, 'ENTER NUMBER', 'numeric', false, Parent)
    )

    Parenthesis = Parent => (
        new Transformation(35, ')', 'parenthesis', false, Parent)
    )

}
