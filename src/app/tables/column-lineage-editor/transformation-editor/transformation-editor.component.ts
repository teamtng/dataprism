/**
 * @author Collin Atkins / 10.2.17 / Removed tabs, removed tree with controls, added buttons for controls, added other minor fixes
 */

import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { ColumnLineage } from '../../columnlineage';
import { ColumnLineageService } from '../../columnlineage.service';
import { TableLineageService } from '../../tablelineage.service';
import { TransformationTypeService } from '../../transformation-type/transformation-type.service';
import { TransformationType } from '../../transformation-type/transformation-type';
import { Transformation } from './transformation';
import { TableLineage } from "app/tables/tablelineage";

@Component({
    selector: 'dp-transformation-editor',
    templateUrl: './transformation-editor.component.html',
    providers: [TableLineageService, TransformationTypeService],
    styleUrls: ['./transformation-editor.component.css']
})

export class TransformationEditorComponent implements OnInit {

    @Input() columnLineage: ColumnLineage;
    @Input() systemId: number;
    @Input() tableId: number;

    @Output() transformationChange = new EventEmitter();

    currentList = new Transformation(null, null, null, null, null);
    transformations: Transformation;
    transformIndex: Transformation;
    tableLineages: TableLineage[];

    transformOperators: TransformationType[] = [];
    transformGates: TransformationType[] = [];
    transformFunctions: TransformationType[] = [];
    transformConditionals: TransformationType[] = [];

    transformString: string;
    transformNumeric: number;

    // showOperators: boolean = false;
    // showGates: boolean = false;
    // showFunctions: boolean = false;
    // showConditionals: boolean = false;
    // showConstants: boolean = false;

    constructor(private tableLineageService: TableLineageService, private transformTypeService: TransformationTypeService) { }

    ngOnInit() {
        // Initialize base transformations
        this.transformations = new Transformation(null, null, null, null, null);
        this.transformIndex = this.transformations;
        this.currentList = new Transformation(null, null, null, null, null);

        this.getTransformationTypes();
        this.getTableLineagesAll();
    }

    private addTransformation(Id, Name, Type) {
        this.transformIndex.setTransformation(Id, Name, Type);
        this.emit();
    }

    /**
     * Adds to transformation box based on func given
     * @param func object with name, format, id, and type based on what transformation was chosen
     */
    addToTransformation(func) {
        if (func.Id != undefined) {
            if (func.Format === 'WHEN' || func.Format === 'ELSE') {
                this.transformIndex.appendCase(func.Id, func.Format, func.Type);
            } else if (func.TypeDesc === 'function' || func.TypeDesc === 'conditional') {
                this.addTransformation(func.Id, func.Format, func.Type);
            } else if (func.TypeDesc === 'Operators' && (func.Type === 'and' || func.Type === 'or')) {
                if (this.transformIndex.Type === 'andor') {
                    this.addTransformation(func.Id, func.Type, 'andor');
                } else {
                    this.transformIndex.appendExpression(func.Id, func.Type, 'andor');
                }
            } else if (func.TypeDesc === 'Operators' && func.Type === 'in') {
                this.addTransformation(func.Id, func.Type, 'in');
            } else if (func.TypeDesc === 'Operators') {
                this.addTransformation(func.Id, func.Type, 'operator');
            } else {
                this.addTransformation(func.Id, func.Name, func.Type);
            }
            this.emit();
        }
    }

    checkForEnd(cur) {
        if (cur.Parent != null && (cur.Index === cur.Params.length || cur.Index > cur.Params.length)) {
            this.currentList = cur.Parent;
            this.currentList.setIndex(this.currentList.Index + 1);
            this.checkForEnd(this.currentList);
        }
    }

    /**
     * Clears the transformation box of all columns and transformations
     */
    clearTransformation() {
        this.currentList = new Transformation(null, null, null, null, null);
        this.transformations = new Transformation(null, null, null, null, null);
        this.transformIndex = this.transformations;
        this.emit();
    }

    emit() {
        this.transformationChange.emit({ transformation: this.transformations })
    }

    /**
     * Finds the column if it exists in the set of tableLineages and adds it to the calculation array
     * @param lineages tableLineages
     * @param columnId
     */
    findTableColumn(lineages, columnId) {
        for (let i = 0; i < lineages.length; i++) {
            const sourceTable = lineages[i].SourceTable;
            if (sourceTable.SourceColumns[0].Id <= columnId && sourceTable.SourceColumns[sourceTable.SourceColumns.length - 1].Id >= columnId) {
                const result = lineages[i].SourceTable.SourceColumns.find(col => col.Id === parseInt(columnId, 10));
                if (result) {
                    this.transformIndex.setTransformation(`[${columnId}]`,
                        `[${sourceTable.FriendlyName}].[${result.FriendlyName1}]`,
                        'column')
                    break;
                }
            }
        }
    }

    /**
     * Final step of adding transformation and setting the indexes
     * @param item transform to add
     * @param array transformFunctions
     */
    formTransformation(item: string, array: TransformationType[]) {
        for (let i = 0; i < array.length; i++) {
            if (item === array[i].Type || item === array[i].Format) {
                this.addTransformation(array[i].Id, array[i].Format, array[i].Type);
                this.transformIndex.setIndex(0);
                this.currentList = this.transformIndex;
                this.transformIndex = this.transformIndex.Params[0];
                return true;
            }
        }
        return false;
    }

    /**
     * Get tableLineages with all sourceColumns
     */
    getTableLineagesAll() {
        this.tableLineageService.getTableLineageAll(this.tableId, this.systemId)
            .subscribe(tableLineages => {
                tableLineages.forEach(element => {
                    element.lineage = false;
                });

                this.tableLineages = tableLineages;

                if (this.columnLineage.Transformation) {
                    this.parseTransform();
                }
            });
    }

    /**
     * Calls the TransformationTypeService to get tranformationTypes pushed them to their respective arrays
     */
    getTransformationTypes() {
        this.transformTypeService.getTransformationTypes()
            .subscribe(types => {
                types.forEach(element => {
                    switch (element.TypeDesc) {
                        case 'Operators': {
                            if (element.Type === 'and' || element.Type === 'or') {
                                this.transformGates.push(element);
                            } else {
                                this.transformOperators.push(element)
                            }
                            break;
                        }
                        case 'function': {
                            this.transformFunctions.push(element)
                            break;
                        }
                        case 'conditional': {
                            this.transformConditionals.push(element);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                });
            })
    }

    /**
     * Called when key is pressed on numeric input field
     * @param $event
     */
    inputNumeric($event) {
        if ($event.key === 'Enter') {
            if (this.transformNumeric) {
                this.addToTransformation({ 'Id': this.transformNumeric, 'Name': this.transformNumeric, 'Type': 'numeric' });
            }
        }
    }

    /**
     * Called when key is pressed on string input field
     * @param $event
     */
    inputString($event) {
        if ($event.key === 'Enter') {
            if (this.transformString) {
                this.addToTransformation({ 'Id': this.transformString, 'Name': '\'' + this.transformString + '\'', 'Type': 'string' });
            }
        }
    }

    /**
     * Parses the transform string of columnLineage.Transformation into an array,
     *   then parses each of the array string into transform objects to add
     */
    parseTransform() {
        const transformArray = this.columnLineage.Transformation.split('|');

        let func = this.formTransformation(transformArray[0], this.transformFunctions);
        if (!func) {
            if (transformArray[0].substring(0, 1) === '[') {
                this.findTableColumn(this.tableLineages, transformArray[0].substring(1, transformArray[0].length - 1));
            } else {
                this.addTransformation(transformArray[0], transformArray[0], this.transformIndex.Type);
            }
        }
        for (let i = 1; i < transformArray.length - 1; i++) {
            this.checkForEnd(this.currentList);

            if (this.currentList.Index !== 0) {
                this.transformIndex = this.currentList.Params[this.currentList.Index];
            }
            if (transformArray[i] === 'CASE') {
                this.formTransformation(transformArray[i], this.transformFunctions);
            } else if (this.currentList.Name === 'CASE' && (transformArray[i] === 'WHEN' || transformArray[i] === 'ELSE') && (this.currentList.Index === this.currentList.Params.length - 1 && this.currentList.Index != 0)) {
                this.transformIndex.appendCase(1, transformArray[i], transformArray[i]);
                this.currentList = this.currentList.Params[this.currentList.Index];
                this.transformIndex = this.currentList.Params[this.currentList.Index];
            } else if ((this.currentList.Name === 'CASE' && transformArray[i] === 'WHEN') || (this.currentList.Name === 'WHEN' && transformArray[i] === 'THEN')) {
                this.currentList = this.currentList.Params[this.currentList.Index];
                this.transformIndex = this.currentList.Params[this.currentList.Index];
            } else if (this.currentList.Name === 'in' && transformArray[i] === 'THEN') {
                this.currentList = this.currentList.Parent.Params[this.currentList.Parent.Params.length - 1];
                this.transformIndex = this.currentList.Params[this.currentList.Index];
            } else if (this.currentList.Params[this.currentList.Index].Name === '|') {
                // Does this to essentially skip over the commas
                this.currentList.setIndex(this.currentList.Index + 1);
                i--;
                if (this.currentList.Name === 'in' && transformArray[i] !== 'THEN') {
                    this.currentList.appendToList(this.currentList.Index);
                }
            } else if (this.currentList.Name === 'WHEN' && (transformArray[i] == 'and' || transformArray[i] == 'or' || transformArray[i] == 'in')) {
                if (transformArray[i] === 'and' || transformArray[i] === 'or') {
                    this.transformIndex.appendExpression(1, transformArray[i], transformArray[i]);
                    this.currentList = this.currentList.Params[this.currentList.Index];
                    this.transformIndex = this.currentList.Params[this.currentList.Index];
                } else {
                    this.transformIndex.appendList(1, transformArray[i], transformArray[i]);
                    this.currentList = this.currentList.Params[this.currentList.Index];
                    this.transformIndex = this.currentList.Params[this.currentList.Index];
                }
            } else if (transformArray[i].substring(0, 1) === '[') {
                this.findTableColumn(this.tableLineages, transformArray[i].substring(1, transformArray[i].length - 1));
            } else if (transformArray[i].substring(transformArray[i].length - 1) === '(') {
                this.formTransformation(transformArray[i], this.transformFunctions);
            } else if (this.transformIndex.Type === 'string' || this.transformIndex.Type === 'numeric' || this.transformIndex.Type === 'column' || this.transformIndex.Type == null) {
                this.addTransformation(transformArray[i], transformArray[i], this.transformIndex.Type);
            } else if (transformArray[i] === '<' || transformArray[i] === '>' || transformArray[i] === '=' || transformArray[i] === '!=') {
                this.addTransformation(1, transformArray[i], 'operator');
                this.addTransformation(transformArray[i], transformArray[i], this.transformIndex.Type);
            } else if (transformArray[i] === ')') {
                if (this.currentList.Parent != null) {
                    this.currentList = this.currentList.Parent;
                }
            }

            if (this.currentList.Params[this.currentList.Index].Id && transformArray[i] !== 'CASE') {
                this.currentList.setIndex(this.currentList.Index + 1);
            }
        }
        this.emit();
    }

    orderIt(list) {
        list.sort(function (a, b) {
            var one = a.FriendlyName1;
            var two = b.FriendlyName1
            return one.localeCompare(two);
        });
        return list;
    }

    /**
     * Toggles lineage/transformation choice tree based on root type
     * @param parent name of tree root type
     * @param tableLineage lineage column chosen
     */
    // toggleTree(parent: string, tableLineage?) {
    //     switch (parent) {
    //         case 'tableLineage': {
    //             if (tableLineage) {
    //                 tableLineage.lineage = !tableLineage.lineage;
    //             }
    //             break;
    //         }
    //         case 'operators': {
    //             this.showOperators = !this.showOperators;
    //             break;
    //         }
    //         case 'gate': {
    //             this.showGates = !this.showGates;
    //             break;
    //         }
    //         case 'function': {
    //             this.showFunctions = !this.showFunctions;
    //             break;
    //         }
    //         case 'conditional': {
    //             this.showConditionals = !this.showConditionals;
    //             break;
    //         }
    //         case 'constants': {
    //             this.showConstants = !this.showConstants;
    //             break;
    //         }
    //         default: {
    //             break;
    //         }
    //     }
    // }

    toggleTree(tableLineage) {
        if (tableLineage) {
            tableLineage.lineage = !tableLineage.lineage;
        }
    }

    setTransformIndex(index) {
        if (index.Name === 'ADD NEW VALUE') {
            index.appendToList(index);
        } else {
            this.transformIndex = index;
        }
    }

}
