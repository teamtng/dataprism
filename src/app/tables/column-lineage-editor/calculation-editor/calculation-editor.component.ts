/**
 * @author Collin Atkins / 10.2.17 / Removed tabs, removed tree with controls, added buttons for controls, added other minor fixes
 */

import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ColumnLineage } from '../../columnlineage';
import { TableLineageService } from '../../tablelineage.service'
import { TableLineage } from '../../tablelineage'
import { UtilityHandler } from 'utilities/utility';

@Component({
    selector: 'dp-calculation-editor',
    templateUrl: './calculation-editor.component.html',
    providers: [TableLineageService, UtilityHandler],
    styleUrls: ['./calculation-editor.component.css']
})

export class CalculationEditorComponent implements OnInit {

    @Input() columnLineage: ColumnLineage;
    @Input() tableId: number;
    @Input() systemId: number;

    @Output() calcChange = new EventEmitter();

    calculation = [];
    calcIndex = -1;

    tableLineages: TableLineage[];
    calcNumeric: number;

    //showOperators: boolean = false;
    //showConstants: boolean = false;

    constructor(private tableLineageService: TableLineageService, private utility: UtilityHandler) { }

    ngOnInit() {
        this.getTableLineageWithNumerics();
    }

    /**
     * Adds an object to the calculation array
     * @param object
     */
    addToCalc(object: any) {
        if (object.Id) {
            this.calculation.splice(this.calcIndex + 1, 0, object);
            this.calcIndex++;
            this.calcChange.emit({ calculation: this.calculation })
        }
    }

    /**
     * Clears all calculations
     */
    clearAll() {
        this.calculation = [];
        this.calcIndex = -1;
        this.calcChange.emit({ calculation: this.calculation });
    }

    /**
     * Removed the object at the specific calc index
     */
    deleteSelected() {
        if (this.calculation.length > 0) {
            this.calculation.splice(this.calcIndex, 1);
            this.calcIndex--;
            this.calcChange.emit({ calculation: this.calculation });
        }
    }

    /**
     * finds the column if it exists in the set of tableLineages and adds it to the calculation array
     * @param lineages- tableLineages
     * @param column- a column Id
     */
    findTableColumn(lineages, column) {
        for (let i = 0; i < lineages.length; i++) {
            const sourceTable = lineages[i].SourceTable;
            if (sourceTable.SourceColumns[0].Id <= column && sourceTable.SourceColumns[sourceTable.SourceColumns.length - 1].Id >= column) {
                const result = sourceTable.SourceColumns.find(col => col.Id === column);
                if (result) {
                    this.calculation.push({
                        'Id': column,
                        'Name': '[' + sourceTable.FriendlyName + '].[' + result.FriendlyName1 + ']',
                        'Type': 'column'
                    });
                    break;
                }
            }
        }
    }

    /**
     * Calls the TableLineageService to get all the numerics of the tableLineages
     */
    getTableLineageWithNumerics() {
        this.tableLineageService.getTableLineageNumerics(this.tableId, this.systemId)
            .subscribe(tableLineages => {
                tableLineages.forEach(tableLineage => {
                    tableLineage.lineage = false;
                });

                this.tableLineages = tableLineages;

                if (this.columnLineage.ColumnCalculation) {
                    this.parseCalc();
                }
            });
    }

    /**
     * Called when key is pressed on numeric input field
     * @param $event
     */
    inputNumeric($event) {
        if ($event.key === 'Enter') {
            if (this.calcNumeric) {
                this.addToCalc({'Id': this.calcNumeric, 'Name': this.calcNumeric, 'Type': 'const'});
            }
        }
    }

    /**
     * Parses a comma separated columncalculation into the calculation array
     */
    parseCalc() {
        const strcalc = this.columnLineage.ColumnCalculation.split(',');
        let i = 0;
        for (i; i < strcalc.length; i++) {
            if (this.utility.isNumeric(strcalc[i])) {
                this.calculation.push({
                    'Id': parseInt(strcalc[i], 10),
                    'Name': parseInt(strcalc[i], 10),
                    'Type': 'const'
                });
            } else if (this.utility.isNumeric(strcalc[i].replace(/[^0-9]/g, ''))) {
                this.findTableColumn(this.tableLineages, parseInt(strcalc[i].replace(/[^0-9]/g, ''), 10));
            } else {
                this.calculation.push({ 'Id': strcalc[i], 'Name': strcalc[i], 'Type': 'op' });
            }
        }
        this.calcIndex = i - 1;
    }

    /**
     * sets the calc index to the clicked object
     * @param i index
     */
    setCalcIndex(i) {
        this.calcIndex = i;
    }

    /**
     * Toggles showing the table lineage
     */
    toggleTree(tableLineage) {
        tableLineage.lineage = !tableLineage.lineage;
    }

    /**
     * Toggles lineage/calculation choice tree based on root type
     * @param parent name of tree root type
     * @param tableLineage lineage column chosen
     */
    // toggleTree(parent: string, tableLineage?) {
    //     switch (parent) {
    //         case 'tableLineage': {
    //             if (tableLineage) {
    //                 tableLineage.lineage = !tableLineage.lineage;
    //             }
    //             break;
    //         }
    //         case 'operators': {
    //             this.showOperators = !this.showOperators;
    //             break;
    //         }
    //         case 'constants': {
    //             this.showConstants = !this.showConstants;
    //             break;
    //         }
    //         default: {
    //             break;
    //         }
    //     }
    // }


}
