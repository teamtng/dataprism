import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculationEditorComponent } from './calculation-editor.component';

describe('CalculationEditorComponent', () => {
  let component: CalculationEditorComponent;
  let fixture: ComponentFixture<CalculationEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculationEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculationEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
