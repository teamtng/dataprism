import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { ColumnLineage } from '../../columnlineage';

@Component({
    selector: 'dp-filter-editor',
    templateUrl: './filter-editor.component.html',
    styleUrls: ['./filter-editor.component.css']
})
export class FilterEditorComponent implements OnInit {

    @Input() columnLineage: ColumnLineage;

    constructor() { }

    ngOnInit() {
    }


}
