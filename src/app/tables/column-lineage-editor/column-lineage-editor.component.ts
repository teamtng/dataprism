/**
 * Column Lineage Editor Tabbed Modal
 * @author Collin Atkins / 10.9.17 / Removed columnName and added initialized column lineage
 */

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ColumnLineage } from '../columnlineage';
import { ColumnLineageService } from '../columnlineage.service';
import { Transformation } from './transformation-editor/transformation';
import { SnackBarHandler } from 'utilities/snackbar';
import { TableLineageService } from 'app/tables/tablelineage.service';
import { TableLineage } from 'app/tables/tablelineage';

@Component({
    selector: 'dp-column-lineage-editor',
    templateUrl: './column-lineage-editor.component.html',
    providers: [ColumnLineageService, SnackBarHandler, TableLineageService],
    styleUrls: ['./column-lineage-editor.component.css']
})

export class ColumnLineageEditorComponent implements OnInit {

    @Input() columnLineage: ColumnLineage = new ColumnLineage();
    @Input() tableId: number;
    @Input() systemId: number;
    @Input() transformation: string;
    //@Input() columnName: string;
    @Output() editorChange = new EventEmitter();

    calcString = '';
    transformResult = '';

    selectedTab: string;
    valid = true;

    runningVariableOk = 0;
    runningDivisionOk = 0;
    validCheck = true;

    constructor(private columnLineageService: ColumnLineageService, private snackBarHandler: SnackBarHandler, private tableLineageService: TableLineageService) { }

    ngOnInit() { }

    /**
     * Checks the state of the tabs and has different functionality based on the selected tab
     * @author Nash Lindley
     */
    checkTab() {
        this.valid = true;
        if (this.selectedTab === 'Calculation') {
            this.columnLineage.Transformation = null;
            if (this.calcString) {
                this.columnLineage.ColumnCalculation = this.calcString;
            } else {
                this.columnLineage.ColumnCalculation = "";
            }
        } else if (this.selectedTab === 'Transformation') {
            this.columnLineage.ColumnCalculation = null;
            if (this.transformResult.includes('null')) {
                this.columnLineage.Transformation = null;
            } else {
                this.columnLineage.Transformation = this.transformResult;
            }
        }
    }

    compileTransformation(t) {
        let deepValid = false;
        for (let i = 0; i < t.Params.length; i++) {
            if (t.Params[i].Id == null && t.Params[i].Name !== 'ADD NEW VALUE') {
                // Utility.displayAlert(this, 'Not all fields have been filled', 'danger');
                return false;
            }
            /*if (t.Params[i].Type == 'COMMA' && i + 1 < t.Params.length) {
                if (t.Params[i + 1].Name == 'ADD NEW VALUE') {
                    t.Params.splice(i, 2);
                    break;
                }
            }
            else */
            if (t.Params[i].Name === 'END') {
                this.transformResult += t.Params[i].Name + '|';
            } else if (t.Params[i].Type === 'column') {
                this.transformResult += t.Params[i].Id + '|';
            } else {
                this.transformResult += t.Params[i].Name + '|';
            }

            if (t.Params[i].Params.length > 0) {
                deepValid = this.compileTransformation(t.Params[i]);
                if (deepValid && t.Params.length == i) {
                    return deepValid;
                }
            }
        }
        return true;
    }

    /**
     * Called when the calculation array changes in the Calculation-editor component
     * @param calculation
     */
    loadCalculation(calculation) {
        let validCalc = '';
        this.calcString = '';
        this.runningDivisionOk = 0;
        this.runningVariableOk = 0;
        calculation.forEach((element, index) => {
            validCalc += element.Id;
            //this.validCheck = this.setDoubleCheck(element.Id);
            if (element.Type === 'column') {
                this.calcString += `[${element.Id}]`;
            } else {
                this.calcString += element.Id;
            }
            if (index !== calculation.length - 1) {
                this.calcString += ',';
            }
        });
        this.valid = this.validateCalculation(validCalc);
    }

    /**
     * Called when the transformations change in the transformation-editor component
     * @param transformation
     */
    loadTransformation(transformation) {
        this.transformResult = (transformation.Type === 'column' ? transformation.Id : transformation.Name) + '|';
        this.valid = this.compileTransformation(transformation);
    }

    /**
     * Calls the ColumnLineageService and save it with any changes that were made in
     * the column lineage editor
     */
    saveColumnLineage() {
        this.checkTab();
        this.columnLineageService.saveColumnLineage(this.columnLineage)
            .subscribe(columnLineage => {
                this.columnLineage = columnLineage;
                this.snackBarHandler.open('Saved column lineage', 'success');
                this.editorChange.emit(columnLineage);
            },
            err => {
                this.snackBarHandler.open('Failed to save column lineage', 'failure');
            });
    }

    setDoubleCheck(item) {
        if (this.runningDivisionOk == -1 || this.runningVariableOk == -1) {
            return false
        }
        let checker = ['/', '*', '(', ')', '+', '-'];
        let notVar = false;
        for (let check of checker) {
            if (check == item) {
                notVar = true;
                if (check == '/') {
                    this.runningDivisionOk = this.runningDivisionOk + 1;
                    if (this.runningDivisionOk > 1) {
                        this.runningDivisionOk = -1;
                        return false
                    }
                }
            }
        }
        if (this.runningVariableOk == 0 && !notVar) {
            this.runningVariableOk = this.runningVariableOk + 1;
        }
        else {
            this.runningVariableOk = 0;
        }

        if (this.runningVariableOk == 2) {
            this.runningVariableOk = -1;
            return false
        }
        return true;
    }

    /**
     * Checks to see if the calculation can be properly validated
     * @param value
     */
    validateCalculation(value) {
        try {
            const result = eval(value);
            return true;
        } catch (e) {
            return false
        }
    }

    /**
     * Sets the active tab title
     */
    setTitle(title) {
        this.selectedTab = title;
    }


    /**
       * Modal Variables
       */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */
    public show(): void {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
