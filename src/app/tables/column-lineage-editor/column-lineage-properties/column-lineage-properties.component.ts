import { Component, Input, OnInit } from '@angular/core';
import { ColumnLineage } from '../../columnlineage';
import { ColumnLineageService } from '../../columnlineage.service';

@Component({
    selector: 'dp-column-lineage-properties',
    templateUrl: './column-lineage-properties.component.html'
})
export class ColumnLineagePropertiesComponent implements OnInit {

    @Input() columnLineage: ColumnLineage;

    cdcDate: boolean;
    cdcTime: boolean;
    lookup: boolean;

    constructor() { }

    ngOnInit() {
        this.getCDC();
    }

    getCDC() {
        this.columnLineage.CdcDateFlag = this.columnLineage.CdcDateFlag || 'N';
        this.columnLineage.CdcTimeFlag = this.columnLineage.CdcTimeFlag || 'N';
        this.columnLineage.UseAsLookupKey = this.columnLineage.UseAsLookupKey || 'N';
        this.cdcDate = this.columnLineage.CdcDateFlag == 'Y';
        this.cdcTime = this.columnLineage.CdcTimeFlag == 'Y';
        this.lookup = this.columnLineage.UseAsLookupKey == 'Y';
    }

    toggleTime(flag) {
        this.columnLineage.CdcTimeFlag = flag ? 'Y' : 'N';
    }

    toggleDate(flag) {
        this.columnLineage.CdcDateFlag = flag ? 'Y' : 'N';
    }

    toggleLookup(flag) {
        this.columnLineage.UseAsLookupKey = flag ? 'Y' : 'N';
    }

}
