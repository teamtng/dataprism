export class Table {

    DatabaseName: string;
    SchemaName: string;
    Id: number;
    TablePrefix: string;
    SchemaTables: Table[];

    constructor() {
        this.Id = -1;
    }
}
