import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TargetColumn } from './targetcolumn';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class TargetColumnService {
    private apiPath = 'api/EDWColumn';

    constructor(private http: Http) { }

    /**
     * Gets all the targetColumns of a given TargetTable
     * @param tableId target table id
     * @author Nash Lindley
     */
    getTargetColumnsByTargetTable(tableId): Observable<any> {
        return this.http.get(this.apiPath + '?tableId=' + tableId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all the targetColumns with column lineages of a given TargetTable and sourceSystem
     * @param tableId target table id
     * @param systemId source system id
     * @author Nash Lindley
     */
    getTargetColumnsByTargetAndSystem(tableId, systemId): Observable<any> {
        return this.http.get(this.apiPath + '?tableId=' + tableId + '&systemId=' + systemId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    getDistinctAllTables(): Observable<any> {
        return this.http.get(this.apiPath + '?distinct=' + true)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * @param schemaName 
     * @author Collin Atkins / 10.5.17 / 
     */
    getDistincTablesBySchemaName(schemaName: string): Observable<any> {
        return this.http.get(this.apiPath + '?schemaName=' + schemaName)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    getColumnById(columnId): Observable<any> {
        return this.http.get(this.apiPath + '?columnId=' + columnId + '&justName=' + false)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all the target columns of a given target table by id and filters the lineages
     * @param tableId target table id
     * @author Nash Lindley
     */
    getTargetColumnsByTargetTableFilterLineages(tableId): Observable<any> {
        return this.http.get(this.apiPath + '?tableId=' + tableId + '&filter=LINEAGES')
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all the target columns of a given target table by id and filters the columns already linked
     * @param tableId target table id
     * @author Collin Atkins / 10.5.17 /
     */
    getTargetColumnsByTargetTableFilterLinked(tableId): Observable<any> {
        return this.http.get(this.apiPath + '?tableId=' + tableId + '&filter=LINKED')
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all the target columns with matching column names
     * @param columnName name of columns to retrieve
     * @author Nash Lindley
     */
    getTargetColumnsByTargetColumn(columnName): Observable<any> {
        return this.http.get(this.apiPath + '?columnName=' + columnName)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all the target columns with matching column names, then filters lineages
     * @param columnName name of columns to retrieve
     * @author Nash Lindley
     */
    getTargetColumnsByTargetColumnFilterLineages(columnName): Observable<any> {
        return this.http.get(this.apiPath + '?columnName=' + columnName + '&filter=LINEAGES')
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Saves given target column
     * @param column: TargetColumn
     * @author Nash Lindley
     */
    saveTargetColumn(column) {
        return this.http.put(this.apiPath, column)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
