import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { SourceSystemService } from '../../source/sourcesystem/sourcesystem.service';
import { SourceSystem } from '../../source/sourcesystem/sourcesystem';

@Component({
    selector: 'dp-target-table-system',
    templateUrl: './target-table-system.component.html',
    providers: [SourceSystemService]
})

export class TargetTableSystemComponent implements OnInit {
    @Output() onSystemSelect = new EventEmitter<SourceSystem>()

    private sourceSystems: SourceSystem[];
    private selectedSystem: SourceSystem;

    constructor(private sourceSystemService: SourceSystemService) { }

    ngOnInit() {
        this.getSourceSystems();
    }

    /**
     * Calls the SourceSystemService to get all SourceSystems, then Calls selectSystem with the 0 index
     * @author Nash Lindley
     */
    getSourceSystems() {
        this.sourceSystemService.getSourceSystems()
            .subscribe(sourceSystems => {
                this.sourceSystems = sourceSystems;
                this.selectSystem(this.sourceSystems[0]);
            });
    }

    /**
     * Sets the selectedSystem to the given SourceSystem and the returns that SourceSystem's Id back to the TargetTableComponent
     * @param system: SourceSystem
     * @author Nash Lindley
     */
    selectSystem(system) {
        this.selectedSystem = system;
        this.onSystemSelect.emit(system);
    }

}
