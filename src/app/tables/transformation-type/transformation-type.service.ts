import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TransformationType } from './transformation-type';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';


@Injectable()
export class TransformationTypeService {
    private apiPath = 'api/EDWTransformationType';

    constructor(private http: Http) { }

    /**
     * Gets all Operator Types
     * @author Nash Lindley
     */
    getTransformationTypes() {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
