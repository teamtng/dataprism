export class TransformationType {
    Id: number;
    TT_Id: number;
    Type: string;
    TypeDesc: string;
    Format: string;
}
