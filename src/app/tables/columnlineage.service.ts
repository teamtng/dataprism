import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ColumnLineage } from './columnlineage';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class ColumnLineageService {
    private apiPath = 'api/EDWColumnLineage';

    private selectedLineages: ColumnLineage[];

    constructor(private http: Http) { }

    /**
     * Deletees a single column lineage by Id
     * @param Id
     * @author Nash Lindley
     */
    deleteColumnLineage(Id) {
        return this.http.delete(`${this.apiPath}?Id=${Id}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets ColumnLineages given a TargetColumn and SourceSystem
     * @param columnId target column id
     * @param systemId source system id
     * @author Nash Lindley
     */
    getColumnLineageByColumnAndSystem(columnId, systemId): Observable<any> {
        return this.http.get(this.apiPath + '?edwColumnId=' + columnId + '&systemId=' + systemId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets ColumnLineages given a TargetColumn
     * @param columnId target column id
     * @author Nash Lindley
     */
    getColumnLineageByColumn(columnId): Observable<any> {
        return this.http.get(this.apiPath + '?edwColumnId=' + columnId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets ColumnLineages given a target table name and target column name
     * @param tableName target table name
     * @param columnName target column name
     * @author Nash Lindley
     */
    getColumnLineageByTableAndColumn(tableName, columnName): Observable<any> {
        return this.http.get(this.apiPath + '?tableName=' + tableName + '&columnName=' + columnName + '&filter=SOURCE&detailLevel=All')
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Adds a new ColumnLineage or updates an existing lineage
     * @param lineage: ColumnLineage
     * @author Nash Lindley
     */
    saveColumnLineage(lineage) {
        return this.http.put(this.apiPath, lineage)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
