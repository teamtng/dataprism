import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SqlconfiguratorComponent } from './sqlconfigurator.component';

describe('SqlconfiguratorComponent', () => {
  let component: SqlconfiguratorComponent;
  let fixture: ComponentFixture<SqlconfiguratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SqlconfiguratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SqlconfiguratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
