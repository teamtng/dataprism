/**
 * SQL Configurator - Table Lineage component
 * @author Collin Atkins / 10.6.17 / Revamped ui, improved speed, and fixed some bugs found
 */

import { Component, OnInit, Inject, Input, SimpleChange, OnChanges } from '@angular/core';
import { SnackBarHandler } from 'utilities/snackbar';
import { OperatorType } from '../operatortype';
import { OperatorTypeService } from '../operatortype.service';
import { TableLineage } from '../tablelineage';
import { TableLineageService } from '../tablelineage.service';
import { SourceTableJoin } from '../sourcetablejoin/sourcetablejoin'
import { SourceTableJoinService } from '../sourcetablejoin/sourcetablejoin.service'
import { SourceTable } from '../../source/sourcetable/sourcetable';
import { SourceColumnService } from '../../source/sourcecolumn/sourcecolumn.service';
import { SourceColumn } from '../../source/sourcecolumn/sourcecolumn';

@Component({
    selector: 'dp-sqlconfigurator',
    templateUrl: './sqlconfigurator.component.html',
    providers: [SourceColumnService, OperatorTypeService, SourceTableJoinService, TableLineageService, SnackBarHandler],
    styleUrls: ['./sqlconfigurator.component.css']
})

export class SqlConfiguratorComponent implements OnInit {

    @Input() tableId: number;
    @Input() systemId: number;
    @Input() sourceTables: SourceTable[];

    join: SourceTableJoin = new SourceTableJoin();
    private operatorTypes: OperatorType[];
    private tableLineages: any[] = [];
    private masterTable: SourceTable = new SourceTable(-1);
    queue: number = 0;
    private sourceTableJoins: SourceTableJoin[] = [];
    private sourceColumns: SourceColumn[] = new Array();
    SourceTableId: any;
    tempMasterTable: SourceTable = new SourceTable(-1);
    filteredSourceTables: SourceTable[] = new Array();

    constructor(private operatorTypeService: OperatorTypeService, private tableLineageService: TableLineageService, private sourceTableJoinService: SourceTableJoinService,
        private snackBarHandler: SnackBarHandler, private sourceColumnService: SourceColumnService) { }

    ngOnInit() { }

    // ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    //     this.loadJoins();
    // }

    /**
     * Creates a new Join Alias
     * @author Nash Lindley
     */
    createJoin(tablelineage) {
        this.join = new SourceTableJoin();
        this.join.EDWTableLineageId = tablelineage.TableLineage.Id;
        this.join.JoinAlias = tablelineage.SourceTable.FriendlyName;
        this.join.SourceTableId = tablelineage.SourceTable.Id;

        this.saveSourceTableJoin(this.join);
    }

    /**
     * Filters source tables of master table and existing joins
     * @author Collin Atkins / 10.6.17 / 
     */
    private filterSourceTables() {
        this.filteredSourceTables = this.sourceTables;

        if (this.masterTable) {
            let masterIndex = this.filteredSourceTables.findIndex(st => st.Id === this.masterTable.Id);
            if (masterIndex >= 0) {
                this.filteredSourceTables.splice(masterIndex, 1);
            }
        }

        if (this.sourceTableJoins) {
            this.sourceTableJoins.forEach(stj => {
                let joinIndex = this.filteredSourceTables.findIndex(st => st.Id === stj.SourceTableId);
                if (joinIndex >= 0) {
                    this.filteredSourceTables.splice(joinIndex, 1);
                }
            });
        }
    }

    /**
     * Calls OperatorTypeService to get all existing OperatingTypes
     * @author Nash Lindley
     */
    getOperatorTypes() {
        this.operatorTypeService.getOperatorTypes()
            .subscribe(operators => {
                this.operatorTypes = operators
            })
    }

    /**
     * Calls SourceTableJoinService to get all SourceTableJoins for the given TargetTable and SourceSystem
     * @author Nash Lindley
     */
    getSourceTableJoins() {
        this.sourceTableJoinService.getSourceTableJoinByTableAndSystem(this.tableId, this.systemId)
            .subscribe(sourceTableJoins => {
                this.masterTable = sourceTableJoins.MasterSourceTable;
                this.sourceTableJoins = sourceTableJoins.MappedJoinedTables;

                this.filterSourceTables();

                if (!sourceTableJoins.MasterSourceTable) {
                    this.getTableLineages();
                }
            }, err => this.getTableLineages());
    }

    /**
     * Calls TableLineageService to get all existing TableLineages
     * @author Nash Lindley
     */
    getTableLineages() {
        this.tableLineageService.getTableLineageByTableAndSystem(this.tableId, this.systemId)
            .subscribe(tablelineages => { this.tableLineages = tablelineages; })
    }

    /**
     * handles requests by adding and subtracting from the queue
     * loads joins when done
     * @param num
     * @author Nash Lindley
     */
    handleQueue(num) {
        this.queue += num;
        if (this.queue == 0) {
            this.loadJoins();
        }
    }

    /**
     * Initializes Source Table Joins and Table Lineages
     * @author Nash Lindley
     */
    loadJoins() {
        if (this.systemId > 0 && this.tableId) {
            this.getSourceTableJoins();
        }
    }

    /**
     * Calls the SourceTableJoinService to Save the SourceTableJoin
     * Uses this components joins
     * @author Nash Lindley
     */
    saveSourceTableJoin(join = this.join) {
        this.sourceTableJoinService.saveSourceTableJoin(join)
            .subscribe(sourcetablejoin => {
                this.handleQueue(-1)
                join = sourcetablejoin;
                this.snackBarHandler.open(`${join.JoinAlias} was saved.`, 'success');
            },
            err => {
                this.handleQueue(-1);
                this.snackBarHandler.open(`${join.JoinAlias} failed to save.`, 'failure')
            });
    }

    /**
     * Calls the TableLineageService to Save the TableLineage
     * @param sourceTableId
     * @author Nash Lindley
     */
    saveTableLineageBySource(sourceTableId) {
        const newLineage = new TableLineage(this.tableId, this.systemId, sourceTableId)
        this.tableLineageService.saveTableLineage(newLineage)
            .subscribe(data => {
                this.snackBarHandler.open(`Table Lineage was saved.`, 'success')
                this.getSourceTableJoins();
            },
            err => {
                this.snackBarHandler.open(`Table Lineage failed to save.`, 'failure')
            })
    }

    /**
     * Saves the selected Table Lineage as the master Table
     * @param tableLineage
     * @author Nash Lindley
    */
    saveTableLineage(tableLineage) {
        const masterLineage = tableLineage;
        masterLineage.isSourceTableMaster = true;
        this.tableLineageService.saveTableLineage(masterLineage)
            .subscribe(() => {
                this.snackBarHandler.open(`Master table was saved.`, 'success')

                this.tableLineages.forEach(element => {
                    if (element.TableLineage.TableLineageId !== masterLineage.TableLineageId) {
                        this.handleQueue(1);
                        this.createJoin(element);
                    }
                })
                if (this.queue == 0) {
                    this.loadJoins();
                }
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`Failed to save master table.`, 'failure')
            });
    }

    /**
     * Modal Variables
     */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */
    show() {
        if (!this.operatorTypes) {
            this.getOperatorTypes();
        }

        this.loadJoins();

        this.filterSourceTables();

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    hide() {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    onContainerClicked(event: MouseEvent) {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
