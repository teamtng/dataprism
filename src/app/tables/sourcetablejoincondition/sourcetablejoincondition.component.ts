import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { SourceTableJoinCondition } from './sourcetablejoincondition';
import { SourceTableJoinConditionService } from './sourcetablejoincondition.service';
import { SourceColumn } from '../../source/sourcecolumn/sourcecolumn';
import { OperatorType } from '../../tables/operatortype';
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-sourcetablejoincondition',
    templateUrl: './sourcetablejoincondition.component.html',
    providers: [SourceTableJoinConditionService, SnackBarHandler],
    styleUrls: ['./sourcetablejoincondition.component.css']
})

export class SourceTableJoinConditionComponent implements OnInit {

    @Input() cond: SourceTableJoinCondition;
    @Output() condDeleted = new EventEmitter();
    @Input() masterColumns: SourceColumn[];
    @Input() sourceColumns: SourceColumn[];
    @Input() operatorTypes: OperatorType[];

    constructor(private joinConditionService: SourceTableJoinConditionService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.cond.SourceMasterColumn = this.cond.SourceMasterColumn || new SourceColumn();
        this.cond.SourceColumn = this.cond.SourceColumn || new SourceColumn();
        this.cond.OperatorType = this.cond.OperatorType || new OperatorType();
    }

    /**
     * Calls the SourceTableJoinConditionService to delete the join condition by Id
     * @author Nash Lindley
     */
    private deleteJoinCondition() {
        this.joinConditionService.deleteJoinConditions(this.cond.Id)
            .subscribe(() => {
                this.snackBarHandler.open(`Successfully Deleted Join Condition`, 'success')
                this.condDeleted.emit();
            })
    }

    /**
     * @author Collin Atkins / 10.6.17 / 
     */
    private toggleDBFunction() {
        this.cond.DBFunction = this.cond.DBFunction === 'Y' ? 'N' : 'Y';
    }

}
