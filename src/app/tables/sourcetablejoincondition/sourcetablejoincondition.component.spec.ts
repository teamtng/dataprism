import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceTableJoinConditionComponent } from './sourcetablejoincondition.component';

describe('SourcetablejoinconditionComponent', () => {
  let component: SourceTableJoinConditionComponent;
  let fixture: ComponentFixture<SourceTableJoinConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceTableJoinConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceTableJoinConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
