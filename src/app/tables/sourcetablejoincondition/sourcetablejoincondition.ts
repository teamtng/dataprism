import { SourceTable } from '../../source/sourcetable/sourcetable';
import { SourceColumn } from '../../source/sourcecolumn/sourcecolumn';
import { OperatorType } from '../operatortype';

export class SourceTableJoinCondition {

    Id: number;
    SourceTableJoinConditionId: number;
    SourceTableJoinId: number;
    SourceMasterSourceTableId: number;
    SourceMasterTable: SourceTable;
    SourceMasterColumnId: number;
    SourceMasterColumn: SourceColumn;
    SourceColumn: SourceColumn;
    SourceColumValues: string;
    ParentSourceTableJoinId: number;
    ParentConnector: string;
    OperatorType: OperatorType;
    JoinOrder: number;
    GroupingValue: string;
    DBFunction: string;

    constructor(joinId: number) {
        this.Id = -1;
        this.SourceTableJoinConditionId = -1;
        this.SourceTableJoinId = joinId;
        this.JoinOrder = null;
        this.DBFunction = 'N';
        this.GroupingValue = null;
        this.ParentConnector = null;
        this.SourceMasterColumn = new SourceColumn();
        this.SourceColumn = new SourceColumn();
        this.OperatorType = new OperatorType();
    }

}
