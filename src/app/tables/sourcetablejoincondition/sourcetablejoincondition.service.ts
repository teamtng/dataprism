import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';


@Injectable()
export class SourceTableJoinConditionService {
    private apiPath = 'api/EDWSourceTableJoinCondition';

    constructor(private http: Http) { }

    /**
     * Deletes SourceTableJoinCondition by Id
     * @param Id Join Condition id
     * @author Nash Lindley
     */
    deleteJoinConditions(Id) {
        return this.http.delete(`${this.apiPath}?Id=${+Id}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}