export class OperatorType {

    Id: number;
    OperatorTypeId: number;
    OperatorType: string;
    OperatorTypeDescription: string;
    OperatorTypeSymbol: string;

    constructor() {
        this.Id = -1;
        this.OperatorTypeId = -1;
    }
}
