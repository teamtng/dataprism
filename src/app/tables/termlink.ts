import { Glossary } from '../glossary/glossary/glossary';
import { Category } from '../glossary/category/category';
import { Term } from '../glossary/term/term';

export class TermLink {

    term: Term
    glossary: Glossary;
    category: Category;

    constructor() {
        this.term = new Term(-1);
        this.glossary = new Glossary();
        this.glossary.BusinessGlossaryId = -1;
        this.category = new Category(-1);
    }

}