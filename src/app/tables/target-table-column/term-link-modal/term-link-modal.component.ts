/**
 * @author Collin Atkins / 10.2-3.17 / Modified to get bg items on show rather than init; also cleaned up code, ui, and sped up considerably
 */

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TermLink } from '../../termlink';
import { Glossary } from '../../../glossary/glossary/glossary';
import { Category } from '../../../glossary/category/category';
import { Term } from '../../../glossary/term/term';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { GlossaryService } from '../../../glossary/glossary/glossary.service';
import { CategoryService } from '../../../glossary/category/category.service';
import { TermStatus } from '../../../glossary/term/term-status';
import { TermStatusService } from '../../../glossary/term/term-status.service';
import { TermService } from '../../../glossary/term/term.service';
import { TargetColumn } from 'app/tables/targetcolumn';

@Component({
    selector: 'dp-term-link-modal',
    templateUrl: './term-link-modal.component.html',
    styleUrls: ['./term-link-modal.component.css'],
    providers: [SnackBarHandler, GlossaryService, CategoryService, TermStatusService, TermService]
})

export class TermLinkModalComponent implements OnInit {

    @Output() linkChange = new EventEmitter();

    private targetColumn: TargetColumn = new TargetColumn(null);
    private target: TermLink = new TermLink();
    private glossaries: Glossary[] = new Array();
    private categories: Category[] = new Array();
    private termStatuses: TermStatus[] = new Array();

    constructor(private glossaryService: GlossaryService, private categoryService: CategoryService, private termStatusService: TermStatusService,
        private termService: TermService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() { }

    /**
     * Checks for if target column has term; if it does it loads it
     * @author John Crabill
     * @author Collin Atkins / 10.3.17 / Added clear/load link on id check
     */
    // checkLink() {
    //     this.termService.getAllTargetColumnTerms()
    //         .subscribe(columnIds => {
    //             if (columnIds.some(x => x === this.targetColumnId)) {
    //                 this.clearLink();
    //                 this.loadLink();
    //             } else {
    //                 this.clearLink();
    //             }
    //         });
    // }

    /**
     * Clears local variables for empty modal
     * @author John Crabill
     * @author Collin Atkins / 10.2.17 / Added if statements to not reload same glossaries/terms
     */
    clearLink() {
        this.target = new TermLink();
        this.categories = new Array();

        if (this.glossaries.length <= 0) {
            this.getGlossaries();
        }

        if (this.termStatuses.length <= 0) {
            this.getTermStatus();
        }
    }

    /**
     * @author Collin Atkins / 10.3.17 / Fills in term category with chosen category
     */
    fillInTerm() {
        this.target.term.targetColumn = this.targetColumn.Id;
        this.target.term.Category = this.target.category;
        this.target.term.CategoryId = this.target.category.Id;
    }

    /**
     * Retrieves glossaries
     * @author John Crabill
     */
    getGlossaries() {
        this.glossaryService.getGlossaries()
            .subscribe(glossaries => {
                this.glossaries = glossaries;
            });
    }

    /**
     * Retrieves categories
     * @author John Crabill
     * @author Collin Atkins / 10.3.17 / Removed hasCategory bool
     */
    getCategories(glossaryId) {
        this.categoryService.getCategoriesByGlossaryId(glossaryId)
            .subscribe(categories => {
                this.categories = categories;
            });
    }

    /**
     * Calls the termStatus Service to get the term statuses
     * @author John Crabgill
     */
    getTermStatus() {
        this.termStatuses = this.termStatusService.getTermStatus();
    }

    /**
     * Saves link
     * @author John Crabill
     * @author Collin Atkins / 10.2.17 / Removed field checks; added through html5 forms
     */
    saveLink() {
        this.fillInTerm();
        this.termService.saveTerm(this.target.term)
            .subscribe(tables => {
                this.snackBarHandler.open(`Term Link was saved.`, 'success');
                this.linkChange.emit();
            }, err => console.log(err));
    }

    /**
     * @author John Crabgill
     * @author Collin Atkins / 10.3.17 / Removed a lot of glut and extra calls
     */
    loadLink() {
        this.termService.getTermByTargetColumn(this.targetColumn.Id)
            .subscribe(term => {
                this.target.term = term[0];
                this.target.glossary = this.glossaries.find(g => g.Id === this.target.term.Category.BusinessGlossaryId || g.BusinessGlossaryId === this.target.term.Category.BusinessGlossaryId);

                this.categoryService.getCategoriesByGlossaryId(this.target.glossary.Id)
                    .subscribe(categories => {
                        this.categories = categories;
                        this.target.category = this.categories.find(c => c.Id === this.target.term.Category.Id);
                    });
            }, err => console.log(err));
    }

    /**
     * Modal Variables
     */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    show(targetColumn: TargetColumn) {
        this.targetColumn = targetColumn;

        this.clearLink();
        if (targetColumn.hasLink) {
            this.loadLink();
        }

        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    hide() {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    onContainerClicked(event: MouseEvent) {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }
}
