import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermLinkModalComponent } from './term-link-modal.component';

describe('TermLinkModalComponent', () => {
  let component: TermLinkModalComponent;
  let fixture: ComponentFixture<TermLinkModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermLinkModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermLinkModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});