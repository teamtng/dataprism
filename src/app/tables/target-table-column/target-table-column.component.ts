/**
 * Row of column lineage
 * @author Collin Atkins / 9.27.17 / Overhauled UI
 * @author Collin Atkins / 10.3.17 / Refactored term link check to use one call
 */

import { Component, OnInit, Input, Output, SimpleChange, OnChanges, EventEmitter } from '@angular/core';
import { TargetColumn } from '../targetcolumn';
import { ColumnLineageService } from '../columnlineage.service';
import { TermService } from '../../glossary/term/term.service';
import { ColumnLineage } from '../columnlineage';
import { SourceTable } from '../../source/sourcetable/sourcetable';
import { TargetColumnPipe } from './target-column-pipe';
import { SchemaFamily, TableFamily, TargetColumnFamily, ColumnLineageInterface } from '../../datalineage/datalineage';


@Component({
    selector: 'dp-target-table-column',
    templateUrl: './target-table-column.component.html',
    providers: [ColumnLineageService, TermService],
    styleUrls: ['./target-table-column.component.css']
})

export class TargetTableColumnComponent implements OnInit {
    @Input() targetColumn: TargetColumn = new TargetColumn();
    @Input() sourceTables: SourceTable[] = new Array();
    @Input() selectedLineages: ColumnLineage[] = new Array();
    @Input() systemId: number;
    @Input() tableId: number;

    columnFamily: TargetColumnFamily;
    private columnLineages: ColumnLineage[] = new Array();;

    constructor(private columnLineageService: ColumnLineageService, private termService: TermService) { }

    ngOnInit() {
        this.columnFamily = { parent: this.targetColumn, children: [] };
        this.columnLineages = [new ColumnLineage(this.targetColumn.Id, this.systemId, this.tableId)]

        this.getColumnLineages(this.targetColumn.Id, this.systemId);
        this.addColumnLineage();
    }

    /**
     * Adds a new blank ColumnLineage to the TargetColumn
     * @author Nash Lindley
     * @author Collin Atkins / 9.27.17 / Added check to make sure last columnLineage is filled
     */
    addColumnLineage() {
        if (this.columnLineages.length == 0 || this.columnLineages[this.columnLineages.length - 1].SourceColumnId >= 0) {
            this.columnLineages.push(new ColumnLineage(this.targetColumn.Id, this.systemId, this.tableId));
        }/* else {
            let lastColumn: ColumnLineage = this.columnLineages[this.columnLineages.length - 1];
            if (lastColumn.SourceTableId >= 0 && lastColumn.SourceColumnId >= 0) {
                this.columnLineages.push(new ColumnLineage(this.targetColumn.Id, this.systemId, this.tableId));
            }
        }*/
    }

    /**
     * Calls ColumnLineageService to get all ColumnLineages based on the TargetColumnId and SourceSystemId
     * @param targetColumnId: number - TargetColumn's Id
     * @param systemId: number - System id
     * @author Nash Lindley
     */
    getColumnLineages(targetColumnId: number, systemId: number) {
        this.columnLineages = this.targetColumn.ColumnLineages.length > 0 ? this.targetColumn.ColumnLineages : [];

        // this.columnLineageService.getColumnLineageByColumnAndSystem(id, systemId)
        //     .subscribe(columnLineages => {
        //         this.columnLineages = columnLineages.length > 0 ? columnLineages : [];
        //         this.addColumnLineage();
        //     });
    }

    /**
     * Checks to see if a link to a BGTerm exists
     * @author John Crabill
     */
    // checkLink() {
    //     this.termService.getAllTargetColumnTerms()
    //         .subscribe(columnIds => {
    //             if (columnIds.some(x => x === this.targetColumn.Id)) {
    //                 this.hasLink = true;
    //             }
    //         });
    // }

}
