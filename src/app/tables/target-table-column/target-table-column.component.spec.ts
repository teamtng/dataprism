import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnLineagesComponent } from './column-lineages.component';

describe('ColumnLineagesComponent', () => {
    let component: ColumnLineagesComponent;
    let fixture: ComponentFixture<ColumnLineagesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ColumnLineagesComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColumnLineagesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
