import { SourceTableJoin } from './sourcetablejoin/sourcetablejoin';
import { SourceTable } from '../source/sourcetable/sourcetable';

export class TableLineage {

    Id: number;
    TableLineageId: number;
    TargetTableId: number
    SourceTableId: number;
    isSourceTableMaster: boolean;
    SourceSystem: number;
    SourceTable: SourceTable;
    SourceTableJoins: SourceTableJoin[];
    show: boolean;

    constructor(targetTableId, systemId, sourceTableId, isMaster = false){
        this.TargetTableId = targetTableId;
        this.SourceSystem = systemId
        this.SourceTableId = sourceTableId;
        this.isSourceTableMaster = isMaster;
    }

}
