/**
 * Column Lineage
 * @author Collin Atkins / 10.9.17 / Added initialization of many target col/table, source col/table/sys, and their respective ids
 */

import { SourceColumn } from '../source/sourcecolumn/sourcecolumn';
import { SummaryType} from './summary/summarytype'
import { TargetColumn } from '../tables/targetcolumn';
import { SourceTable } from '../source/sourcetable/sourcetable';
import { SourceSystem } from '../source/sourcesystem/sourcesystem';
import { TargetTable } from '../tables/targettable';

export class ColumnLineage {

    Id: number;
    EDWColumnId: number;
    SourceColumnId: number;
    SourceTableId: number;
    TargetTableId: number;
    SourceColumns: SourceColumn[];
    SourceTableAlias: string;
    SourceSystemId: number;
    ColumnCalculation: string;
    CdcDateFlag: string;
    CdcTimeFlag: string;
    UseAsLookupKey: string;
    ColumnFilters: string;
    Summaries: SummaryType[];
    Transformation: string;

    TargetColumn: TargetColumn;
    TargetTable: TargetTable;
    SourceTable: SourceTable;
    SourceColumn: SourceColumn;
    SourceSystem: SourceSystem;

    Order: number;

    constructor(columnId = -1, systemId = -1, tableId = -1) {
        this.Id = -1;
        this.SourceColumnId = -1;
        this.SourceTableId = -1;
        this.EDWColumnId = columnId;
        this.SourceSystemId = systemId;
        this.TargetTableId = tableId;

        this.TargetColumn = new TargetColumn();
        this.TargetTable = new TargetTable();
        this.SourceTable = new SourceTable();
        this.SourceColumn = new SourceColumn();
        this.SourceSystem = new SourceSystem();
    }

}
