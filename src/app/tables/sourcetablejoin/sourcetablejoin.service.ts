import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {  } from './sourcetablejoin';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class SourceTableJoinService {
    private apiPath = 'api/EDWSourceTableJoins';


    constructor(private http: Http) { }

    /**
     * Deletes a SourceTableJoin by Id
     * @param Id id of a SourceTableJoin
     * @author Nash Lindley
     */
    deleteSourceTableJoin(Id) {
        return this.http.delete(`${this.apiPath}/?Id=${Id}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets ColumnLineages given a TargetColumn, SourceSystem, and SourceTable
     * @param tableId TargetTable.Id
     * @param systemId SourceSystem.Id
     * @param sourceTableId SourceTable.Id
     * @author Nash Lindley
     */
    getSourceTableJoinBySourceTable(tableId, systemId, sourceTableId){
        return this.http.get(`${this.apiPath}?TargetTableId=${tableId}&SourceSystemId=${systemId}&SourceTableId=${sourceTableId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets ColumnLineages given a TargetColumn and SourceSystem
     * @param tableId TargetTable.Id
     * @param systemId SourceSystem.Id
     * @author Nash Lindley
     */
    getSourceTableJoinByTableAndSystem(tableId, systemId): Observable<any> {
        return this.http.get(`${this.apiPath}?TargetTableId=${tableId}&SourceSystemId=${systemId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Adds a new ColumnLineage or updates an existing lineage
     * @param lineage: ColumnLineage
     * @author Nash Lindley
     */
    saveSourceTableJoin(join) {
        return this.http.put(this.apiPath, join)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
