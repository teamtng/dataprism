import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SourceTableJoin } from './sourcetablejoin';
import { SourceTableJoinService } from './sourcetablejoin.service';
import { SourceTableJoinCondition } from '../sourcetablejoincondition/sourcetablejoincondition';
import { TableLineageService } from '../../tables/tablelineage.service';
import { SourceTableJoinConditionService } from '../sourcetablejoincondition/sourcetablejoincondition.service';
import { SourceColumn } from '../../source/sourcecolumn/sourcecolumn';
import { OperatorType } from '../operatortype';
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-sourcetablejoin',
    templateUrl: './sourcetablejoin.component.html',
    providers: [SnackBarHandler, TableLineageService, SourceTableJoinConditionService]
})

export class SourceTableJoinComponent implements OnInit {

    @Input() join: SourceTableJoin = new SourceTableJoin();
    @Input() masterColumns: SourceColumn[] = new Array();
    @Input() operatorTypes: OperatorType[] = new Array();
    @Input() systemId: number;
    @Input() tableId: number;
    @Output() refreshJoins = new EventEmitter();

    private count: number;

    constructor(private sourceTableJoinService: SourceTableJoinService, private joinConditionService: SourceTableJoinConditionService,
        private snackBarHandler: SnackBarHandler, private tableLineageService: TableLineageService) { }

    ngOnInit() {
        //this.checkForAlias()
        if (this.join.SourceTableJoinConditions.length === 0) {
            this.join.SourceTableJoinConditions = [new SourceTableJoinCondition(this.join.SourceTableJoinId)];
        }
    }

    /**
     * Checks for existing Aliases of the current table and then creates a count
     */
    checkForAlias() {
        this.sourceTableJoinService.getSourceTableJoinBySourceTable(this.tableId, this.systemId, this.join.SourceTableId)
            .subscribe(joins => { this.count = joins.MappedJoinedTables.length })
    }

    /**
     * Checks to see if there are no more Aliases and then deletes the table lineage if that's the case
     * Future: Add popup to confirm delete
     */
    checkForAliasDelete() {
        this.sourceTableJoinService.getSourceTableJoinBySourceTable(this.tableId, this.systemId, this.join.SourceTableId)
            .subscribe(joins => {
                joins.MappedJoinedTables.length === 0 ? this.deleteTableLineage(this.join.EDWTableLineageId) : this.refreshJoins.emit();
            })
    }

    /**
     * Creates a new Join Alias
     */
    createJoinAlias() {
        this.checkForAlias();
        const joinAlias = JSON.parse(JSON.stringify(this.join));

        joinAlias.Id = -1;
        joinAlias.SourceTableJoinId = -1
        joinAlias.SourceTableJoinConditions = [];
        joinAlias.JoinAlias = `${joinAlias.SourceTable.FriendlyName}_${this.count}`;

        this.saveSourceTableJoin(joinAlias);

        this.refreshJoins.emit();
    }

    /**
     * Calls SourceTableJoinService to delete this components join
     */
    deleteJoinAlias() {
        this.deleteJoinConditions();
        this.sourceTableJoinService.deleteSourceTableJoin(this.join.Id)
            .subscribe(() => {
                this.snackBarHandler.open(`${this.join.JoinAlias} was deleted.`, 'success');
                this.checkForAliasDelete();
            },
            err => {
                console.log(err);
                this.snackBarHandler.open(`${this.join.JoinAlias} failed to be deleted.`, 'failure');
            });
    }

    deleteJoinConditions() {
        for (const cond of this.join.SourceTableJoinConditions) {
            this.joinConditionService.deleteJoinConditions(cond.Id);
        }
    }

    deleteTableLineage(Id) {
        this.tableLineageService.deleteTableLineageById(Id)
            .subscribe(() => {
                this.snackBarHandler.open(`Table lineage was deleted.`, 'success');
                this.refreshJoins.emit();
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`Failed to delete table lineage.`, 'failure');
            })
    }

    /**
     * Adds a new join condition to the join
     */
    loadNewJoinCondition() {
        this.join.SourceTableJoinConditions.push(new SourceTableJoinCondition(this.join.SourceTableJoinId));
    }

    /**
     * Removes SourceTableJoinConditionComponent if it was deleted
     * Will load a new SourceTableJoinCondition if there are none left
     */
    onCondDelete(condition) {
        const index = this.join.SourceTableJoinConditions.findIndex(cond => condition === cond);
        if (index >= 0) {
            this.join.SourceTableJoinConditions.splice(index, 1);
        }
        if (this.join.SourceTableJoinConditions.length === 0) {
            this.loadNewJoinCondition();
        }
    }

    /**
     * Calls the SourceTableJoinService to Save the SourceTableJoin
     * Uses this components joins
     */
    saveSourceTableJoin(join = this.join) {
        this.sourceTableJoinService.saveSourceTableJoin(join)
            .subscribe(sourcetablejoin => {
                join = sourcetablejoin;
                this.snackBarHandler.open(`${join.JoinAlias} was saved.`, 'success');
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${join.JoinAlias} failed to save.`, 'failure');
            });
    }

}
