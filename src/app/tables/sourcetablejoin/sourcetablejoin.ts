import {SourceTable} from '../../source/sourcetable/sourcetable';
import {SourceTableJoinCondition} from '../sourcetablejoincondition/sourcetablejoincondition';

export class SourceTableJoin {

    Id: number;
    SourceTableJoinId: number;
    EDWTableLineageId: number;
    SourceTableId: number;
    JoinAlias: string;
    JoinType: string;
    JoinDescription: string;
    SourceTable: SourceTable;
    SourceTableJoinConditions: SourceTableJoinCondition[];
    show: boolean;

    constructor(){
        this.Id = -1;
        this.SourceTableJoinId = -1;
        this.SourceTableJoinConditions = new Array();
        //this.JoinType = 'LEFT OUTER'
        this.SourceTable = new SourceTable(-1);
    }
}
