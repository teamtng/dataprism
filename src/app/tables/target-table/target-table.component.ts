/**
 * Column Lineage Edtior - Main Page
 * @author Collin Atkins / 9.29.17 / Added removal of selections after successful delete and made speed improvements
 * @author Collin Atkins / 10.3.17 / Refactored term link check to use one call
 */

import { Component, OnInit } from '@angular/core';
import { SourceSystem } from '../../source/sourcesystem/sourcesystem'
import { Subject, Observable } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { TargetColumn } from '../targetcolumn';
import { TargetColumnService } from '../targetcolumn.service'
import { SourceTableService } from '../../source/sourcetable/sourcetable.service';
import { SourceTable } from '../../source/sourcetable/sourcetable';
import { TableLineageService } from '../tablelineage.service'
import { TableLineage } from '../tablelineage';
import { ColumnLineage } from '../columnlineage';
import { ColumnLineageService } from '../columnlineage.service';
import { Table } from '../table';
import { TableService } from '../table.service';
import { SqlConfiguratorComponent } from '../sqlconfigurator/sqlconfigurator.component'
import { SnackBarHandler } from 'utilities/snackbar';
import { SSISGeneratorService } from '../ssis-generator.service'
import { TermService } from 'app/glossary/term/term.service';

@Component({
    selector: 'dp-target-table',
    templateUrl: './target-table.component.html',
    providers: [TargetColumnService, SourceTableService, TableLineageService, ColumnLineageService, SnackBarHandler, TableService, SSISGeneratorService, TermService],
    styleUrls: ['./target-table.component.css']
})

export class TargetTableComponent implements OnInit {

    table: Table = new Table();
    system: SourceSystem = new SourceSystem();

    targetColumns: TargetColumn[];
    sourceTables: SourceTable[];
    private targetTableColumn: TargetColumn;

    selectedLineages: ColumnLineage[] = [];
    lineagesDeleted: boolean = true;
    searchText: any;
    page: number = -1;

    constructor(private route: ActivatedRoute, private router: Router, private targetColumnService: TargetColumnService,
        private tableService: TableService, private sourceTableService: SourceTableService, private tableLineageService: TableLineageService,
        private columnLineageService: ColumnLineageService, private snackbar: SnackBarHandler, private ssisGeneratorService: SSISGeneratorService,
        private termService: TermService) { }

    /**
     * Gets tableId and name from url params
     * Loads fresh target column
     * @author Nash Lindley
     * @author Collin Atkins / 9.29.17 / Edited to accept qparams and removed getTable call
     */
    ngOnInit() {
        this.route.params.subscribe(params => {
            this.table.Id = +params['id'];
        });

        this.route.queryParams.subscribe(qparams => {
            this.table.DatabaseName = qparams['name'] + '';
        });

        this.system = new SourceSystem();
        this.loadNewTargetColumn();
    }

    /**
     * Checks to see if all column lineages were successfully deleted
     * @param index
     * @param array
     * @author Nash Lindley
     * @author Collin Atkins / 9.29.17 / Added reset to selections after delete
     */
    checkDeleteEnd(index, array) {
        if (index === array.length - 1) {
            if (this.lineagesDeleted) {
                this.selectedLineages = new Array();
                this.snackbar.open('Deleted all column lineages', 'success');
                this.getTargetColumns();
            } else {
                this.snackbar.open(`Failed to delete all column lineages`, 'failure');
            }
        }
    }

    /**
     * Calls ColumnLineage Service to delete the selectedLineage
     * @param element - Column Lineage
     * @author Nash Lindley
     */
    deleteColumnLineage(element, index, array) {
        if (element.Id > -1) {
            this.columnLineageService.deleteColumnLineage(element.Id)
                .subscribe(data => {
                    this.checkDeleteEnd(index, array);
                },
                err => {
                    this.lineagesDeleted = false;
                    this.checkDeleteEnd(index, array);
                });
        }

    }

    /**
     * Gets all target column terms to find if any have links
     * @author Collin Atkins / 10.5.17 / 
     */
    private getAllTargetColumnTerms(targetColumns: TargetColumn[]) {
        this.termService.getAllTargetColumnTerms()
            .subscribe(columnIds => {
                targetColumns.forEach(targetColumn => {
                    targetColumn.hasLink = columnIds.some(colId => colId === targetColumn.Id);
                });

                this.targetColumns = targetColumns;
            }, err => console.log(err));
    }

    /**
     * Gets the table by tableId
     * @param Id
     * @author Nash Lindley
     */
    getTable(Id: number) {
        this.tableService.getTableById(Id)
            .subscribe(table => {
                this.table = table;
            });
    }

    /**
     * Calls the TargetColumnService to get all targetColumn of the TargetTable
     * @author Nash Lindley
     */
    getTargetColumns() {
        this.targetColumnService.getTargetColumnsByTargetAndSystem(this.table.Id, this.system.Id)
            .subscribe(targetColumns => {
                this.targetColumns = targetColumns;
            });
    }

    /**
     * Loads an empty TargetColumn for AddTargetColumn
     * @author Nash Lindley
     */
    loadNewTargetColumn() {
        this.targetTableColumn = new TargetColumn(this.table.Id);
    }

    /**
     * Loads SourceTables by SourceSystemId and stores them in the SourceTableService to be accessed later
     * @author Nash Lindley
     */
    loadSourceTables() {
        this.sourceTableService.getSourceTablesBySourceSystem(this.system.Id)
            .subscribe(sourcetables => {
                this.sourceTables = sourcetables;
            });
    }

    /**
     * Link to SSIS
     * @author John Crabill
     */
    linkSelectedLineages() {
        this.ssisGeneratorService.linkToSSIS(this.table.Id)
            .subscribe(targetColumns => {
            });
    }

    /**
     * Recieves a SourceSystemId from target-table-system then loads the associated sourcetables
     * @param Id
     * @author Nash Lindley
     * @author Collin Atkins / 10.3.17 / Refactored to find if has term link
     */
    onSystemSelect(system: SourceSystem) {
        this.system = system;
        
        this.loadSourceTables();

        this.targetColumnService.getTargetColumnsByTargetAndSystem(this.table.Id, this.system.Id)
            .subscribe(targetColumns => {
                this.targetColumns = targetColumns;

                this.getAllTargetColumnTerms(this.targetColumns);
            }, err => console.log(err));
    }

    /**
     * Deletes the selected Lineages
     * @author Nash Lindley
     */
    removeSelectedLineages() {
        this.selectedLineages.forEach(this.deleteColumnLineage.bind(this));
    }

}
