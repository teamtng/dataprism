import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ColumnLineage } from './columnlineage';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class SSISGeneratorService {
    private apiPath = 'api/SSISGenerator';

    constructor(private http: Http) { }

    /**
     * Links to SSIS
     * @param Id - target table id
     * @author John Crabill
     */
    linkToSSIS(Id){
        return this.http.get(`${this.apiPath}?Id=${Id}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }


}
