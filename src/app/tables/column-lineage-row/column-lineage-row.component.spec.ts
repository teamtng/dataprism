import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumnLineageRowComponent } from './column-lineage-row.component';

describe('ColumnLineageRowComponent', () => {
    let component: ColumnLineageRowComponent;
    let fixture: ComponentFixture<ColumnLineageRowComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ColumnLineageRowComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ColumnLineageRowComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
