/**
 * Row information for column lineage
 * @author Collin Atkins / 9.27.17 / Overhauled UI and refactored
 * @author Collin Atkins / 9.29.17 / Added input filters, fixed input bugs, and revised UI
 * @author Collin Atkins / 10.9.17 / Added initialization, fixed bug of column lineage not being disabled always, and added default choice to select sources
 */

import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ColumnLineage } from '../columnlineage';
import { ColumnLineageService } from '../columnlineage.service';
import { SourceColumnService } from '../../source/sourcecolumn/sourcecolumn.service';
import { TableLineageService } from '../tablelineage.service';
import { TableLineage } from '../tablelineage';
import { SourceTable } from '../../source/sourcetable/sourcetable';
import { SourceColumn } from '../../source/sourcecolumn/sourcecolumn';
import { DataTableDirective } from 'angular-datatables';
import { SnackBarHandler } from 'utilities/snackbar';
import { Observable } from 'rxjs/Observable';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'dp-column-lineage-row',
    templateUrl: './column-lineage-row.component.html',
    providers: [SourceColumnService, ColumnLineageService, TableLineageService, SnackBarHandler],
    styleUrls: ['./column-lineage-row.component.css']
})

export class ColumnLineageRowComponent implements OnInit {

    @Input() columnLineage: ColumnLineage = new ColumnLineage();
    @Input() sourceTables: SourceTable[] = new Array();
    @Input() selectedLineages: ColumnLineage[] = new Array();
    @Input() tableId: number;
    @Input() systemId: number;
    @Input() transformation: string;
    @Output() columnLineageChange = new EventEmitter();
    @Output() columnLineageSaved = new EventEmitter();

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    sourceColumns: SourceColumn[] = new Array();
    selected: boolean = false;
    //columnName: string = "";

    private sourceTablesControl: FormControl = new FormControl();
    private sourceColumnsControl: FormControl = new FormControl();
    private filteredSourceTables: Observable<SourceTable[]>;
    private filteredSourceColumns: Observable<SourceColumn[]>;

    constructor(private sourceColumnService: SourceColumnService, private columnLineageService: ColumnLineageService,
        private TableLineageService: TableLineageService, private snackBarHandler: SnackBarHandler) { }

    /**
     * @author Collin Atkins / 9.29.17 / Added many listeners to filter on input and enable/disable controls
     */
    ngOnInit() {
        this.sourceColumns = this.columnLineage.SourceColumns ? this.columnLineage.SourceColumns : new Array();

        if (this.sourceTables) {
            this.filteredSourceTables = this.sourceTablesControl.valueChanges.startWith(null)
                .map(val => (val && typeof(val) === 'string') ? this.filterSourceTables(val) : this.sourceTables.slice());
        }

        if (this.columnLineage.SourceTableId >= 0 && this.sourceTables) {
            this.columnLineage.SourceTable = this.sourceTables.find(sourceTable => sourceTable.Id == this.columnLineage.SourceTableId);
            
            if (this.columnLineage.SourceColumnId >= 0 && this.sourceColumns && this.columnLineage.SourceTable) {
                this.columnLineage.SourceColumn = this.sourceColumns.find(sourceColumn => sourceColumn.Id == this.columnLineage.SourceColumnId);
                //this.columnName = this.columnLineage.SourceColumn ? this.columnLineage.SourceColumn.FriendlyName1 : "";

                this.filteredSourceColumns = this.sourceColumnsControl.valueChanges.startWith(null)
                    .map(val => (val && typeof(val) === 'string') ? this.filterSourceColumns(val) : this.sourceColumns.slice());

                this.sourceColumnsControl.enable();
            } else {
                this.sourceColumnsControl.disable();
            }
        } else {
            this.sourceColumnsControl.disable();
        }

    }

    /**
     * Maps sourceTable object to name for input
     * @param sourceTable
     * @author Collin Atkins / 9.27.17 / 
     */
    private displaySourceTable(sourceTable: SourceTable): SourceTable | string {
        return sourceTable ? sourceTable.FriendlyName : sourceTable;
    }

    /**
     * Maps sourceColumn object to name for input
     * @param sourceColumn 
     * @author Collin Atkins / 9.27.17 / 
     */
    private displaySourceColumn(sourceColumn: SourceColumn): SourceColumn | string {
        return sourceColumn ? sourceColumn.FriendlyName1 : sourceColumn;
    }

    private emit($event) {
        // Sets color of transformOrCalc button
        let selector: string = `#transformOrCalcBtn${this.columnLineage.Id}`;
        $(selector).removeClass('btn-success');
        $(selector).removeClass('btn-danger');
        if ($event.ColumnCalculation || $event.Transformation) {
            $(selector).addClass('btn-success');
        } else {
            $(selector).addClass('btn-danger');
        }

        this.columnLineageChange.emit();
    }

    /**
     * Filters source tables input based on val entered
     * @param sourceTableName 
     * @author Collin Atkins / 9.29.17 / 
     */
    private filterSourceTables(sourceTableName: string): SourceTable[] {
        return this.sourceTables.filter(st => st.FriendlyName.toLowerCase().indexOf(sourceTableName.toLowerCase()) === 0);
    }

    /**
     * Filters source columns input based on val entered
     * @param sourceColumnName 
     * @author Collin Atkins / 9.29.17 / 
     */
    private filterSourceColumns(sourceColumnName: string): SourceColumn[] {
        return this.sourceColumns.filter(sc => sc.FriendlyName1.toLowerCase().indexOf(sourceColumnName.toLowerCase()) === 0);
    }

    /**
     * Called when key is pressed on source table field
     * @param $event
     */
    private inputSourceTable($event) {
        if ($event.key === 'Backspace') {
            if (this.columnLineage.SourceTableId >= 0) {
                this.resetSourceTable();
            }
        }
    }

    /**
     * Called when key is pressed on source column field
     * @param $event
     */
    private inputSourceColumn($event) {console.log($event);
        if ($event.key === 'Backspace') {
            if (this.columnLineage.SourceTableId) {
                this.resetSourceColumn();
            }
        }
    }

    /**
     * Calls the sourceColumnService to get the source columns for the given sourcetable
     * @author Nash Lindley
     */
    // getSourceColumnsByTable(sourceTableId) {
    //     this.sourceColumnService.getSourceColumnsBySourceTable(sourceTableId)
    //         .subscribe(columns => { this.sourceColumns = columns });
    // }

    private rerender() {
        this.dtElement.dtInstance
            .then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
            });
    }

    /**
     * Custom control for reseting source table input/select
     * @author Collin Atkins / 10.9.17 /
     */
    private resetSourceTable() {
        this.resetSourceColumn();
        this.sourceColumnsControl.disable();

        this.columnLineage.SourceColumns = new Array();
        this.columnLineage.SourceTableId = -1;
        this.columnLineage.SourceTable = new SourceTable();
    }

    /**
     * Custom control for reseting source column input/select
     * @author Collin Atkins / 10.9.17 /
     */
    private resetSourceColumn() {
        this.columnLineage.SourceColumnId = -1;
        this.columnLineage.SourceColumn = new SourceColumn();
    }

    /**
     * Saves this column Lineage
     * @author Nash Lindley
     * @author Collin Atkins / 9.27.17 / Moved name mapping to init, added subscribe to saveTableLineage
     */
    saveColumnLineage(columnLineage: ColumnLineage) {
        this.saveTableLineage(columnLineage.SourceTableId)
            .subscribe(savedTableLineage => {
                this.columnLineageService.saveColumnLineage(columnLineage)
                    .subscribe(savedColumnLineage => {
                        savedColumnLineage.SourceTable = this.columnLineage.SourceTable;
                        savedColumnLineage.SourceColumn = this.columnLineage.SourceColumn;
                        this.columnLineage = savedColumnLineage;
                        this.columnLineageSaved.emit();
                        this.snackBarHandler.open(`${savedColumnLineage.Id} was saved.`, 'success');
                    },
                    err => {
                        console.log(err);
                        this.snackBarHandler.open(`${columnLineage.Id} failed to save.`, 'failure');
                    });
            }, err => {
                console.log(err);
                this.snackBarHandler.open(`${columnLineage.Id} failed to save.`, 'failure');
            });

    }

    /**
     * Calls the TableLineageService to Save the TableLineage
     * @param sourceTableId
     * @author Nash Lindley
     * @author Collin Atkins / 9.27.17 / Returns observable of service call
     */
    saveTableLineage(sourceTableId): Observable<any> {
        const newLineage = new TableLineage(this.tableId, this.systemId, sourceTableId)
        return this.TableLineageService.saveTableLineage(newLineage);
    }

    /**
     * Handles selection of source table. 
     * Sets columnLineage value then gets new source columns.
     * @param sourceTable: SourceTable - st selected
     * @author Collin Atkins / 9.27.17 / 
     * @author Collin Atkins / 9.29.17 / Edited to clear source column, added check to not repeat on same choice
     */
    private selectSourceTable(sourceTable: SourceTable) {
        if (this.columnLineage.SourceTableId != sourceTable.Id && sourceTable.Id >= 0) {
            this.columnLineage.SourceTableId = sourceTable.Id;
            this.columnLineage.SourceColumn = new SourceColumn();

            this.sourceColumnService.getSourceColumnsBySourceTable(sourceTable.Id)
                .subscribe(columns => { 
                    this.sourceColumns = columns;

                    this.filteredSourceColumns = this.sourceColumnsControl.valueChanges.startWith(null)
                        .map(val => (val && typeof(val) === 'string') ? this.filterSourceColumns(val) : this.sourceColumns.slice());
                    
                    this.sourceColumnsControl.enable();
                });
        }
    }

    /**
     * Handles selection of source table.
     * Sets column lineage value then saves the column lineage.
     * @param sourceColumn: SourceColumn - sc selected
     * @author Collin Atkins / 9.27.17 / 
     * @author Collin Atkins / 10.4.17 / Added if check to not repeat save
     */
    private selectSourceColumn(sourceColumn: SourceColumn) {
        if (this.columnLineage.SourceColumnId != sourceColumn.Id && sourceColumn.Id >= 0) {
            this.columnLineage.SourceColumnId = sourceColumn.Id;
            this.saveColumnLineage(this.columnLineage);
        }
    }

    /**
     * Appends/Removes this columnLineage's to the list of selected Column Lineages
     * @author Nash Lindley
     * @author Collin Atkins / 9.26.17 / Refactored for brevity
     */
    toggleSelected() {
        let index: number = this.selectedLineages.findIndex(selectedLineage => selectedLineage.Id == this.columnLineage.Id);
        if (index >= 0) {
            this.selectedLineages.splice(index, 1);
        } else {
            this.selectedLineages.push(this.columnLineage);
        }
    }

}
