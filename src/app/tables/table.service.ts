import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Table } from './table';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';
import { Schema } from "app/schema/schema";


@Injectable()
export class TableService {
    private apiPath = 'api/EDWTable';

    constructor(private http: Http) { }

    /**
     * Deletes table by id
     * @param tableId id of table
     * @author Nash Lindley
     */
    deleteTable(tableId: number): Observable<any> {
        return this.http.delete(this.apiPath + '/?Id=' + tableId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all tables
     * @author Nash Lindley
     */
    getTables(): Observable<any>  {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all tables from a specific schema by name
     * @param schema - schema Name
     * @author Nash Lindley
     */
    getTablesbySchema(schema: Schema): Observable<any>  {
        return this.http.get(this.apiPath + '?schemaName=' + schema.SchemaName)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all tables from a specific schema by name and filters the lineages
     * @param schema - schema Name
     * @author Nash Lindley
     */
    getTablesbySchemaFilterLineages(schema: Schema): Observable<any>  {
        return this.http.get(this.apiPath + '?schemaName=' + schema.SchemaName + '&filter=LINEAGES')
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all tables by id
     * @param tableId - id of table to get
     * @author Nash Lindley
     */
    getTableById(tableId: number): Observable<any>  {
        return this.http.get(this.apiPath + '?tableId=' + tableId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Saves a table
     * @param table - table to save
     * @author Nash Lindley
     */
    saveTable(table: Table): Observable<any> {
        return this.http.put(this.apiPath, JSON.stringify(table))
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all tables from a specific schema by name
     * @param schemaName - schema Name
     * @author John Crabill
     */
    getTablesbySchemaName(schemaName) {
        return this.http.get(this.apiPath + '?import=true&schemaNameImport=' + schemaName)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }
}
