import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { OperatorType } from './OperatorType';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';


@Injectable()
export class OperatorTypeService {
    private apiPath = 'api/EDWOperatorTypes';

    constructor(private http: Http) { }

    /**
     * Gets all Operator Types
     * @author Nash Lindley
     */
    getOperatorTypes() {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
