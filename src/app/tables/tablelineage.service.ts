import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ColumnLineage } from './columnlineage';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class TableLineageService {
    private apiPath = 'api/EDWTableLineage';
    private apiPathSSIS = 'api/SSISGenerator';

    private columnLineages: ColumnLineage[];

    constructor(private http: Http) { }


    /**
     * Deletes a Table Lineage by Id
     * @param Id
     * @author Nash Lindley
     */
    deleteTableLineageById(Id) {
        return this.http.delete(`${this.apiPath}?Id=${Id}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets TableLineages given a TargetTable and SourceSystem
     * @param tableId - target table id
     * @param systemId - source system id
     * @author Nash Lindley
     */
    getTableLineageByTableAndSystem(tableId, systemId): Observable<any> {
        return this.http.get(this.apiPath + '?TargetTableId=' + tableId + '&SourceSystemId=' + systemId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets All Columns of tableLineages
     * @param tableId - target table id
     * @param systemId - source system id
     * @author Nash Lindley
     */
    getTableLineageAll(tableId, systemId){
        return this.http.get(`${this.apiPath}?TargetTableId=${tableId}&SourceSystemId=${systemId}&Type=All`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets Numerics of tableLineages
     * @param tableId - target table id
     * @param systemId - source system id
     * @author Nash Lindley
     */
    getTableLineageNumerics(tableId, systemId){
        return this.http.get(`${this.apiPath}?TargetTableId=${tableId}&SourceSystemId=${systemId}&Type=Numeric`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Links to SSIS
     * @param Id - target table id
     * @author John Crabill
     */
    linkToSSIS(Id){
        return this.http.put(this.apiPathSSIS, Id)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Adds a new TableLineage or updates and exisiting 
     * @param lineage - ColumnLineage
     * @author Nash Lindley
     */
    saveTableLineage(lineage) {
        return this.http.put(this.apiPath, lineage)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
