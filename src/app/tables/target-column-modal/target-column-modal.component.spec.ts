import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetColumnModalComponent } from './target-column-modal.component';

describe('TargetColumnModalComponent', () => {
  let component: TargetColumnModalComponent;
  let fixture: ComponentFixture<TargetColumnModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetColumnModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetColumnModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
