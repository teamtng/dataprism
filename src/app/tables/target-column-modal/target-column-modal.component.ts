import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TargetColumn } from '../targetcolumn';
import { TargetColumnService } from '../targetcolumn.service'
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-target-column-modal',
    templateUrl: './target-column-modal.component.html',
    providers: [TargetColumnService, SnackBarHandler],
    styleUrls: ['./target-column-modal.component.css']
})
export class TargetColumnModalComponent implements OnInit {

    @Input() targetColumn: TargetColumn
    @Output() columnChange = new EventEmitter();

    private dataTypes;

    constructor(private targetColumnService: TargetColumnService, private snackbar: SnackBarHandler) { }

    ngOnInit() { }

    private clearCharTypeDataLength(targetColumn: TargetColumn): TargetColumn {
        targetColumn.CharType = null;
        targetColumn.DataLength = null;
        return targetColumn;
    }

    private clearPrecisionScale(targetColumn: TargetColumn): TargetColumn {
        targetColumn.Precision = null;
        targetColumn.Scale = null;
        return targetColumn;
    }

    private fillFlags(targetColumn: TargetColumn): TargetColumn {
        targetColumn.NullableFlag = targetColumn.IsNullable ? 'Y' : 'N';
        targetColumn.PrimaryKeyFlag = targetColumn.IsPrimaryKey ? 1 : 0;
        return targetColumn;
    }

    /**
     * Autofills data in target column depending form filled out
     * @param targetColumn TargetColumn
     * @author Nash Lindley
     */
    generateFullDataType(targetColumn: TargetColumn): TargetColumn {
        if (targetColumn.BaseDataType) {
            targetColumn.FullDataType = targetColumn.BaseDataType;
            if (targetColumn.BaseDataType.includes('CHAR')) {
                targetColumn = this.clearPrecisionScale(targetColumn);
                targetColumn.FullDataType = targetColumn.DataLength ? `${targetColumn.BaseDataType}(${targetColumn.DataLength})` : `${targetColumn.BaseDataType}`;
            } else if (targetColumn.BaseDataType == 'DATE') {
                targetColumn = this.clearPrecisionScale(targetColumn);
                targetColumn = this.clearCharTypeDataLength(targetColumn);
            }
            else {
                targetColumn = this.clearCharTypeDataLength(targetColumn);
                targetColumn.FullDataType = (targetColumn.Precision && targetColumn.Scale) 
                ? `${targetColumn.BaseDataType}(${targetColumn.Precision},${targetColumn.Scale})` 
                    : `${targetColumn.BaseDataType}`;
            }
        }

        return targetColumn;
    }

    /**
     * returns all datatypes in this case for an oracle database;
     * @author Nash Lindley
     */
    private loadDataTypes() {
        return [{ 'Name': 'NUMBER' },
        { 'Name': 'FLOAT' },
        { 'Name': 'VARCHAR2' },
        { 'Name': 'DATE' },
        { 'Name': 'CHAR' },
        { 'Name': 'NVARCHAR2' },
        { 'Name': 'NCHAR' }];
    }

    /**
     * Calls TargetColumnService to Add or Update a TargetColumn
     * @author Nash Lindley
     */
    saveTargetColumn() {
        this.targetColumn = this.generateFullDataType(this.targetColumn);
        this.targetColumn = this.fillFlags(this.targetColumn);
        this.targetColumnService.saveTargetColumn(this.targetColumn)
            .subscribe(targetcolumn => {
                this.columnChange.emit(this.targetColumn);
                this.snackbar.open(`${this.targetColumn.FriendlyName} was successfully saved.`, 'success');
            },
            err => {
                this.snackbar.open(`${this.targetColumn.FriendlyName} failed to be saved.`, 'failure');
            })
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */
    public show(): void {
        this.dataTypes = this.loadDataTypes();
        if (this.targetColumn.NullableFlag == null || this.targetColumn.NullableFlag == undefined) {
            this.targetColumn.IsNullable = true;
        }
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
