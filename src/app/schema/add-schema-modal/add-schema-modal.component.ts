import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SchemaService } from '.././schema.service'
import { TableService } from '../../tables/table.service';
import { Table } from '../../tables/table';
import { Schema } from '.././schema';
import { SnackBarHandler } from 'utilities/snackbar';
//import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

@Component({
    selector: 'dp-add-schema-modal',
    templateUrl: './add-schema-modal.component.html',
    providers: [SchemaService, TableService, SnackBarHandler],
    styleUrls: ['./add-schema-modal.component.css']
})

export class AddSchemaModalComponent implements OnInit {

    @Output() schemaChange = new EventEmitter();

    Schema: Schema = new Schema();
    Tables: Table[] = new Array();
    TablesChosen: Table[] = new Array();

    // Options and settings for multi-select dropdown
    // optionsModel: number[];
    // myOptions: IMultiSelectOption[];
    // mySettings: IMultiSelectSettings = {
    //     enableSearch: true,
    //     showCheckAll: true,
    //     showUncheckAll: true,
    // };
    // myTexts: IMultiSelectTexts = {
    //     searchPlaceholder: 'Find table',
    //     defaultTitle: 'Select tables',
    //     allSelected: 'All selected',
    //     searchEmptyResult: 'No tables found',
    //     checkedPlural: 'tables selected'
    // };

    constructor(private schemaService: SchemaService, private tableService: TableService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.clearSchema();
        this.clearTables();
    }

    /**
     * Empties out the Schema object
     */
    clearSchema() {
        this.Schema = new Schema();
    }

    private clearTables() {
        this.Tables = [];
        this.TablesChosen = [];
    }

    /**
     * Calls TableService and gets all tables then sets them as options for multiselect
     */
    populateTableOptions() {
        this.clearTables();
        this.tableService.getTables()
            .subscribe(tables => {
                // Resets past chosen options
                // this.optionsModel = new Array();

                // Sets options for multi-select dropdown on add schema modal
                // this.myOptions = tables.map(function (t) {
                //     return { id: t.DatabaseName, name: t.DatabaseName }
                // });

                this.Tables = tables;
            });
    }

    /**
     * Saves schema using schemaService saveSchema.
     * Then calls tableService to saveTable for each table chosen in optionsModel.
     */
    saveSchema(Schema: Schema, TablesChosen?: Table[]) {
        Schema.Id = -1;
        this.Schema.LoginUserPassword = window.btoa(this.Schema.LoginUserPassword);

        this.schemaService.saveSchema(Schema)
            .subscribe(() => {
                // Saves all tables selected from multi-select dropdown
                // if (this.optionsModel.length > 0) {
                //     for (let i = 0; i < this.optionsModel.length; i++) {
                //         let tableName: string = String(this.optionsModel[i]);
                //         let tempTable: Table = { Id: -1, DatabaseName: tableName, SchemaName: this.Schema.SchemaName }
                //         this.tableService.saveTable(tempTable).subscribe(() => {
                //             // Only prints on last loop, after saveTable completes
                //             if (i == this.optionsModel.length - 1) {
                //                 this.schemaChange.emit(this.Schema);
                //                 this.snackBarHandler.open(this.Schema.SchemaName + ' has been added.', 'success');
                //             }
                //         });
                //     }
                // } else {
                if (TablesChosen.length > 0) {
                    TablesChosen.forEach((table, index) => {
                        table.SchemaName = Schema.SchemaName;
                        this.tableService.saveTable(table)
                            .subscribe(savedTable => {
                                if (index == this.TablesChosen.length - 1) {
                                    // Only prints on last loop, after saveTable completes
                                    this.schemaChange.emit(Schema);
                                    this.snackBarHandler.open(Schema.SchemaName + ' has been added.', 'success');
                                }
                            });
                    });
                } else {
                    this.schemaChange.emit(Schema);
                    this.snackBarHandler.open(Schema.SchemaName + ' has been added.', 'success');
                }
            });
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */
    public show(): void {
        this.clearSchema();
        this.populateTableOptions();
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
