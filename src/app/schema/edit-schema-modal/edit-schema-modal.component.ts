import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { SchemaService } from '.././schema.service';
import { TableService } from '../../tables/table.service';
import { Table } from '../../tables/table';
import { Schema } from '.././schema';
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-edit-schema-modal',
    templateUrl: './edit-schema-modal.component.html',
    providers: [SchemaService, TableService, SnackBarHandler],
    styleUrls: ['./edit-schema-modal.component.css']
})

export class EditSchemaModalComponent implements OnInit {

    @Output() schemaChange = new EventEmitter();

    @ViewChild(DataTableDirective)
    editDtElement: DataTableDirective;

    editDtInstance: any = {};
    editDtOptions: DataTables.Settings = {};
    editDtTrigger: Subject<Table[]> = new Subject();

    Schema: Schema = new Schema();
    Table: Table = new Table();
    Tables: Table[] = [];
    tablePrefix: boolean = false;

    showAddTable: boolean = false;
    showEditTable: boolean = false;

    constructor(private schemaService: SchemaService, private tableService: TableService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.editDtOptions = {};
        this.clearTable();
    }

    /**
     * Hides add table form
     */
    cancelAddTableForm() {
        this.clearTable();
        this.showAddTable = false;
    }

    /**
     * Hides edit table form
     */
    cancelEditTableForm() {
        this.clearTable();
        this.showEditTable = false;
        this.tablePrefix = false;
    }

    /**
     * Empties out the Table object
     */
    clearTable() {
        this.Table = new Table();
    }

    /**
     * Sets up Table object, saves it, then hides addTableForm.
     * Called from confirm button on addTableForm.
     */
    confirmAddTableForm() {
        this.Table.Id = -1;
        this.Table.SchemaName = this.Schema.SchemaName;

        if(this.Schema.DBType.toLowerCase() == "sql server"){
            this.Table.TablePrefix = "dbo";
        }
        this.tableService.saveTable(this.Table)
            .subscribe(() => {
                this.getTables();
                this.snackBarHandler.open(this.Table.DatabaseName + ' has been added.', 'success');
                this.cancelAddTableForm();
            }, err => console.log(err));
    }

    /**
     * Checks Table object, updates it, then hides editTableForm.
     * Called from confirm button on editTableForm.
     * @param table - table object from row of edit button clicked
     */
    confirmEditTableForm(table: Table) {
        if (this.Table.DatabaseName == table.DatabaseName) {
            this.tableService.saveTable(this.Table)
                .subscribe(() => {
                    this.getTables();
                    this.snackBarHandler.open(this.Table.DatabaseName + ' has been updated.', 'success');
                    this.cancelEditTableForm();
                }, err => console.log(err));
        }
    }

    /**
     * Calls tableServices to delete given table
     * @param table - table to be deleted
     */
    deleteTable(table: Table) {
        const deletedTable = table;
        this.tableService.deleteTable(table.Id)
            .subscribe(() => {
                this.getTables();
                this.snackBarHandler.open(deletedTable.DatabaseName + ' was deleted.', 'default');
            });
    }

    /**
     * First deletes the current dtInstance if one exist.
     * Calls TableService and gets all tables with given schema name to populate datatable of Tables
     */
    getTables() {
        if (this.Schema) {
            if (this.editDtElement && this.editDtElement.dtInstance) {
                this.editDtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                });
            }

            this.tableService.getTablesbySchema(this.Schema)
                .subscribe(tables => {
                    this.Tables = tables;
                    this.editDtTrigger.next();
                }, err => {
                    this.Tables = [];
                    this.editDtTrigger.next();
                });
        }
    }

    /**
     * Shows addTableForm as first row of table
     */
    showAddTableForm() {
        this.clearTable();
        this.showEditTable = false;
        this.showAddTable = true;
    }

    /**
     * Changes row of table to editTableForm
     * @param table - table row of edit button clicked
     */
    showEditTableForm(table: Table) {
        this.tablePrefix = true;
        if (this.Table.DatabaseName) {
            // Ensures not opening another edit table form
            if (this.Table.DatabaseName == table.DatabaseName) {
                this.clearTable();
                this.Table = table;
                this.showEditTable = true;
            }
        }
        // Handles first time case 
        else {
            this.clearTable();
            this.Table = table;
            this.showAddTable = false;
            this.showEditTable = true;
        }
    }

    /**
     * Updates schema using schemaService saveSchema.
     * Called from save schema button.
     */
    updateSchema(Schema: Schema) {
        //Schema.LoginUserPassword = window.btoa(Schema.LoginUserPassword);
        this.schemaService.saveSchema(Schema)
            .subscribe(savedSchema => {
                this.snackBarHandler.open(savedSchema.SchemaName + ' has been updated.', 'success');
                this.schemaChange.emit(savedSchema);
            });
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */
    public show(schema: Schema): void {
        this.ngOnInit();
        this.Schema = schema;
        this.getTables();
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
