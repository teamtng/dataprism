import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportSchemaImportModalComponent } from './import-schema-modal.component';

describe('ImportSchemaImportModalComponent', () => {
  let component: ImportSchemaImportModalComponent;
  let fixture: ComponentFixture<ImportSchemaImportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportSchemaImportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportSchemaImportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});