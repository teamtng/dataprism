/**
 * Import Schema Modal - Import source tables from a schema
 * @author Collin Atkins / 10.6.17 / Fixed rerendering bug
 */

import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { SchemaService } from '.././schema.service';
import { TableService } from '../../tables/table.service';
import { Table } from '../../tables/table';
import { Schema } from '.././schema';
import { TableImport } from '../import-schema';
import { SnackBarHandler } from 'utilities/snackbar';
import { SourceSystem } from '../../source/sourcesystem/sourcesystem';
import { SourceSystemService } from '../../source/sourcesystem/sourcesystem.service';
import { SourceTable } from '../../source/sourcetable/sourcetable';
import { SourceTableService } from '../../source/sourcetable/sourcetable.service';

@Component({
    selector: 'dp-import-schema-modal',
    templateUrl: './import-schema-modal.component.html',
    styleUrls: ['./import-schema-modal.component.css'],
    providers: [SchemaService, SourceSystemService, SourceTableService, TableService, SnackBarHandler]
})

export class ImportSchemaImportModalComponent implements OnInit {

    @Input() schema: Schema;
    @Output() change = new EventEmitter();

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    sourceImport: TableImport = new TableImport();
    startImport: TableImport = new TableImport();
    selectAllButton: boolean = false;
    sourceSystems: SourceSystem[] = new Array();
    systemId: number;
    checked: boolean = false;

    dtInstance: any = {};
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<TableImport[]> = new Subject();

    constructor(private schemaService: SchemaService, private sourceSystemService: SourceSystemService, private sourceTableService: SourceTableService, private tableService: TableService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.clearTable();
    }

    checkIfClicked(x) {
        return x.checked;
    }

    /**
     * Finds if any tables are selected
     */
    checkIfAnyChecked() {
        var isChecked = false;
        for (var table of this.sourceImport.stringTables) {
            if (table.value) {
                isChecked = true;
            }
        }

        if(isChecked){
            this.checked = true;
        }
        else {
            this.checked = false;
        }
    }

    /**
     * For empty modal
     */
    clearTable() {
        this.sourceImport = new TableImport();
    }

    clearChecks() {
        for (const table of this.sourceImport.stringTables) {
            table.value = false;
        }
        this.selectAllButton = false;
    }

    // creates the basic key-value dictionary
    createDictionary(imports: string[]) {
        for (let i = 0; i < imports.length; i++) {
            this.sourceImport.stringTables.push({ key: imports[i], value: false });
        }
    }

    getSourceSystems() {
        this.rerender();
        //if(this.sourceSystems.length != 0){
            //$('.datatableRerender').DataTable().destroy();
            //this.rerender();
        //}
        this.sourceSystemService.getSourceSystems()
            .subscribe(sourceSystems => {
                this.clearTable();
                this.clearChecks();
                this.sourceSystems = sourceSystems;
                this.dtTrigger.next();
            });
    }

    hideit() {
        var result_style = document.getElementsByClassName('dataTables_empty');
        for (var i = 0; i < result_style.length; i++) {
            (result_style[i] as HTMLElement).style.display = 'none';
        }
    }

    importTables(importedTables) {
        for (var i = 0; i < this.sourceSystems.length; i++) {
            if (this.systemId == this.sourceSystems[i].Id) {
                importedTables.DBType = this.sourceSystems[i].DBVendorName;
            }
        }
        importedTables.schemaName = this.schema.SchemaName;
        //importedTables.DBType = this.schema.DBType;
        importedTables.systemId = this.systemId;
        importedTables.importType = "source";
        this.schemaService.importTables(importedTables)
            .subscribe(data => {
                this.change.emit();
                this.snackBarHandler.open('Successfully Completed Table and Column Import', 'success')
            }, err => {
                this.snackBarHandler.open('Failed to save all Tables or Columns', 'failure')
            })
    }

    saveTables() {
        const trueImport = new TableImport();
        trueImport.Id = this.sourceImport.Id;
        for (const table of this.sourceImport.stringTables) {
            if (table.value) {
                trueImport.stringTables.push(table.key);
            }
        }
        this.importTables(trueImport);
    }

    // Select all for modal
    selectAll() {
        for (const table of this.sourceImport.stringTables) {
            table.value = this.selectAllButton;
        }
        this.checkIfAnyChecked();
        // for (const table of this.sourceImport.Tables) {
        //     if (this.checkIfClicked(document.getElementsByName('tables')[0])) {
        //         table.value = true;
        //     } else {
        //         table.value = false;
        //     }
        // }
    }

    // for just one value for modal
    select(key) {
        this.sourceImport.stringTables[key.value] = !key.value;
        this.checkIfAnyChecked();
    }

    showit() {
        var result_style = document.getElementsByClassName('dataTables_empty');
        for (var i = 0; i < result_style.length; i++) {
            (result_style[i] as HTMLElement).style.display = 'block';
        }
    }

    updateTables(systemId) {
        if (systemId != -1) {
            this.systemId = systemId;
            this.sourceImport.stringTables = [];
            //this.showit();
            this.sourceTableService.getSourceTablesBySourceSystemImport(systemId)
                .subscribe(sourceTables => {
                    this.startImport = sourceTables
                    //console.log(this.startImport);
                    //console.log(this.startImport.SchemaTables);
                    //this.hideit();
                    this.createDictionary(this.startImport.SchemaTables);
                    //this.dtTrigger.next();
                });
        }
        else {
            this.sourceImport.stringTables = [];
            this.showit();
        }
    }

    /**
     * Destroys table instance when populated
    */
    private rerender() {
        if (this.dtElement && this.dtElement.dtInstance) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                dtInstance.destroy();
            });
        }
    }

    /**
     * Modal Variables
     */
    visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    hide() {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    onContainerClicked(event: MouseEvent) {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}