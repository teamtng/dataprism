export class Schema {

    Id: number;
    SchemaName: string;
    LoginUserName: string;
    LoginUserPassword: string;
    DBHostName: string;
    DBProvider: string;
    DBType: string;
    TablePrefix: string;

    constructor() {
        this.Id = -1;
    }

}
