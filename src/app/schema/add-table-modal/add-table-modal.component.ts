import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SchemaService } from '.././schema.service'
import { TableService } from '../../tables/table.service';
import { Table } from '../../tables/table';
import { Schema } from '.././schema';
import { SnackBarHandler } from 'utilities/snackbar';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'dp-add-table-modal',
    templateUrl: './add-table-modal.component.html',
    providers: [SchemaService, TableService, SnackBarHandler],
    styleUrls: ['./add-table-modal.component.css']
})

export class AddTableModalComponent implements OnInit {

    @Output() tableChange = new EventEmitter();

    Schemas: Schema[] = [];
    Table: Table = new Table();

    constructor(private schemaService: SchemaService, private tableService: TableService, private router: Router, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.clearTable();
    }

    /**
     * Empties out the Table object
     */
    clearTable() {
        this.Table = new Table();
    }

    /**
     * Gets all schemas using schemaService
     */
    getSchemas() {
        this.schemaService.getSchemas()
            .subscribe(schemas => {
                this.Schemas = schemas;
            }, err => console.log(err));
    }

    /**
     * Saves table using tableService saveTable
     */
    saveTable() {
        if (this.Schemas && this.Table && this.Table.SchemaName && this.Table.SchemaName != '-1') {
            for (var i = 0; i < this.Schemas.length; i++) {
                if (this.Table.SchemaName == this.Schemas[i].SchemaName && this.Schemas[i].DBType != null && this.Schemas[i].DBType.toLowerCase() == "sql server") {
                    this.Table.TablePrefix = "dbo";
                }
            }
        }

        this.Table.Id = -1;
        this.tableService.saveTable(this.Table)
            .subscribe(table => {
                this.Table = table;
                this.router.navigateByUrl('schema/table/' + this.Table.Id);
                this.snackBarHandler.open(this.Table.DatabaseName + ' has been saved.', 'success');
                this.tableChange.emit();
            }, err => console.log(err));
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */
    public show(): void {
        this.clearTable();
        this.getSchemas();
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}
