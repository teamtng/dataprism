import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { TableService } from '../../tables/table.service';
import { Table } from '../../tables/table';
import { Schema } from '.././schema';
import { SnackBarHandler } from 'utilities/snackbar';
import { Router } from '@angular/router';

@Component({
    selector: 'dp-schema-detail-modal',
    templateUrl: './schema-detail-modal.component.html',
    providers: [TableService, SnackBarHandler],
    styleUrls: ['./schema-detail-modal.component.css']
})

export class SchemaDetailModalComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    dtInstance: any = {};
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<Table[]> = new Subject();

    Schema: Schema = new Schema();
    Tables: Table[] = [];

    constructor(private tableService: TableService, private snackBarHandler: SnackBarHandler, private router: Router) { }

    ngOnInit() {
        this.dtInstance = {};
        this.dtOptions = {};
    }

    /**
     * First deletes a dtInstance if one exist.
     * Calls TableService and gets all tables with given schema name to populate datatable of Tables
     */
    getTables() {
        if (this.Schema) {
            if (this.dtElement && this.dtElement.dtInstance) {
                this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                    dtInstance.destroy();
                });
            }

            this.tableService.getTablesbySchema(this.Schema)
                .subscribe(tables => {
                    this.Tables = tables;
                    this.dtTrigger.next();
                },
                // Covers case of viewing a schema with no tables
                err => {
                    this.Tables = [];
                    this.dtTrigger.next();
                });
        }
    }

    /**
     * Redirects to the table route
     * @param table
     * @author Collin Atkins / 9.29.17 / Added qparam to reduce call to get name
     */
    private gotoTable(table: Table) {
        this.router.navigate(['/schema/table/' + table.Id], { queryParams: { name: table.DatabaseName } });
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */
    public show(schema: Schema): void {
        this.ngOnInit();
        this.Schema = schema;
        this.Tables = [];
        this.getTables();
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}