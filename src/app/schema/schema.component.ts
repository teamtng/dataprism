import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { SchemaService } from './schema.service'
import { TableService } from '../tables/table.service';
import { Table } from '../tables/table';
import { Schema } from './schema';
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-schema',
    templateUrl: './schema.component.html',
    providers: [SchemaService, TableService, SnackBarHandler],
    'styleUrls': ['./schema.component.css']
})

export class SchemaComponent implements OnInit {

    schemas: Schema[];

    constructor(private schemaService: SchemaService, private tableService: TableService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.getSchemas();
    }

    /**
     * Calls tableService to delete given schema
     * @param schema - schema to be deleted
     */
    deleteSchema(schema: Schema) {
        const deletedSchema = schema;
        this.schemaService.deleteSchema(schema).subscribe(() => {
            this.getSchemas();
            this.snackBarHandler.open(deletedSchema.SchemaName + ' was deleted.', 'success');
        });
    }

    /**
     * Gets all schemas using schemaService
     */
    getSchemas() {
        this.schemaService.getSchemas().subscribe(schemas => {
            this.schemas = schemas;
        });
    }

    /**
     * Testing Connection
     * @author John Crabill
     * @param schema
     */
    private testConnection(schema: Schema){
        this.schemaService.testConnection(schema)
            .subscribe(conn => {
                if(conn == "Success"){
                    this.snackBarHandler.open('Connection Found', 'success');
                }
                else if(conn == "Failed"){
                    this.snackBarHandler.open('Connection Failed', 'failure');
                }
            });
    }

}
