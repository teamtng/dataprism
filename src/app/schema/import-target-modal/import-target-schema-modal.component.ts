import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Rx';
import { SnackBarHandler } from 'utilities/snackbar';

import { Schema } from '.././schema';
import { SchemaService } from '.././schema.service';

import { SourceImport } from '../../source/sourcetable/sourceImport';
import { TableService } from '../../tables/table.service';
import { Table } from '../../tables/table';

import { TableImport } from '../import-schema';

@Component({
    selector: 'dp-import-target-schema-modal',
    templateUrl: './import-target-schema-modal.component.html',
    styleUrls: ['./import-target-schema-modal.component.css'],
    providers: [SchemaService, TableService, SnackBarHandler]
})

export class SchemaTableImportModalComponent implements OnInit {

    @Input() schema: Schema;
    @Output() change = new EventEmitter();

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    private sourceImport: SourceImport;
    selectAllButton: boolean = false;
    schemas: Schema[] = new Array();
    fromSchemaName: string;
    checked: boolean = false;
    targetTables: Table = new Table();


    dtTrigger: Subject<TableImport[]> = new Subject();
    dtOptions: DataTables.Settings = {};

    constructor(private schemaService: SchemaService, private tableService: TableService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.clearTable();
    }

    checkIfClicked(x) {
        return x.checked;
    }

    /**
     * Finds if any tables are selected
     */
    checkIfAnyChecked() {
        var isChecked = false;
        for (var table of this.sourceImport.SchemaTables) {
            if (table.value) {
                isChecked = true;
            }
        }

        if(isChecked){
            this.checked = true;
        }
        else {
            this.checked = false;
        }
        console.log(this.checked);
    }

    /**
     * For empty modal
     */
    clearTable() {
        //this.targetTables = new Table();
        //this.schemas = new Array();
        this.sourceImport = new SourceImport();

        //this.sourceImport = new TableImport();
        //this.sourceImport.SchemaName = null;
        //this.schemas = [];
        
        this.selectAllButton = false;
        this.fromSchemaName = null;
        this.checked = false;
    }

    clearChecks() {
        for (const table of this.sourceImport.SchemaTables) {
            table.value = false;
        }
        this.selectAllButton = false;
    }

    // creates the basic key-value dictionary
    createDictionary(imports) {
        this.sourceImport.Id = imports.Id;
        this.sourceImport.SchemaTables = [];
        for (let i = 0; i < imports.SchemaTables.length; i++) {
            this.sourceImport.SchemaTables.push({ key: imports.SchemaTables[i], value: false });
        }
        //console.log(this.sourceImport);
    }

    getSchemas() {
        console.log(this.schemas);
        if(this.sourceImport && this.sourceImport.SchemaTables.length != 0){
            this.rerender();
        }
        this.schemaService.getSchemaTables(this.schema.SchemaName)
            .subscribe(schemas => {
                //this.clearTable();
                //this.clearChecks();
                this.createDictionary(schemas);
                this.dtTrigger.next();
            });
    }

    hideit() {
        var result_style = document.getElementsByClassName('dataTables_empty');
        for (var i = 0; i < result_style.length; i++) {
            (result_style[i] as HTMLElement).style.display = 'none';
        }
    }

    importTables(importedTables) {
            for (var i = 0; i < this.schemas.length; i++) {
                if (this.fromSchemaName == this.schemas[i].SchemaName) {
                    importedTables.DBType = this.schemas[i].DBType;
                }
            }
            importedTables.schemaName = this.schema.SchemaName;
            importedTables.DBType = this.schema.DBType;
            importedTables.importType = "target";
            console.log(importedTables);
        //importedTables.systemId = this.systemId;
        this.schemaService.importTables(importedTables)
            .subscribe(data => {
                this.change.emit();
                this.snackBarHandler.open('Successfully Completed Table and Column Import', 'success')
            }, err => {
                this.snackBarHandler.open('Failed to save all Tables or Columns', 'failure')
            })
    }

    saveTables() {
        const trueImport = new TableImport();
        trueImport.Id = this.sourceImport.Id;
        for (const table of this.sourceImport.SchemaTables) {
            if (table.value) {
                trueImport.stringTables.push(table.key);
            }
        }
        this.importTables(trueImport);
    }

    // Select all for modal
    selectAll() {
        for (const table of this.sourceImport.SchemaTables) {
            table.value = this.selectAllButton;
        }
        this.checkIfAnyChecked();
    }

    // for just one value for modal
    select(key) {
        this.sourceImport.SchemaTables[key.value] = !key.value;
        this.checkIfAnyChecked();
    }

    showit() {
        var result_style = document.getElementsByClassName('dataTables_empty');
        for (var i = 0; i < result_style.length; i++) {
            (result_style[i] as HTMLElement).style.display = 'block';
        }
    }

    /*updateTables(schema: Schema) {
        if (schema.SchemaName != null) {
            this.fromSchemaName = schema.SchemaName;
            this.sourceImport.SchemaTables = [];
            //this.showit();
            this.tableService.getTablesbySchemaName(schema.SchemaName)
                .subscribe(targetTables => {
                    console.log(targetTables);
                    this.targetTables = targetTables;
                    //this.hideit();
                    var tableList = new Array(new Table);
                    console.log(this.targetTables.SchemaTables);
                    this.createDictionary(this.targetTables.SchemaTables);
                    //this.dtTrigger.next();
                });
        }
        else {
            this.sourceImport.SchemaTables = [];
            //this.showit();
        }
    }*/

    /**
     * Destroys table instance when populated
    */
    private rerender() {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
        });
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    public show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }
}