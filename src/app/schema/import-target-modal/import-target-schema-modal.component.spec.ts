import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchemaTableImportModalComponent } from './import-target-schema-modal.component';

describe('SchemaTableImportModalComponent', () => {
  let component: SchemaTableImportModalComponent;
  let fixture: ComponentFixture<SchemaTableImportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchemaTableImportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchemaTableImportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});