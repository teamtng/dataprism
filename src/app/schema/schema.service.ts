import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Schema } from './schema';
import { DataHandler } from 'utilities/data';
import { ErrorHandler } from 'utilities/error';

@Injectable()
export class SchemaService {
    // private apiPath = 'api/EDWTargetDBSchema';
    private apiPath = 'api/EDWSchemaTargets';
    private apiPathImport = 'api/EDWSchemaImport';//'api/ET_EDW_TABLE';
    private apiConn = 'api/Connection';

    constructor(private http: Http) { }

    /**
     * Deletes schema by name
     * @param schema - schema to be deleted by name
     */
    deleteSchema(schema: Schema): Observable<any> {
        return this.http.delete(this.apiPath + '?SchemaName=' + schema.SchemaName)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all Schemas
     */
    getSchemas() {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Gets all tables from the schema connection
     */
    getSchemaTables(Name) {
        return this.http.get(`${this.apiPathImport}?SchemaName=${Name}`)//return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Saves a schema
     * @param schema - schema to be saved
     */
    saveSchema(schema: Schema): Observable<any> {
        return this.http.put(this.apiPath, JSON.stringify(schema))
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    importTables(tables): Observable<any> {
        return this.http.put(this.apiPathImport, tables)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    
    /**
     * Tests the connection
     * @author John Crabill
     * @param Schema Schema - Schema to check
     */
    testConnection(schema: Schema){
        return this.http.put(this.apiConn, schema) 
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
