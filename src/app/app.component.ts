import { Component } from '@angular/core';
import { AuthenticationService } from './auth/authentication.service';

@Component({
    selector: 'dp-app',
    templateUrl: './app.component.html',
    providers: [AuthenticationService],
    styleUrls: ['./app.component.css']
})

export class AppComponent {
    title = 'app';

    constructor(private authService: AuthenticationService) { }

}
