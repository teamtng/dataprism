import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SourceTable } from '../sourcetable';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { UtilityHandler } from '../../../../utilities/utility';
import { SourceTableService } from '../sourcetable.service';
import { SourceColumnService } from '../../sourcecolumn/sourcecolumn.service';
import { Subject } from 'rxjs/Rx';
import { FileHandler } from 'utilities/file';
import { SourceColumn } from 'app/source/sourcecolumn/sourcecolumn';
import { SourceUtility } from 'app/source/source-utility';

@Component({
    selector: 'dp-source-table-modal',
    templateUrl: './source-table-modal.component.html',
    styleUrls: ['./source-table-modal.component.css'],
    providers: [SourceTableService, SnackBarHandler, FileHandler, SourceColumnService]
})

export class SourceTableModalComponent implements OnInit {

    @Output() systemTableChange = new EventEmitter();
    @Input() SourceSystemId: number;
    @Input() vendor: string;

    private fileHandler: FileHandler = new FileHandler();
    private sourceUtility: SourceUtility = new SourceUtility();

    private SourceTable: SourceTable;
    FromFile: boolean = false;

    dtTrigger: Subject<SourceTable[]> = new Subject();

    constructor(private tableService: SourceTableService, private snackBarHandler: SnackBarHandler, private columnService: SourceColumnService) { }

    ngOnInit() {
        this.clearTable();
    }

    /**
     * For empty modal
     */
    clearTable() {
        this.SourceTable = new SourceTable(this.SourceSystemId);
    }

    /**
     * Called when file is uploaded via browse file path to set values of other inputs
     * @param event 
     */
    fileUploaded(event) {
        const file: File = event.target.files[0];
        const [fileName, fileExt] = file.name.split('.');

        if (fileExt == 'txt' || fileExt == 'csv') {
            this.SourceTable.FileType = fileExt;
            $('#typeFormInput').val(fileExt);

            // Sets Default values
            this.SourceTable.FileDelimiter = ',';
            this.SourceTable.TextQualifier = `"`;
            this.SourceTable.FirstLineHeader = 1;
        }

        this.SourceTable.DatabaseName = fileName;
    }

    /**
     * Load for modal
     * @param table sourceTable - used as base for edit modal
     */
    loadSystem(table: SourceTable) {
        this.SourceTable = table;
    }

    /**
     * Save/Update table using tableService
     * @param table - source table to update
     */
    saveTable(table: SourceTable) {
        if (this.FromFile) {
            let file: File = document.forms['dp-add-st-form']['fileFormInput'].files[0];
            this.fileHandler.readLocalCsv(file, table.FirstLineHeader, table.FileDelimiter, table.TextQualifier)
                .then(csvObject => {
                    this.tableService.saveTable(this.SourceTable)
                        .subscribe(savedTable => {
                            this.snackBarHandler.open(`${this.SourceTable.DatabaseName} has been saved.`, 'success');
                            this.systemTableChange.emit();

                            let sourceColumns: SourceColumn[] = new Array();
                            sourceColumns = this.sourceUtility.constructSourceColumns(savedTable.Id, csvObject);

                            sourceColumns.forEach(column => {
                                this.columnService.saveColumn(column).subscribe(() => {}, err => console.log(err));
                            });
                        }, err => {
                            console.log(err);
                            this.snackBarHandler.open('The table failed to save.', 'failure');
                        });
                })
                .catch(err => {
                    this.snackBarHandler.open('The file could not be read.', 'failure');
                });
        } else {
            if(this.vendor.toLowerCase() == 'sql server ' && this.SourceTable.TablePrefix == undefined){
                this.SourceTable.TablePrefix = "dbo";
            }
            this.tableService.saveTable(this.SourceTable)
                .subscribe(() => {
                    this.snackBarHandler.open(`${this.SourceTable.SchemaName} ${this.SourceTable.FriendlyName} has been saved.`, 'success');
                    this.systemTableChange.emit();
                }, err => {
                    this.snackBarHandler.open(err);
                });
        }
    }

    setFromFile(fromFile: boolean) {
        this.FromFile = fromFile;
    }

    toggleHeader() {
        this.SourceTable.FirstLineHeader = this.SourceTable.FirstLineHeader == 1 ? 0 : 1;
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    public show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }

}