import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceTableModalComponent } from './source-table-modal.component';

describe('SourceTableModalComponent', () => {
  let component: SourceTableModalComponent;
  let fixture: ComponentFixture<SourceTableModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceTableModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceTableModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});