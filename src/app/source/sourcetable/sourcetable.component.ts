import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { SourceTableService } from './sourcetable.service';
import { SourceTable } from './sourcetable';
import { Subject } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'dp-sourcetable',
    templateUrl: './sourcetable.component.html',
    providers: [SourceTableService]
})

export class SourceTableComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    SourceSystemId: any;
    name: any;
    sourceTables: SourceTable[];
    vendor: string;

    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<SourceTable[]> = new Subject();

    constructor(private route: ActivatedRoute, private router: Router, private sourceTableService: SourceTableService) { }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.SourceSystemId = +params['id'];
        });
        this.route.queryParams.subscribe(qparams => {
            this.name = qparams['name'] + ' ';
            this.vendor = qparams['vendor'] + ' ';
        });
        this.dtOptions = {};
        this.getSourceTables();
    }

    /**
     * Calls the SourceTableService to get all Source Tables of the SourceSystem
     */
    getSourceTables() {
        if (this.sourceTables) {
            this.rerender();
        }

        this.sourceTableService.getSourceTablesBySourceSystem(this.SourceSystemId)
            .subscribe(sourceTables => {
                this.sourceTables = sourceTables;
                console.log(this.sourceTables);
                this.dtTrigger.next();
            });
    }

    gotoTable(table: SourceTable, name) {
        this.router.navigate(['/source/sourcecolumn/' + table.SourceSystemId], { queryParams: { tableId: table.Id, name: name, friendlyName: table.FriendlyName } });
    }

    /**
     * Destroys table instance when populated
     */
    private rerender() {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
        });
    }
}
