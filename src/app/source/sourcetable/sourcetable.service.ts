import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SourceTable } from './sourcetable';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class SourceTableService {
    private apiPath = 'api/EDWSourceTable';

    sourceTables: SourceTable[];
    sourceCheckedTables: SourceTable[];
    schemaName: any;

    constructor(private http: Http) { }

    /**
     * Gets all the SourceTables of a given SourceSystem
     * @param systemId number - id of source system
     */
    getSourceTablesBySourceSystem(systemId: number) {
        return this.http.get(this.apiPath + '?systemId=' + systemId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    getSourceTablesBySourceSystemImport(systemId: number) {
        return this.http.get(this.apiPath + '?systemTableId=' + systemId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }


    /**
     * Save the Source Table from modal
     * @param SourceTable SourceTable - source table to save
     */
    saveTable(SourceTable: SourceTable): Observable<any> {
        return this.http.put(this.apiPath, SourceTable)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }


    /**
     * Loads all the SourceTables into the local variable sourceTables
     * @param systemId id of source system
     */
    loadSourceTables(systemId) {
        this.getSourceTablesBySourceSystem(systemId)
            .subscribe(sourceTables => {
                this.sourceTables = sourceTables;
                //if(this.sourceTables != null){
                //    this.schemaName = this.sourceTables[0].SchemaName;
                //}
            })
    }

}
