import { SourceColumn} from '../sourcecolumn/sourcecolumn';

export class SourceTable {

    Id: number;
    SourceSystemId: number;
    DatabaseName: string;
    SchemaName: string;
    FriendlyName: string;
    TablespaceName: string;
    NumRowStats: number;
    NumRowSelect: number;
    SourceColumns: SourceColumn[];
    TablePrefix: string;

    FileDelimiter: string;
    TextQualifier: string;
    FileSourceUrl: string;
    FirstLineHeader: number;
    FileType: string;

    constructor(SourceSystemId: number = -1) {
        this.Id = -1;
        this.SourceSystemId = SourceSystemId;
    }
}
