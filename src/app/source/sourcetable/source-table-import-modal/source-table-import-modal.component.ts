import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SourceImport } from '../sourceImport';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { UtilityHandler } from '../../../../utilities/utility';
import { SourceImportTableService } from '../sourceImport.service';
import { Subject } from 'rxjs/Rx';

@Component({
    selector: 'dp-source-table-import-modal',
    templateUrl: './source-table-import-modal.component.html',
    styleUrls: ['./source-table-import-modal.component.css'],
    providers: [SourceImportTableService, SnackBarHandler]
})

export class SourceTableImportModalComponent implements OnInit {

    @Input() SourceSystemId: number;
    @Output() importChange = new EventEmitter();

    private sourceImport: SourceImport;
    private selectAllButton: boolean;

    dtTrigger: Subject<SourceImport[]> = new Subject();

    constructor(private sourceImportService: SourceImportTableService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.clearTable();
    }

    /**
     * For empty modal
     */
    clearTable() {
        this.sourceImport = new SourceImport();
    }

    /**
     * Clears all tables checked
     */
    clearChecks() {
        for (const table of this.sourceImport.SchemaTables) {
            table.value = false;
        }
        this.selectAllButton = false;
    }

    private containsItem(item, array) {
        for (let i = 0; i < array.length; i++) {
            if (item == array[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds if table is checked
     * @param x
     */
    private checkIfChecked(x) {
        return x.checked;
    }

    /**
     * Finds if any tables are selected
     */
    checkIfAnyChecked(): boolean {
        let checked: boolean = false;
        if (this.sourceImport.SchemaTables) {
            for (const table of this.sourceImport.SchemaTables) {
                if (table.value) {
                    checked = true;
                }
            }
        } else {
            checked = false;
        }
        return checked;
    }

    /**
     * Creates dictionary from source imports
     * @param imports
     */
    private createDictionary(imports: SourceImport) {
        this.sourceImport.Id = imports.Id;
        for (let i = 0; i < imports.SchemaTables.length; i++) {
            this.sourceImport.SchemaTables.push({ key: imports.SchemaTables[i], value: false });
        }
    }

    /**
     * Gets all source tables from source import service
     * @param sourceTables
     */
    getImportSchemas(sourceTables) {
        if (sourceTables != null) {
            this.sourceImportService.getCheckedTables(sourceTables)
                .subscribe(tableSchema => {
                    this.createDictionary(tableSchema);
                    this.dtTrigger.next();
                }, err => {
                    console.log(err);
                });
        }
    }

    /**
     * Calls the SourceImportService to import the selected tables
     */
    private importTables(schema) {
        this.sourceImportService.importTables(schema)
            .subscribe(data => {
                this.snackBarHandler.open('Imported successfully.', 'success');
                this.importChange.emit();
            }, err => {
                console.log(err);
                this.snackBarHandler.open('Import failed.', 'failure')
            })
    }

    /**
     * Loads into usable schema object with all selected tables
     */
    saveTables() {
        const tableImport: SourceImport = new SourceImport();
        tableImport.Id = this.sourceImport.Id;
        tableImport.SchemaName = this.sourceImport.SchemaName;
        tableImport.SchemaTables = [];
        for (const table of this.sourceImport.SchemaTables) {
            if (table.value) {
                tableImport.SchemaTables.push(table.key)
            }
        }
        this.importTables(tableImport);
    }

    /**
     * Selects all tables
     */
    selectAll() {
        for (const table of this.sourceImport.SchemaTables) {
            table.value = this.selectAllButton;
            //if (this.checkIfChecked(document.getElementsByName('tables')[0])) {
            //     table.value = true;
            // } else {
            //     table.value = false;
            // }
        }
    }

    /**
     * Selects a single table
     * @param key 
     */
    select(key) {
        key.value = !key.value;
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    public show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }


}
