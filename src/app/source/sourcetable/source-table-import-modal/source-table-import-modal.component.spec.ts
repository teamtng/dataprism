import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceTableImportModalComponent } from './source-table-import-modal.component';

describe('SourceTableImportModalComponent', () => {
  let component: SourceTableImportModalComponent;
  let fixture: ComponentFixture<SourceTableImportModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceTableImportModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceTableImportModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});