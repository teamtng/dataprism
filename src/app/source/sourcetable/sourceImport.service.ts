import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SourceTable } from './sourcetable';
import { ErrorHandler } from 'utilities/error';
import { DataHandler } from 'utilities/data';

@Injectable()
export class SourceImportTableService {
    private apiPath = 'api/EDWSourceSystemImport';

    sourceTables: SourceTable[];
    sourceCheckedTables: SourceTable[];
    schemaName: any;

    constructor(private http: Http) { }


    /**
     * Loads all the SourceTables into the local variable sourceTables
     * @param systemId id of source system
     */
    getCheckedTables(systemId) {
        return this.http.get(this.apiPath + '/' + systemId)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Imports the selected tables into the sourcetables
     * @param schema
     */
    importTables(schema) {
        return this.http.put(this.apiPath, schema)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
