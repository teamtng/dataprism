import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { SourceColumnService } from './sourcecolumn.service';
import { SourceColumn } from './sourcecolumn';
import { Subject } from 'rxjs/Rx';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'dp-sourcecolumn',
    templateUrl: './sourcecolumn.component.html',
    providers: [SourceColumnService]
})

export class SourceColumnComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    tableId: any;
    name: any;
    table: any;
    systemId: any;
    sourceColumns: SourceColumn[];
    defaultOrder: number = 1;

    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<SourceColumn[]> = new Subject();

    constructor(private route: ActivatedRoute, private router: Router, private sourceColumnService: SourceColumnService) { }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.systemId = +params['id'];
        });
        this.route.queryParams.subscribe(qparams => {
            this.name = qparams['name'] + '';
            this.table = qparams['friendlyName'] + '';
            this.tableId = qparams['tableId'] + '';
        });
        this.dtOptions = {};
        this.getSourceColumns();
    }

    /**
     * Calls the SourceColumnService to get all the SourceColumns of the SourceTable
     */
    getSourceColumns() {
        if (this.sourceColumns) {
            this.rerender();
        }
        this.sourceColumnService.getSourceColumnsBySourceTable(this.tableId)
            .subscribe(sourceColumns => {
                this.sourceColumns = sourceColumns;
                this.defaultOrder = this.sourceColumns.length + 1;
                this.dtTrigger.next();
            });
    }

    gotoTable(table, name) {
        this.router.navigate(['/source/sourcetable/' + table], { queryParams: { name: name } });
    }

    /**
     * Destroys table instance when populated
     */
    private rerender() {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
        });
    }

}
