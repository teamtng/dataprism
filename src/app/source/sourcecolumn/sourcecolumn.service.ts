import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { DataHandler } from 'utilities/data';
import { ErrorHandler } from 'utilities/error';
import { SourceColumn } from 'app/source/sourcecolumn/sourcecolumn';

@Injectable()
export class SourceColumnService {
    private apiPath = 'api/EDWSourceColumn';

    constructor(private http: Http) { }

    /**
     * Gets all the SourceColumns of a given SourceTable
     * @param tableId number - id of source table
     */
    getSourceColumnsBySourceTable(tableId: number): Observable<any> {
        return this.http.get(`${this.apiPath}?tableId=${tableId}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Save the Source System from modal
     * @param SourceColumn SourceColumn - source column to save
     */
    saveColumn(SourceColumn: SourceColumn): Observable<any> {
        return this.http.put(this.apiPath, SourceColumn)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
