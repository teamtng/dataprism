import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceColumnModalComponent } from './source-column-modal.component';

describe('SourceColumnModalComponent', () => {
  let component: SourceColumnModalComponent;
  let fixture: ComponentFixture<SourceColumnModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceColumnModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceColumnModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});