import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { SourceColumn } from '../sourcecolumn';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { UtilityHandler } from '../../../../utilities/utility';
import { SourceColumnService } from '../sourcecolumn.service';
import { Subject } from 'rxjs/Rx';
import { StringHandler } from '../../../../utilities/string';

@Component({
    selector: 'dp-source-column-modal',
    templateUrl: './source-column-modal.component.html',
    styleUrls: ['./source-column-modal.component.css'],
    providers: [SourceColumnService, SnackBarHandler]
})

export class SourceColumnModalComponent implements OnInit {

    @Output() systemColumnChange = new EventEmitter();
    @Input() tableId: number;
    @Input() defaultOrder: number;

    dtTrigger: Subject<SourceColumn[]> = new Subject();

    private stringHandler: StringHandler = new StringHandler();

    private dataTypes;
    private SourceColumn: SourceColumn;

    constructor(private columnService: SourceColumnService, private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.dataTypes = this.loadDataTypes();
        this.clearColumn();
    }

    /**
     * Autofills data in source column depending on BaseDataType
     * @param sourceColumn
     */
    baseDataTypeFilled(sourceColumn: SourceColumn): SourceColumn {
        if (sourceColumn.BaseDataType) {
            if (sourceColumn.BaseDataType == "DATE") {
                sourceColumn.Precision = null;
                sourceColumn.Scale = null;
                sourceColumn.FullDataType = "DATE";
            }
            else {
                sourceColumn.FullDataType = null;
                sourceColumn = this.scaleOrPrecisionFilled(sourceColumn);
            }
        }

        return sourceColumn;
    }

    /**
     * For empty modal
     */
    clearColumn() {
        this.SourceColumn = new SourceColumn(this.tableId);
        this.SourceColumn.Order = this.defaultOrder;
    }

    /**
     * Load for modal
     * @param column - sourceColumn to use as base
     */
    loadColumn(column: SourceColumn) {
        if (column.FriendlyName1 && !column.FriendlyName1Conv) {
            column.FriendlyName1Conv = this.uglifyString(column.FriendlyName1);
        }
        if (column.FriendlyName2 && !column.FriendlyName2Conv) {
            column.FriendlyName2Conv = this.uglifyString(column.FriendlyName2);
        }

        column = this.baseDataTypeFilled(column);
        column = this.scaleOrPrecisionFilled(column);

        this.SourceColumn = column;
    }

    /**
     * returns all datatypes in this case for an oracle database;
     */
    private loadDataTypes() {
        return [{ 'Name': 'NUMBER' },
        { 'Name': 'FLOAT' },
        { 'Name': 'VARCHAR2' },
        { 'Name': 'DATE' },
        { 'Name': 'CHAR' },
        { 'Name': 'NVARCHAR2' },
        { 'Name': 'NCHAR' }];
    }

    /**
     * Save/Update column using columnService
     * @param column - source column to be saved
     */
    saveColumn(column: SourceColumn) {
        this.columnService.saveColumn(this.SourceColumn).subscribe(() => {
            this.snackBarHandler.open(`${this.SourceColumn.DatabaseName} ${this.SourceColumn.Order} has been saved.`, 'success');
            this.systemColumnChange.emit();
        }, err => {
            this.snackBarHandler.open(err);
        });
    }

    /**
     * Autofills data in FullDataType field
     * @param sourceColumn 
     */
    scaleOrPrecisionFilled(sourceColumn: SourceColumn): SourceColumn {
        if (sourceColumn.Scale && sourceColumn.Precision) {
            sourceColumn.FullDataType = sourceColumn.BaseDataType + "(" + sourceColumn.Precision + "," + sourceColumn.Scale + ")";
        }
        else if (sourceColumn.Precision) {
            sourceColumn.FullDataType = sourceColumn.BaseDataType + "(" + sourceColumn.Precision + ")";
        }

        return sourceColumn
    }

    toggleNullable() {
        this.SourceColumn.NullableFlag = this.SourceColumn.NullableFlag == 'Y' ? 'N' : 'Y';
    }

    uglifyString(str: string) {
        return this.stringHandler.uglifyString(str);
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    public show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }
}