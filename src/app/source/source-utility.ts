import { SourceColumn } from 'app/source/sourcecolumn/sourcecolumn';
import { StringHandler } from 'utilities/string';
import { SourceTable } from 'app/source/sourcetable/sourcetable';
import { SourceSystem } from 'app/source/sourcesystem/sourcesystem';

export class SourceUtility {

    constructor() { }

    private stringHandler: StringHandler = new StringHandler();

    /**
     * Constructs source columns from csv object to be saved
     */
    constructSourceColumns(tableId: number, csvObject: any): SourceColumn[] {
        let returnColumns: SourceColumn[] = new Array();
        const columnNames: string[] = csvObject[0];
        const columnValues: any[] = csvObject[1];

        columnNames.forEach((column, index) => {
            let tempColumn: SourceColumn = new SourceColumn(tableId);
            tempColumn.SourceTableId = tableId;
            tempColumn.Order = index;

            column = column.trim();
            tempColumn.DatabaseName = column;
            tempColumn.FriendlyName1 = this.stringHandler.friendifyString(column);
            tempColumn.FriendlyName1Conv = this.stringHandler.uglifyString(column);

            if (typeof (columnValues[index]) === 'string') {
                if (columnValues[index].toLowerCase() == 'null') {
                    tempColumn.NullableFlag = 'Y';
                }
            }
            else if (columnValues[index] instanceof Date) {
                tempColumn.BaseDataType = 'DATE';
                tempColumn.FullDataType = 'DATE';
            } else if (typeof (columnValues[index]) === 'number') {
                tempColumn.BaseDataType = 'NUMBER';
            }

            returnColumns.push(tempColumn);
        });
        return returnColumns;
    }

    /**
     * Constructs a new source table from info in params to be saved
     */
    constructSourceTable(savedSourceSystem: SourceSystem, sourceTableInfo: SourceTable, fileName: string): SourceTable {
        let tempSourceTable = new SourceTable(savedSourceSystem.Id);
        tempSourceTable.SourceSystemId = savedSourceSystem.Id;
        tempSourceTable.SchemaName = savedSourceSystem.DBSchemaName;

        tempSourceTable.FileType = sourceTableInfo.FileType;
        tempSourceTable.FirstLineHeader = sourceTableInfo.FirstLineHeader;
        tempSourceTable.FileDelimiter = sourceTableInfo.FileDelimiter;
        tempSourceTable.TextQualifier = sourceTableInfo.TextQualifier;

        tempSourceTable.DatabaseName = fileName;
        tempSourceTable.FriendlyName = this.stringHandler.friendifyString(fileName);

        return tempSourceTable;
    }

}