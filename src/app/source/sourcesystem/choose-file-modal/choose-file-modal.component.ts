import { Component, OnInit, Inject } from '@angular/core';
import { MdDialogRef, MD_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'choose-file-modal',
    templateUrl: './choose-file-modal.component.html'
})

export class ChooseFileModalComponent implements OnInit {
    curFile: File = null;

    constructor(public dialogRef: MdDialogRef<ChooseFileModalComponent>, @Inject(MD_DIALOG_DATA) public data: any) { }

    ngOnInit() { }

    /**
     * Called when file is uploaded via browse file path to set values of other inputs
     * @param event 
     */
    fileUploaded(event) {
        this.curFile = event.target.files[0];
    }
}
