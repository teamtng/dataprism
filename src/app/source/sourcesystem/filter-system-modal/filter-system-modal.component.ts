import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SourceSystem } from '../sourcesystem';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { UtilityHandler } from '../../../../utilities/utility';
import { SourceSystemService } from '../sourcesystem.service';
import { Subject } from 'rxjs/Rx';

@Component({
    selector: 'dp-filter-source-system-modal',
    templateUrl: './filter-system-modal.component.html',
    styleUrls: ['./filter-system-modal.component.css'],
    providers: [SourceSystemService, SnackBarHandler]
})

export class FilterSystemModalComponent implements OnInit {

    @Output() filterChange = new EventEmitter();

    systemOutput: SourceSystem;
    dtTrigger: Subject<SourceSystem[]> = new Subject();
    SourceSystem: SourceSystem;
    

    constructor(private systemService: SourceSystemService, 
        private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.clearSystem();
    }

    /**
     * For empty modal
     * @author John Crabill
     */
    clearSystem() {
        this.SourceSystem = new SourceSystem();
    }


    /**
     * For updating system shown in sourcesystem
     * @author John Crabill
     */
    saveFilter(system: SourceSystem) {
        this.systemOutput = system;
        this.filterChange.emit();
    }

    /**
     * For clearing filter shown in sourcesystem
     * @author John Crabill
     */
    clearFilter(){
        this.systemOutput = new SourceSystem;
        this.filterChange.emit();
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    public show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }


}
