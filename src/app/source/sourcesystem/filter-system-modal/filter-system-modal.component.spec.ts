import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterSystemModalComponent } from './filter-system-modal.component';

describe('FilterSystemModalComponent', () => {
  let component: FilterSystemModalComponent;
  let fixture: ComponentFixture<FilterSystemModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterSystemModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterSystemModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
