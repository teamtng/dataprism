import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { SourceSystemService } from './sourcesystem.service';
import { SourceSystem } from './sourcesystem';
import { Subject } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { SnackBarHandler } from 'utilities/snackbar';

@Component({
    selector: 'dp-sourcesystem',
    templateUrl: './sourcesystem.component.html',
    providers: [SourceSystemService, SnackBarHandler]
})

export class SourceSystemComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    sourceSystems: SourceSystem[];

    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<SourceSystem[]> = new Subject();

    constructor(private sourceSystemService: SourceSystemService, private router: Router, private snackBarHandler: SnackBarHandler) { };

    ngOnInit(): void {
        this.dtOptions = {};
        this.getSourceSystems();
    }

    /**
     * Checks to see if sourceSystems have been loaded; destroys the datatable if they have
     * Calls the SourceSystemService to get all SourceSystems
     */
    getSourceSystems() {
        if (this.sourceSystems) {
            this.rerender();
        }

        this.sourceSystemService.getSourceSystems()
            .subscribe(sourceSystems => {
                this.sourceSystems = sourceSystems;
                this.dtTrigger.next();
            });
    }

    /**
     * Redirects to the table route
     * @param system
     */
    private gotoSystem(system: SourceSystem) {
        this.router.navigate(['/source/sourcetable/' + system.Id], { queryParams: { name: system.Name, vendor: system.DBVendorName } });
    }

    /**
     * Testing Connection
     * @author John Crabill
     * @param system
     */
    private testConnection(system: SourceSystem){
        this.sourceSystemService.testConnection(system)
            .subscribe(conn => {
                if(conn == "Success"){
                    this.snackBarHandler.open('Connection Found', 'success');
                }
                else if(conn == "Failed"){
                    this.snackBarHandler.open('Connection Failed', 'failure');
                }
            });
    }

    getFilterSystems(system){
        if(system.Name != undefined || system.DBVendorName != undefined || system.DBHostName){
            if (this.sourceSystems) {
                this.rerender();
            }
            this.sourceSystemService.getSourceSystemFiltered(system)
                .subscribe(systems => {
                    this.sourceSystems = systems;
                    this.dtTrigger.next();
            });
        }
        else {
            this.getSourceSystems();
        }
    }

    /**
     * Destroys table instance when populated
     */
    private rerender() {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
        });
    }

    // private showChooseFileModal(system: SourceSystem): Promise<any> {
    //     return new Promise((resolve, reject) => {
    //         let dialogRef = this.dialog.open(ChooseFileModalComponent, {
    //             data: system
    //         });
    //         dialogRef.afterClosed()
    //             .subscribe(file => {
    //                 if (this.checkValidFile(file, system)) {
    //                     this.fileHandler.readCsv(file, system.Header, system.Delimiter, system.TextQualifier)
    //                         .then(readSystem => {
    //                             console.log(readSystem);
    //                         })
    //                         .catch(err => {
    //                             this.snackBarHandler.openSnackBar('The file could not be read.', 'failure');
    //                         });
    //                 }
    //             });
    //     });
    // }

    tableChosen(system: SourceSystem) {
        this.gotoSystem(system);
    }

}
