import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SourceSystem } from '../sourcesystem';
import { SnackBarHandler } from '../../../../utilities/snackbar';
import { UtilityHandler } from '../../../../utilities/utility';
import { FileHandler } from '../../../../utilities/file';
import { SourceSystemService } from '../sourcesystem.service';
import { Subject } from 'rxjs/Rx';
import { SourceTable } from 'app/source/sourcetable/sourcetable';
import { SourceColumn } from 'app/source/sourcecolumn/sourcecolumn';
import { SourceTableService } from 'app/source/sourcetable/sourcetable.service';
import { SourceColumnService } from 'app/source/sourcecolumn/sourcecolumn.service';
import { SourceUtility } from 'app/source/source-utility';

@Component({
    selector: 'dp-source-system-modal',
    templateUrl: './source-system-modal.component.html',
    styleUrls: ['./source-system-modal.component.css'],
    providers: [SourceSystemService, SnackBarHandler, SourceTableService, SourceColumnService]
})

export class SourceSystemModalComponent implements OnInit {

    @Output() systemChange = new EventEmitter();

    private fileHandler: FileHandler = new FileHandler();
    private sourceUtility: SourceUtility = new SourceUtility();

    dtTrigger: Subject<SourceSystem[]> = new Subject();

    SourceSystem: SourceSystem;
    SourceTablesInfo: SourceTable;
    
    FromFolder: boolean = false;

    constructor(private systemService: SourceSystemService, private tableService: SourceTableService, private columnService: SourceColumnService, 
        private snackBarHandler: SnackBarHandler) { }

    ngOnInit() {
        this.clearSystem();
    }

    /**
     * For empty modal
     */
    clearSystem() {
        this.SourceSystem = new SourceSystem();
        this.SourceTablesInfo = new SourceTable(this.SourceSystem.Id);
    }

    /**
     * Called when file is uploaded via browse file path to set values of other inputs
     * @param event 
     */
    filesUploaded(event) {
        const file: File = event.target.files[0];
        const [fileName, fileExt] = file.name.split('.');

        if (fileExt == 'txt' || fileExt == 'csv') {
            this.SourceTablesInfo.FileType = fileExt;
            $('#typeFormInput').val(fileExt);

            // Sets Default values
            this.SourceTablesInfo.FileDelimiter = ',';
            this.SourceTablesInfo.TextQualifier = `"`;
            this.SourceTablesInfo.FirstLineHeader = 1;
        }
    }

    /**
     * Load for modal
     * @param system sourceSystem - source system to load
     */
    loadSystem(system: SourceSystem) {
        this.SourceSystem = system;
    }

    /**
     * Save/Update on source system
     * @param system sourceSystem - source system to be saved
     */
    saveSystem(system: SourceSystem, tablesInfo: SourceTable) {
        this.systemService.saveSystem(system)
            .subscribe(savedSystem => {
                this.snackBarHandler.open(`${savedSystem.Name} has been saved.`, 'success');
                this.systemChange.emit();

                if (this.FromFolder) {
                    let files: FileList = document.forms['dp-add-ss-form']['fileFormInput'].files;
                    for (let i = 0; i < files.length; i++) {
                        this.fileHandler.readLocalCsv(files.item(i), tablesInfo.FirstLineHeader, tablesInfo.FileDelimiter, tablesInfo.TextQualifier)
                            .then(csvObject => {
                                let tempSourceTable: SourceTable = this.sourceUtility.constructSourceTable(savedSystem, this.SourceTablesInfo, files.item(i).name.split('.')[0]);
                                this.tableService.saveTable(tempSourceTable)
                                    .subscribe(savedTable => {
                                        let sourceColumns: SourceColumn[] = new Array();
                                        sourceColumns = this.sourceUtility.constructSourceColumns(savedTable.Id, csvObject);
            
                                        sourceColumns.forEach(column => {
                                            this.columnService.saveColumn(column).subscribe(() => {}, err => console.log(err));
                                        });
                                    }, err => {
                                        console.log(err);
                                    });
                            })
                            .catch(err => {
                                this.snackBarHandler.open('The file could not be read.', 'failure');
                            });
                    }
                }
            }, err => {
                console.log(err);
                this.snackBarHandler.open('The system could not be saved.', 'failure');
            });
    }

    testConnection(system){
        this.systemService.testConnection(system)
            .subscribe(conn => {
                if(conn == "Success"){
                    this.snackBarHandler.open('Connection Found', 'success');
                }
                else if(conn == "Failed"){
                    this.snackBarHandler.open('Connection Failed', 'failure');
                }
            });
    }

    setFromFolder(fromFolder: boolean) {
        this.FromFolder = fromFolder;
    }

    toggleHeader() {
        this.SourceTablesInfo.FirstLineHeader = this.SourceTablesInfo.FirstLineHeader == 1 ? 0 : 1;
    }

    /**
     * Modal Variables
     */
    public visible = false;
    private visibleAnimate = false;

    /**
     * Modal Functions
     */

    /**
     * Modal Show
     */
    public show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true, 100);
    }

    /**
     * Modal Hide
     */
    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }


    /**
     * Click Modal Event
     */
    public onContainerClicked(event: MouseEvent): void {
        if ((<HTMLElement>event.target).classList.contains('modal')) {
            this.hide();
        }
    }


}
