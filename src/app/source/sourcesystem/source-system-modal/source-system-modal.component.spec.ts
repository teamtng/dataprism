import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceSystemModalComponent } from './source-system-modal.component';

describe('SourceSystemModalComponent', () => {
  let component: SourceSystemModalComponent;
  let fixture: ComponentFixture<SourceSystemModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceSystemModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceSystemModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
