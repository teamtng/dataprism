import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SourceSystem } from './sourcesystem';
import { DataHandler } from 'utilities/data';
import { ErrorHandler } from 'utilities/error';

@Injectable()
export class SourceSystemService {
    private apiPath = 'api/EDWSourceSystem';
    private apiConn = 'api/Connection';

    constructor(private http: Http) { }

    /**
     * Gets all Source Systems
     */
    getSourceSystems(): Observable<any> {
        return this.http.get(this.apiPath)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    getSourceSystemFiltered(system): Observable<any> {
        console.log(system);
        if(system.Name == undefined){
            system.Name = null;
        }
        if(system.DBVendorName == undefined){
            system.DBVendorName = null;
        }
        if(system.DBHostName == undefined){
            system.DBHostName = null;
        }
        return this.http.get(`${this.apiPath}?name=${system.Name}&dbVendorName=${system.DBVendorName}&dbHostName=${system.DBHostName}`)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Save the Source System from modal
     * @param SourceSystem SourceSystem - source system to save
     */
    saveSystem(SourceSystem: SourceSystem): Observable<any> {
        return this.http.put(this.apiPath, SourceSystem)
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

    /**
     * Tests the connection
     * @author John Crabill
     * @param SourceSystem SourceSystem - source system to scheck
     */
    testConnection(SourceSystem: SourceSystem){
        return this.http.put(this.apiConn, SourceSystem) 
            .map(new DataHandler().extractData)
            .catch(new ErrorHandler().handleError);
    }

}
