import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http'
import { httpFactory } from '../utilities/http.factory';
import { DateAdapter, MaterialModule, MD_DATE_FORMATS, MdDatepickerModule, MdInputModule, MdNativeDateModule, NativeDateAdapter, MdDialogModule } from '@angular/material';
import 'hammerjs';
import { CookieModule, CookieService } from 'ngx-cookie';
import { TokenService } from './auth/token.service';
import { AuthenticationService } from './auth/authentication.service';
import { SnackBarHandler } from 'utilities/snackbar';
import { UserUtility } from 'utilities/user-utility';
import { NgxPaginationModule } from 'ngx-pagination';
import { DndModule } from 'ng2-dnd';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AppComponent } from './app.component';
import { ModalComponent } from './core/modal.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BusinessGlossaryComponent } from './glossary/business-glossary/business-glossary.component';
import { CategoryComponent } from './glossary/category/category.component';
import { TermComponent } from './glossary/term/term.component';
import { GlossaryComponent } from './glossary/glossary/glossary.component';
import { TermModalComponent } from './glossary/term/term-modal/term-modal.component';
import { CategoryModalComponent } from './glossary/category/category-modal/category-modal.component';
import { GlossaryModalComponent } from './glossary/glossary/glossary-modal/glossary-modal.component';
import { RelatedTermModalComponent } from './glossary/term/related-term-modal/related-term-modal.component';
import { SourceSystemComponent } from './source/sourcesystem/sourcesystem.component'
import { SourceSystemModalComponent } from './source/sourcesystem/source-system-modal/source-system-modal.component';
import { SourceTableModalComponent } from './source/sourcetable/source-table-modal/source-table-modal.component';
import { SourceTableComponent } from './source/sourcetable/sourcetable.component';
import { SourceTableImportModalComponent } from './source/sourcetable/source-table-import-modal/source-table-import-modal.component';
import { ImportSchemaImportModalComponent } from './schema/import-modal/import-schema-modal.component';
import { SourceColumnComponent } from './source/sourcecolumn/sourcecolumn.component';
import { SourceColumnModalComponent } from './source/sourcecolumn/source-column-modal/source-column-modal.component';
import { AdminDashboardComponent } from './admin/admindashboard.component';
import { UsersComponent } from './admin/users/users.component';
import { JobHistoryComponent } from './jobhistory/jobhistory.component';
import { JobEditorModalComponent } from './jobhistory/job-editor-modal/job-editor-modal.component';
import { SchemaComponent } from './schema/schema.component';
import { AddSchemaModalComponent } from './schema/add-schema-modal/add-schema-modal.component';
import { EditSchemaModalComponent } from './schema/edit-schema-modal/edit-schema-modal.component';
import { SchemaDetailModalComponent } from './schema/schema-detail-modal/schema-detail-modal.component';
import { AddTableModalComponent } from './schema/add-table-modal/add-table-modal.component';
import { TargetTableComponent } from './tables/target-table/target-table.component';
import { TargetTableSystemComponent } from './tables/target-table-system/target-table-system.component';
import { TargetTableColumnComponent } from './tables/target-table-column/target-table-column.component';
import { TargetLinkModalComponent } from './glossary/term/target-column-link-modal/target-column-link-modal.component';
import { ColumnLineageRowComponent } from './tables/column-lineage-row/column-lineage-row.component';
import { TargetColumnModalComponent } from './tables/target-column-modal/target-column-modal.component';
import { SqlConfiguratorComponent } from './tables/sqlconfigurator/sqlconfigurator.component';
import { SourceTableJoinComponent } from './tables/sourcetablejoin/sourcetablejoin.component';
import { SourceTableJoinConditionComponent } from './tables/sourcetablejoincondition/sourcetablejoincondition.component';
import { ColumnLineageEditorComponent } from './tables/column-lineage-editor/column-lineage-editor.component';
import { JoinAliasSelectorComponent } from './tables/column-lineage-editor/join-alias-selector/join-alias-selector.component';
import { FilterEditorComponent } from './tables/column-lineage-editor/filter-editor/filter-editor.component';
import { TransformationEditorComponent } from './tables/column-lineage-editor/transformation-editor/transformation-editor.component';
import { CalculationEditorComponent } from './tables/column-lineage-editor/calculation-editor/calculation-editor.component';
import { SummarySelectorComponent } from './tables/column-lineage-editor/summary-selector/summary-selector.component';
import { ColumnLineagePropertiesComponent } from './tables/column-lineage-editor/column-lineage-properties/column-lineage-properties.component';
import { TransformationButtonComponent } from './tables/column-lineage-editor/transformation-editor/transformation-button.component';
import { DatalineageComponent } from './datalineage/datalineage.component';
import { TargetColumnTreeComponentComponent } from './datalineage/target-column-tree-component/target-column-tree-component.component';
import { LoginComponent } from './login/login.component';
import { CollapsibleTreeComponent } from './datalineage/collapsibleTree.component';
import { TabsComponent } from './core/tabs/tabs.component';
import { TabComponent } from './core/tab/tab.component';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { TargetColumnPipe } from './tables/target-table-column/target-column-pipe';
import { ExcludeObjectPipe } from './pipes/exclude-object.pipe';
import { TermTypePipe } from './pipes/term-type.pipe';
import { TermStatusPipe } from './pipes/term-status.pipe';
import { ChooseFileModalComponent } from 'app/source/sourcesystem/choose-file-modal/choose-file-modal.component';
import { AddUserModalComponent } from 'app/admin/users/add-user-modal/add-user-modal.component';
import { TermLinkModalComponent } from './tables/target-table-column/term-link-modal/term-link-modal.component';
import { FilterSystemModalComponent } from './source/sourcesystem/filter-system-modal/filter-system-modal.component';
import { GeneratedJobsComponent } from './admin/generatedjobs/generatedjobs.component';
import { SchemaTableImportModalComponent } from './schema/import-target-modal/import-target-schema-modal.component';

@NgModule({
    declarations: [
        // APP
        AppComponent,

        // CORE
        ModalComponent,

        // HEADER
        HeaderComponent,

        // DASHBOARD
        DashboardComponent,

        // GLOSSARY
        BusinessGlossaryComponent,
        GlossaryComponent,
        CategoryComponent,
        TermComponent,
        TermModalComponent,
        CategoryModalComponent,
        GlossaryModalComponent,
        RelatedTermModalComponent,
        TargetLinkModalComponent,

        // SOURCE
        SourceSystemComponent,
        SourceSystemModalComponent,
        SourceTableComponent,
        SourceTableModalComponent,
        SourceColumnComponent,
        SourceColumnModalComponent,
        SourceTableImportModalComponent,
        FilterSystemModalComponent,

        // ADMIN
        AdminDashboardComponent,
        UsersComponent,
        AddUserModalComponent,
        GeneratedJobsComponent,

        // JOB HISTORY
        JobHistoryComponent,
        JobEditorModalComponent,

        // LINEAGE EDITOR
        SchemaComponent,
        AddSchemaModalComponent,
        EditSchemaModalComponent,
        SchemaDetailModalComponent,
        AddTableModalComponent,
        TermLinkModalComponent,
        SchemaTableImportModalComponent,

        ImportSchemaImportModalComponent,

        TargetTableComponent,
        TargetTableSystemComponent,
        TargetTableColumnComponent,
        TargetColumnModalComponent,
        ColumnLineageRowComponent,
        SourceTableJoinComponent,
        SourceTableJoinConditionComponent,
        ColumnLineageEditorComponent,
        JoinAliasSelectorComponent,
        FilterEditorComponent,
        TransformationEditorComponent,
        CalculationEditorComponent,
        SummarySelectorComponent,
        ColumnLineagePropertiesComponent,
        TransformationButtonComponent,

        // DATA LINEAGE
        DatalineageComponent,
        TargetColumnTreeComponentComponent,
        CollapsibleTreeComponent,

        // LOGIN
        LoginComponent,

        // MISC
        TabComponent,
        TabsComponent,

        // PIPES
        TargetColumnPipe,
        ExcludeObjectPipe,
        TermTypePipe,
        TermStatusPipe,

        // Dialogs
        SqlConfiguratorComponent,
        ChooseFileModalComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        DataTablesModule,
        HttpModule,
        MaterialModule,
        MdDatepickerModule,
        MdDialogModule,
        MdInputModule,
        MdNativeDateModule,
        NgxPaginationModule,
        MultiselectDropdownModule,
        CookieModule.forRoot(),
        DndModule.forRoot(),
        // Router
        RouterModule.forRoot([
            { path: '', component: DashboardComponent },
            { path: 'login', component: LoginComponent },
            {
                path: 'glossary', component: BusinessGlossaryComponent, children: [
                    { path: ':id', component: GlossaryComponent},
                    { path: 'category/:id', component: CategoryComponent},
                    { path: 'term/:id', component: TermComponent }
                ]
            },
            {
                path: 'source', children: [
                    { path: 'sourcesystem', component: SourceSystemComponent },
                    { path: 'sourcetable/:id', component: SourceTableComponent },
                    { path: 'sourcecolumn/:id', component: SourceColumnComponent }
                ]
            },
            {
                path: 'admin', children: [
                    { path: '', component: AdminDashboardComponent },
                    { path: 'users', component: UsersComponent },
                    { path: 'generatedjobs', component: GeneratedJobsComponent }
                ]
            },
            { path: 'jobhistory', component: JobHistoryComponent },
            {
                path: 'schema', children: [
                    { path: '', component: SchemaComponent },
                    { path: 'table/:id', component: TargetTableComponent }
                ]
            },
            { path: 'dataLineage', component: DatalineageComponent }
        ])
    ],
    entryComponents: [
        // Required for MDDialog
        SqlConfiguratorComponent,
        ChooseFileModalComponent
    ],
    providers: [
        {
            provide: Http,
            useFactory: httpFactory,
            deps: [XHRBackend, RequestOptions]
        },
        CookieService,
        TokenService,
        SnackBarHandler,
        UserUtility,
        AuthenticationService
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
