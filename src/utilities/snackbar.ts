import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http';
import { MdSnackBarConfig, MdSnackBar } from '@angular/material';

@Injectable()
export class SnackBarHandler {

    constructor(private snackBar: MdSnackBar) { }

    /**
     * Shows a custom toast message with message string, action dismissal string, and background color decided by type,
     * then auto scrolls to the bottom of the page so the message is seen.
     * @param message message to display
     * @param action action string to show on dismissal button
     * @param type success, failure, or default for green, red, or blue snackbar
     * @param duration length of time the snackbar stays open
     */
    open(message: string, type?: string, action?: string, duration?: number) {

        type = type || 'default';
        action = action || 'dismiss';
        duration = duration || 2000;

        const config = new MdSnackBarConfig();
        config.duration = duration;
        config.extraClasses = type === 'success' ? ['success-snackbar'] : type === 'failure' ? ['failure-snackbar'] : ['default-snackbar'];

        this.snackBar.open(message, action, config);
    }

    close() {
        this.snackBar.dismiss();
    }
}
