export class UtilityHandler {
    /**
     * Checks given variable to see if null or undefined
     * @param variable
     */
    isNull(variable: any): boolean {
        if (typeof (variable) == 'string') {
            return variable == '' || variable == null || variable == undefined;
        }
        return variable == null || variable == undefined;
    }

    /**
     * Checks given variable to see if it is a numeric value
     * @param variable
     */
    isNumeric(variable: any): boolean{
        return /^\d+$/.test(variable);
    }
}
