import { Injectable } from '@angular/core'
import * as parse from 'csv-parse';

@Injectable()
export class FileHandler {

    constructor() { }

    /**
     * Reads a csv from a file input
     * @param file
     * @param Header 
     * @param Delimiter 
     * @param TextQualifier 
     */
    readLocalCsv(file: File, Header: number = 1, Delimiter: string = ",", TextQualifier: string = `"`): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                var reader: FileReader = new FileReader();
                reader.onload = function (event) {
                    let header: boolean = Header == 1 ? true : false;
                    let options: parse.Options = {
                        delimiter: Delimiter,
                        quote: TextQualifier,
                        auto_parse: true,
                        auto_parse_date: true,
                        skip_empty_lines: true,
                        trim: true,
                        to: 2 // First line column names, second line used for typing
                    }
                    let records = parse(reader.result, options, function (err, output) {
                        resolve(output);
                    });
                }
                reader.readAsText(file);
            } catch (err) {
                console.log('Could not read file.');
                reject(err);
            }
        });
    }

    /**
     * Reads a csv from a url
     * @param fileUrl
     * @param Header 
     * @param Delimiter 
     * @param TextQualifier 
     */
    readWebCsv(fileUrl: string, Header: number = 1, Delimiter: string = ",", TextQualifier: string = `"`): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                let xhr = new XMLHttpRequest();
                xhr.onloadend = function (e) {
                    if (xhr.status === 200 || xhr.status == 0) {
                        var reader: FileReader = new FileReader();
                        reader.onload = function (event) {
                            let header: boolean = Header == 1 ? true : false;
                            let records = parse(reader.result, { columns: true, delimiter: Delimiter, quote: TextQualifier }, function (err, output) { });
                            resolve(records);
                        }
                        reader.readAsText(xhr.response);
                    }
                }
                xhr.open('GET', fileUrl, true);
                xhr.responseType = 'blob';
                xhr.send();
            } catch (err) {
                console.log('Could not read file.');
                reject(err);
            }
        });
    }

}
