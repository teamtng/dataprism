import { Injectable } from '@angular/core';

@Injectable()
export class QueueHandler {

    private queue = 0;

    constructor() { }

    /**
     * Adds or Subtracts to the queue length
     * If the queue > 0 it will show a loading screen and then remove it when the queue == 0
     * @param value 1 or -1
     * @author Nash Lindley
     */
    queueChange(value) {
        this.queue += value;
        this.queue === 0 ? $('.loading-bg:not(.hide)').addClass('hide') : $('.loading-bg.hide').removeClass('hide');
    }
}
