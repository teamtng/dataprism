import { Injectable, Injector } from '@angular/core';
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers, ResponseOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { QueueHandler } from 'utilities/queue';
import 'rxjs/add/operator/catch';
import { ErrorHandler } from 'utilities/error';

@Injectable()
export class InterceptedHttp extends Http {
    private router;
    private injector: Injector;
    private queueHandler: QueueHandler = new QueueHandler();

    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
        super(backend, defaultOptions);
    }

    /**
     * Generic Request Interceptor
     * Adds the environments base url to the given path
     * Will add RequestOptionArgs if none have been assigned
     * @param url
     * @param options
     */
    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        this.queueHandler.queueChange(1);
        return super.request(url, options)
            .finally(() => this.queueHandler.queueChange(-1));
    }

    /**
     * Get Inteceptor
     * Adds the environments base url to the given path
     * Will add RequestOptionArgs if none have been assigned
     * @param url
     * @param options
     */
    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        this.queueHandler.queueChange(1);
        url = this.updateUrl(url);
        return super.get(url, this.getRequestOptionArgs(options))
            .finally(() => this.queueHandler.queueChange(-1));
    }

    /**
     * Post Interceptor
     * Adds the environments base url to the given path
     * Will add RequestOptionArgs if none have been assigned
     * @param url
     * @param body
     * @param options
     */
    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        this.queueHandler.queueChange(1);
        url = this.updateUrl(url);
        return super.post(url, body, this.getRequestOptionArgs(options))
            .finally(() => this.queueHandler.queueChange(-1));
    }

    /**
     * Put Interceptor
     * Adds the environments base url to the given path
     * Will add RequestOptionArgs if none have been assigned
     * @param url
     * @param body
     * @param options
     */
    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        this.queueHandler.queueChange(1);
        url = this.updateUrl(url);
        return super.put(url, body, this.getRequestOptionArgs(options))
            .finally(() => this.queueHandler.queueChange(-1));
    }

    /**
     * Delete Interceptor
     * Adds the environments base url to the given path
     * Will add RequestOptionArgs if none have been assigned
     * @param url
     * @param options
     */
    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        this.queueHandler.queueChange(1);
        url = this.updateUrl(url);
        return super.delete(url, this.getRequestOptionArgs(options))
            .finally(() => this.queueHandler.queueChange(-1));
    }

    /**
     * Appends the environments base url to the apiPath
     * @param req
     */
    private updateUrl(req: string) {
        if (environment.production) {
            return environment.serviceUrl + req;
        } else {
            return environment.serviceUrl + req;
        }
        // return environment.serviceUrl + req;
        // return environment.serviceProdUrl + req;
    }

    /**
     * Creates a default RequestOptionsArgs if none is provided
     * @param options
     */
    private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        options.withCredentials = true;

        options.headers.append('Content-Type', 'application/json');

        return options;
    }

}
