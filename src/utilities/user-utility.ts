import { Inject, Injectable } from '@angular/core';
import { AuthenticationService } from '../app/auth/authentication.service'

import { User } from 'app/admin/users/user';

@Injectable()
export class UserUtility {

    constructor(private authService: AuthenticationService) { }

    /**
     * loads the user into the object
     * loads CreatedUser if objects's id < 0
     * loads UpdatedUSer no matter what
     * only works for objects with created and updated users
     * @param object
     */
    loadUser(object) {
        const userId = this.authService.token_user.Id;
        if (object.Id < 0) {
            object.CreatedUser = new User();
            object.CreatedUser.Id = userId;
        }
        object.UpdatedUser = new User();
        object.UpdatedUser.Id = userId;
        return object;
    }
}
