import { Observable } from 'rxjs/Observable';
import { SnackBarHandler } from 'utilities/snackbar'
import { Router } from '@angular/router';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

export class ErrorHandler {
    /**
     * Handles Errors that occur
     * @param error
     */

    private snackbar: SnackBarHandler;
    private router: Router

    handleError(error: any): ErrorObservable {
        let errMsg: string;
        if (error instanceof Response) {
            switch (error.status) {
                case 401:
                    errMsg = `Not Authenticated`;
                    this.router.navigateByUrl('/login');
                    break;
                default:
                    errMsg = `${error.statusText || 'Request Failed'}`;
                    break;
            }
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        // this.snackbar.openSnackBar(errMsg, 'failure');
        return Observable.throw(errMsg);
    }

}
