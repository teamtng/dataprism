import { Http, Response } from '@angular/http';

export class DataHandler {

    /**
     * Converts a response to a json object
     * @param res - response from API call
     */
    extractData(res: Response) {
        const body = res.text().length > 0 ? res.json() : {};
        return body || {};
    }
}
